DROP PROCEDURE IF EXISTS migration;
DELIMITER &&
  CREATE PROCEDURE migration()
  proc: BEGIN
		  SET @version := (SELECT `version` FROM `version`);
		  IF @version >= 15 THEN
			LEAVE proc;
		  END IF;

		  START TRANSACTION;
		  ALTER TABLE user
		  ADD COLUMN `prénom` VARCHAR(255) DEFAULT '' NULL,
		  ADD COLUMN `nom` VARCHAR(255) DEFAULT '' NULL,
		  ADD COLUMN `nom_complet` VARCHAR(255) DEFAULT '' NULL,
		  ADD COLUMN `biographie` TEXT DEFAULT '' NULL,
		  ADD COLUMN `pseudonyme` VARCHAR(255) DEFAULT '' NOT NULL,
		  ADD COLUMN `avatar` VARCHAR(255) DEFAULT '' NULL,
		  ADD COLUMN `connaissances` VARCHAR(255) DEFAULT '' NULL,
		  ADD COLUMN `occupation` INT(11) DEFAULT 0 NOT NULL;

		  UPDATE `user` SET `pseudonyme` = `username`;

		  UPDATE `version` SET `version` = 15;
		  COMMIT;

		END &&
DELIMITER ;

CALL migration();
DROP PROCEDURE migration;
