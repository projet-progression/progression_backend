DROP PROCEDURE IF EXISTS migration;
DELIMITER &&
  CREATE PROCEDURE migration()
  proc: BEGIN
          SET @version := (SELECT `version` FROM `version`);
          IF @version >= 14 THEN
            LEAVE proc;
          END IF;

          START TRANSACTION;

          ALTER TABLE reponse_prog ADD COLUMN `publique` BOOLEAN DEFAULT FALSE;

          UPDATE `version` SET `version` = 14;
          COMMIT;

        END &&
DELIMITER ;

CALL migration();
DROP PROCEDURE migration;
