<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
   |--------------------------------------------------------------------------
   | Application Routes
   |--------------------------------------------------------------------------
   |
   | Here is where you can register all of the routes for an application.
   | It is a breeze. Simply tell Lumen the URIs it should respond to
   | and give it the Closure to call when that URI is requested.
   |
 */

$router->options("{all:.*}", [
	"middleware" => "cors",
	function () {
		return response("");
	},
]);

$router->group(["middleware" => ["auth_optionnelle"]], function () use ($router) {
	// Configuration serveur
	$router->get("/", "ConfigCtl@get");

	// Inscription
	$router->put("/user/{username}", "ContrôleurFrontal@put_user");
	$router->post("/users", "ContrôleurFrontal@post_user");
});

$router->group(["middleware" => ["auth", "étatNonInactif"]], function () use ($router) {
	// Ébauche
	$router->get("/ebauche/{question_uri}/{langage}", "ÉbaucheCtl@get");

	// Question
	$router->get("/question/{question_uri}", "QuestionCtl@get");
	$router->get("/question/{question_uri}/relationships/ebauches", "NotImplementedCtl@get");
	$router->get("/question/{question_uri}/relationships/tests", "NotImplementedCtl@get");
	$router->get("/question/{question_uri}/ebauches", "NotImplementedCtl@get");
	$router->get("/question/{question_uri}/tests", "NotImplementedCtl@get");

	// Test
	$router->get("/test/{question_uri}/{numero:[[:digit:]]+}", "TestCtl@get");

	// Résultat
	$router->post("/question/{uri}/resultats", "ContrôleurFrontal@post_résultat");

	// Banque
	$router->get("/user/{username}/banques", "BanqueCtl@get");
});

$router->group(["middleware" => ["auth_optionnelle", "permissionsRessources"]], function () use ($router) {
	// Avancement
	$router->get("/avancement/{username}/{question_uri}", "AvancementCtl@get");
	$router->get("/avancement/{username}/{chemin}/relationships/tentatives", "NotImplementedCtl@get");
	$router->get("/avancement/{username}/{chemin}/relationships/sauvegardes", "NotImplementedCtl@get");
	$router->get("/avancement/{username}/{chemin}/relationships/avancements", "NotImplementedCtl@get");
	$router->get("/avancement/{username}/{chemin}/avancements", "NotImplementedCtl@get");

	// Avancements
	$router->get("/user/{username}/avancements", "AvancementsCtl@get");

	// Banque(s)
	$router->get("/banque/{username}/{url}", "BanqueCtl@get");
	$router->get("/user/{username}/banques", "BanquesCtl@get");

	// Commentaire
	$router->get(
		"/commentaire/{username}/{question_uri}/{timestamp:[[:digit:]]{10}}/{numero}",
		"NotImplementedCtl@get",
	);

	// Sauvegarde
	$router->get("/sauvegarde/{username}/{question_uri}/{langage}", "SauvegardeCtl@get");
	$router->get("/avancement/{username}/{question_uri}/sauvegardes", "NotImplementedCtl@get");

	// Tentative
	$router->get("/tentative/{username}/{question_uri}/{timestamp:[[:digit:]]{10}}", "TentativeCtl@get");

	// User
	$router->get("/user/{username}", "UserCtl@get");
	$router->get("/user/{username}/relationships/avancements", "NotImplementedCtl@get");
});

$router->group(["middleware" => ["auth", "permissionsRessources", "étatActif"]], function () use ($router) {
	// Token
	$router->post("/user/{username}/tokens", "ContrôleurFrontal@post_token");

	// Avancement
	$router->put("/avancement/{username}/{question_uri}", "ContrôleurFrontal@put_avancement");
	$router->patch("/avancement/{username}/{question_uri}", "ContrôleurFrontal@patch_avancement");
	$router->post("/user/{username}/avancements", "ContrôleurFrontal@post_avancement");

	// Clé
	$router->get("/cle/{username}/{nom}", "CléCtl@get");

	// Commentaire
	$router->post(
		"/tentative/{username}/{question_uri}/{timestamp:[[:digit:]]{10}}/commentaires",
		"ContrôleurFrontal@post_commentaire",
	);

	// Résultat
	$router->post("/test/{username}/{question_uri}/{numero:[[:digit:]]+}", "NotImplementedCtl@get");

	// Sauvegarde
	$router->post("/avancement/{username}/{question_uri}/sauvegardes", "ContrôleurFrontal@post_sauvegarde");
	// Tentative
	$router->post("/avancement/{username}/{question_uri}/tentatives", "ContrôleurFrontal@post_tentative");

	// Banque
	$router->put("/banque/{username}/{url}", "ContrôleurFrontal@put_banque");
	$router->post("/user/{username}/banques", "ContrôleurFrontal@post_banque");

	$router->delete(
		"/commentaire/{username}/{question_uri}/{timestamp:[[:digit:]]{10}}/{numero}",
		"CommentaireCtl@delete",
	);

	$router->patch(
		"/tentative/{username}/{question_uri}/{timestamp:[[:digit:]]{10}}",
		"ContrôleurFrontal@patch_tentative",
	);
});

$router->group(["middleware" => ["auth_optionnelle", "permissionsRessources", "étatNonInactif"]], function () use (
	$router,
) {
	$router->patch("/user/{username}", "ContrôleurFrontal@patch_user");
});

$router->group(["middleware" => ["auth", "authParMdPOuClé", "permissionsRessources", "étatActif"]], function () use (
	$router,
) {
	// Clé
	$router->post("/user/{username}/cles", "ContrôleurFrontal@post_clé");
});
