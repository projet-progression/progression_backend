<?php
/*
   This file is part of Progression.

   Progression is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Progression is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Progression.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace progression\services\ldap;

use LDAP\Result;
use Illuminate\Support\Facades\Log;
use progression\domaine\interacteur\IntéracteurException;

class LDAPImpl
{
	/**
	 * @return array<mixed>|null
	 */
	function get_user(string $identifiant, string $password, string $domaine = null): array|null
	{
		// Connexion au serveur LDAP
		$ldap = @ldap_connect("ldap://" . config("ldap.hôte"), (int) config("ldap.port"));
		if (!$ldap) {
			Log::error("Erreur de configuration LDAP");
			throw new IntéracteurException("Erreur de configuration LDAP", 500);
		}
		ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
		ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);
		ldap_set_option($ldap, LDAP_OPT_NETWORK_TIMEOUT, (int) config("ldap.timeout"));

		// bind l'utilisateur LDAP
		if (config("ldap.bind.dn") && config("ldap.bind.pw")) {
			$bind = @ldap_bind($ldap, config("ldap.bind.dn"), config("ldap.bind.pw"));
		} else {
			$bind = @ldap_bind($ldap, $identifiant, $password);
		}

		if (!$bind) {
			ldap_get_option($ldap, LDAP_OPT_DIAGNOSTIC_MESSAGE, $extended_error);
			Log::error("Erreur de connexion à LDAP : $extended_error");
			throw new IntéracteurException("Impossible de se connecter au serveur d'authentification", 503);
		}

		//Recherche de l'utilisateur à authentifier
		$result = @ldap_search(
			$ldap,
			base: config("ldap.base") ?: "",
			filter: "(" . config("ldap.uid") . "=$identifiant)",
			attributes: ["dn", "cn", "mail"],
		);

		if ($result instanceof Result) {
			$user = ldap_get_entries($ldap, $result);
			if (
				$user &&
				isset($user["count"]) &&
				$user["count"] == 1 &&
				isset($user[0]) &&
				is_array($user[0]) &&
				isset($user[0]["dn"]) &&
				@ldap_bind($ldap, $user[0]["dn"], $password)
			) {
				return $user;
			}
		}

		return null;
	}
}
