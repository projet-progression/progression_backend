<?php
namespace progression\http\transformer;

use progression\domaine\entité\question\Type;
use progression\http\transformer\dto\QuestionDTO;
use progression\http\transformer\dto\QuestionSeqDTO;
use progression\util\Encodage;
use progression\http\contrôleur\{AvancementCtl, QuestionCtl};
use progression\domaine\interacteur\{ObtenirAvancementInt, ObtenirQuestionInt};

use League\Fractal\Resource\Collection;

class QuestionSeqTransformer extends QuestionTransformer
{
	/** @var string[] */
	protected array $availableIncludes = ["questions"];

	/**
	 * @param QuestionDTO $data_in
	 * @return string[]
	 */
	public function transform(QuestionDTO $data_in): array
	{
		$data_out = array_merge(parent::transform($data_in), [
			"sous_type" => Type::SEQ,
		]);

		return $data_out;
	}

	public function includeQuestions(QuestionSeqDTO $data_in): Collection
	{
		return $this->collection($data_in->questions, new QuestionTransformer(), "question");
	}
}

?>
