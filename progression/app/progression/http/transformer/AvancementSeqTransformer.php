<?php
/*
   This file is part of Progression.

   Progression is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Progression is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Progression.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace progression\http\transformer;

use progression\domaine\entité\AvancementSeq;
use progression\domaine\entité\question\État;
use progression\http\transformer\dto\AvancementSeqDTO;

use League\Fractal\Resource\Collection;

class AvancementSeqTransformer extends BaseTransformer
{
	public string $type = "avancement";

	/** @var array<string> $availableIncludes */
	protected array $availableIncludes = ["avancements"];
	/** @var array<string> $availableParams */
	protected array $availableParams = ["fields"];

	/**
	 * @return array<mixed>
	 */
	public function transform(AvancementSeqDTO $data_in): array
	{
		$id = $data_in->id;
		$avancement = $data_in->objet;
		$avancement = (fn($avancement): AvancementSeq => $avancement)($avancement);
		$liens = $data_in->liens;

		$data_out = [
			"id" => $id,
			"état" => match ($avancement->état) {
				État::DEBUT => "début",
				État::NONREUSSI => "non_réussi",
				État::REUSSI => "réussi",
				default => "indéfini",
			},
			"titre" => $avancement->titre,
			"niveau" => $avancement->niveau,
			"date_modification" => $avancement->date_modification,
			"date_réussite" => $avancement->date_réussite,
			"extra" => $avancement->extra,
			"links" => $liens,
		];

		return $data_out;
	}

	public function includeAvancements(AvancementSeqDTO $data_in): Collection
	{
		return $this->collection($data_in->avancements, new AvancementTransformer(), "avancement");
	}
}
