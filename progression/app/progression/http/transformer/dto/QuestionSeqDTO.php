<?php

namespace progression\http\transformer\dto;

use progression\http\contrôleur\{Contrôleur, QuestionCtl};
use progression\domaine\entité\Avancement;
use progression\domaine\entité\user\User;
use progression\domaine\entité\question\{Question, QuestionSeq};
use progression\util\Encodage;

class QuestionSeqDTO extends QuestionDTO
{
	/** @var array<Question> $questions */

	public array $questions;
	/**
	 * @param mixed $id
	 * @param mixed $objet
	 * @param array<string> $liens
	 */
	public function __construct(mixed $id, mixed $objet, array $liens)
	{
		parent::__construct($id, $objet, $liens);
		$url = Contrôleur::$urlBase;

		$this->questions = [];
		if ($objet instanceof QuestionSeq) {
			foreach ($objet->questions as $uri => $question) {
				$id_sous_question = Encodage::base64_encode_url($uri);
				array_push(
					$this->questions,
					new QuestionDTO(
						id: $id_sous_question,
						objet: $question,
						liens: QuestionCtl::get_liens($id_sous_question),
					),
				);
			}
		}
	}
}
