<?php
/*
  This file is part of Progression.

  Progression is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Progression is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Progression.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace progression\http\transformer\dto;

use progression\http\contrôleur\{BanqueCtl, QuestionCtl};
use progression\domaine\entité\question\Question;
use progression\domaine\entité\Banque;
use progression\util\Encodage;

class BanqueDTO extends GénériqueDTO
{
	/**
	 * @var array<Question>|null $questions
	 */
	public array|null $questions;

	/**
	 * @var array<Banque>|null $banques
	 */
	public array|null $banques;

	public function __construct(string $id, mixed $objet, array $liens)
	{
		parent::__construct($id, $objet, $liens);

		$this->ajouter_questions($objet);

		$this->ajouter_banques($id, $objet);
	}

	private function ajouter_questions(mixed $objet): void
	{
		$this->questions = [];
		foreach ($objet->questions as $question_id => $question) {
			$question_uri = Encodage::base64_encode_url($question_id);
			array_push(
				$this->questions,
				new QuestionDTO(id: $question_uri, objet: $question, liens: QuestionCtl::get_liens($question_uri)),
			);
		}
	}

	private function ajouter_banques(string $id, mixed $objet): void
	{
		$this->banques = [];

		[$username, $banque_uri] = explode("/", $id);

		foreach ($objet->banques as $banque_id => $banque) {
			$sous_banque_uri = $this->construire_id($banque_uri, $banque_id);

			array_push(
				$this->banques,
				new BanqueDTO(
					id: "${username}/${sous_banque_uri}",
					objet: $banque,
					liens: BanqueCtl::get_liens($username, $sous_banque_uri),
				),
			);
		}
	}

	private function construire_id(string $banque_uri, string $banque_id): string
	{
		return Encodage::base64_encode_url(
			Encodage::base64_decode_url($banque_uri) .
				(str_contains(Encodage::base64_decode_url($banque_uri), "#") ? "/" : "#") .
				$banque_id,
		);
	}
}
