<?php
/*
  This file is part of Progression.

  Progression is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Progression is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Progression.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace progression\http\transformer\dto;

use progression\http\contrôleur\AvancementCtl;
use progression\domaine\entité\{Avancement, AvancementSeq};
use progression\util\Encodage;

class AvancementSeqDTO extends GénériqueDTO
{
	/**
	 * @var array<Avancement> $avancements
	 */
	public array $avancements;

	/**
	 * @param array<string> $liens
	 */
	public function __construct(string $username, string $question_uri, AvancementSeq $objet, array $liens)
	{
		$id = $username . "/" . $question_uri;
		parent::__construct($id, $objet, $liens);

		$this->avancements = [];
		foreach ($objet->avancements as $url => $avancement) {
			$avancement_uri = Encodage::base64_encode_url($url);
			$id_avancement = $username . "/" . $avancement_uri;
			array_push(
				$this->avancements,
				new AvancementDTO(
					id: $id_avancement,
					objet: $avancement,
					liens: AvancementCtl::get_liens($username, $avancement_uri),
				),
			);
		}
	}
}
