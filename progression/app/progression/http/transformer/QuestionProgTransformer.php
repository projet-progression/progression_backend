<?php
/*
   This file is part of Progression.

   Progression is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Progression is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Progression.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace progression\http\transformer;

use progression\util\Encodage;
use progression\domaine\entité\question\Type;
use progression\http\transformer\dto\{QuestionDTO, QuestionProgDTO};

class QuestionProgTransformer extends QuestionTransformer
{
	protected array $availableIncludes = ["tests", "ebauches"];

	public function transform(QuestionDTO $question): array
	{
		$data_out = array_merge(parent::transform($question), [
			"sous_type" => Type::PROG,
		]);

		return $data_out;
	}

	public function includeTests(QuestionProgDTO $data_in)
	{
		return $this->collection($data_in->tests, new TestProgTransformer(), "test");
	}

	public function includeEbauches(QuestionProgDTO $data_in)
	{
		return $this->collection($data_in->ébauches, new ÉbaucheTransformer(), "ebauche");
	}
}
