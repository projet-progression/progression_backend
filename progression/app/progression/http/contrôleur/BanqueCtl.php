<?php
/*
  This file is part of Progression.

  Progression is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Progression is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Progression.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace progression\http\contrôleur;

use Illuminate\Http\{JsonResponse, Request};
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Validator as ValidatorImpl;

use progression\domaine\entité\Banque;
use progression\domaine\interacteur\{ObtenirBanqueInt, ObtenirBanquesInt, SauvegarderBanqueInt};
use progression\http\transformer\BanqueTransformer;
use progression\http\transformer\dto\BanqueDTO;
use progression\util\Encodage;

class BanqueCtl extends Contrôleur
{
	public function get(string $username, string $url): JsonResponse
	{
		Log::debug("BanqueCtl.get. Params : ", [$username, $url]);
		$réponse = null;

		[$url_banque, $sous_répertoire] = $this->décoder_url($url);

		$banque = $this->obtenir_banque($username, $url_banque);

		$sous_banque = $this->extraire_sous_banque($banque, $sous_répertoire);

		$url_effectif = Encodage::base64_encode_url(
			$url_banque . (empty($sous_répertoire) ? "" : "#" . $sous_répertoire),
		);

		$réponse = $this->valider_et_préparer_réponse($sous_banque, $username, $url_effectif);

		Log::debug("BanqueCtl.get. Retour : ", [$réponse]);
		return $réponse;
	}

	/**
	 * @param array<mixed> $attributs
	 * @return JsonResponse
	 */
	public function put(string $username, string $url, array $attributs): JsonResponse
	{
		Log::debug("BanqueCtl.put Params : ", [$username, $attributs]);

		$validateur = $this->valider_paramètres($attributs, $url);
		$chemin = Encodage::base64_decode_url($url);

		$réponse = null;
		if ($validateur->fails()) {
			$réponse = $this->réponse_json(["erreur" => $validateur->errors()], 400);
		} else {
			$banque = $this->construire_banque($attributs);
			$créerBanqueInt = new SauvegarderBanqueInt();
			$banque_retournée = $créerBanqueInt->sauvegarder($username, $chemin, $banque);

			$id = array_key_first($banque_retournée);
			$réponse = $this->valider_et_préparer_réponse(
				$banque_retournée[$id],
				$username,
				Encodage::base64_encode_url($id),
			);
		}

		Log::debug("BanqueCtl.put Retour : ", [$réponse]);

		return $réponse;
	}

	private function obtenir_banque(string $username, string $url): Banque|null
	{
		Log::debug("BanqueCtl.obtenir_banque. Params : ", [$username, $url]);

		$banquesInt = new ObtenirBanqueInt();

		$banque = $banquesInt->get_banque($username, $url, $this->get_includes());

		Log::debug("BanqueCtl.obtenir_banque. Retour : ", [$banque]);
		return $banque;
	}

	/**
	 * @return array<int, mixed>
	 */
	private function décoder_url(string $url): array
	{
		$chemin = Encodage::base64_decode_url($url);

		$fragment = parse_url($chemin, PHP_URL_FRAGMENT);

		if ($fragment !== null && $fragment !== false) {
			$chemin = substr($chemin, 0, strlen($chemin) - strlen($fragment) - 1);
			$fragment = rtrim($fragment, "/");
		}

		return [$chemin, $fragment];
	}

	private function extraire_sous_banque(Banque|null $banque, string|null $sous_répertoire): Banque|null
	{
		if ($banque == null || empty($sous_répertoire)) {
			return $banque;
		}

		if (str_starts_with($sous_répertoire, "/")) {
			$sous_répertoire = substr($sous_répertoire, 1);
		}

		$pos_slash = strpos($sous_répertoire, "/");

		$sous_répertoire = rawurldecode($sous_répertoire);
		if ($pos_slash === false || $pos_slash == strlen($sous_répertoire) - 1) {
			return array_key_exists($sous_répertoire, $banque->banques) ? $banque->banques[$sous_répertoire] : null;
		}

		$racine = substr($sous_répertoire, 0, $pos_slash);
		$restant = substr($sous_répertoire, $pos_slash);

		return array_key_exists($racine, $banque->banques)
			? $this->extraire_sous_banque($banque->banques[$racine], $restant)
			: null;
	}

	/**
	 * @param array<mixed> $attributs
	 * @return Banque
	 */
	private function construire_banque(array $attributs): Banque
	{
		return new Banque(nom: $attributs["nom"] ?? null);
	}

	private function valider_et_préparer_réponse(Banque|null $banque, string $username, string $url): JsonResponse
	{
		Log::debug("BanqueCtl.valider_et_préparer_réponse. Params : ", [$banque, $username, $url]);

		if ($banque) {
			$dto = new BanqueDTO(
				id: "{$username}/{$url}",
				objet: $banque,
				liens: BanqueCtl::get_liens($username, $url),
			);

			$réponse = $this->item($dto, new BanqueTransformer());
		} else {
			$réponse = null;
		}

		$réponse = $this->préparer_réponse($réponse);

		Log::debug("BanqueCtl.valider_et_préparer_réponse. Retour : ", [$réponse]);
		return $réponse;
	}

	/**
	 * @return array<string>
	 */
	public static function get_liens(string $username, string $url): array
	{
		$urlBase = Contrôleur::$urlBase;
		return [
			"self" => "{$urlBase}/banque/{$username}/{$url}",
			"user" => "{$urlBase}/user/{$username}",
		];
	}

	/**
	 * @param array<mixed> $attributs
	 */
	private function valider_paramètres(array $attributs, string $url): ValidatorImpl
	{
		$validateur = Validator::make(
			array_merge($attributs, ["url" => $url]),
			[
				"url" => [
					"required",
					function ($attribute, $value, $fail) {
						$url = Encodage::base64_decode_url($value);
						if (!$url || Validator::make(["url" => $url], ["url" => "url"])->fails()) {
							$fail("Le champ url doit être un URL encodé en base64.");
						}
					},
				],
			],
			[
				"required" => "Le champ :attribute est obligatoire.",
			],
		);

		return $validateur;
	}
}
