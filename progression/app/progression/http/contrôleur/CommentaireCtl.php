<?php
/*
   This file is part of Progression.

   Progression is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Progression is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Progression.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace progression\http\contrôleur;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\{Auth, Gate, Log};
use Illuminate\Support\Facades\Validator;
use progression\domaine\entité\Commentaire;
use progression\domaine\entité\Résultat;
use progression\domaine\interacteur\{
	ObtenirCommentaireInt,
	CréerCommentaireInt,
	SauvegarderCommentaireInt,
	IntéracteurException,
	ObtenirUserInt,
	SupprimerCommentaireInt,
};
use progression\http\transformer\CommentaireTransformer;
use progression\http\transformer\dto\GénériqueDTO;
use progression\util\Encodage;
use Carbon\Carbon;

class CommentaireCtl extends Contrôleur
{
	/**
	 * @param array<mixed> $attributs
	 */
	public function post(string $username, string $question_uri, int $timestamp, array $attributs): JsonResponse
	{
		Log::debug("CommentaireCtl.post. Params : ", [$username, $question_uri, $timestamp, $attributs]);
		$chemin = Encodage::base64_decode_url($question_uri);

		$commentaire = null;
		$réponse = null;

		$validateur = $this->valider_paramètres($attributs);
		if ($validateur->fails()) {
			$réponse = $this->réponse_json(["erreur" => $validateur->errors()], 400);
		} else {
			/* @phpstan-ignore-next-line https://git.dti.crosemont.quebec/progression/progression_backend/-/issues/231 */
			$créateur = (new ObtenirUserInt())->get_user($attributs["créateur"] ?? Auth::user()->username);

			if (!Gate::allows("créer-commentaire", $créateur)) {
				$réponse = $this->réponse_json(["erreur" => "Opération interdite."], 403);
			} else {
				if (!$créateur) {
					$réponse = $this->réponse_json(["erreur" => "Créateur inexistant."], 400);
				} else {
					$commentaireInt = new CréerCommentaireInt();
					$commentaire = $commentaireInt->créer_commentaire(
						$username,
						$chemin,
						$timestamp,
						new Commentaire(
							$attributs["message"],
							$créateur,
							Carbon::now()->getTimestamp(),
							$attributs["numéro_ligne"],
						),
					);

					$id = array_key_first($commentaire);
					$réponse = $this->valider_et_préparer_réponse(
						$commentaire[$id],
						$username,
						$question_uri,
						$timestamp,
						$id,
					);
				}
			}
		}

		Log::debug("CommentaireCtl.post. Retour : ", [$réponse]);
		return $réponse;
	}

	public function delete(string $username, string $question_uri, int $timestamp, int $numero): JsonResponse
	{
		$chemin = Encodage::base64_decode_url($question_uri);

		$commentaire = (new ObtenirCommentaireInt())->get_commentaire($username, $chemin, $timestamp, $numero);
		if (!$commentaire) {
			return $this->réponse_json(code: 204);
		}

		if (!Gate::allows("supprimer-commentaire", $commentaire->créateur)) {
			$réponse = $this->réponse_json(["erreur" => "Opération interdite."], 403);
		} else {
			(new SupprimerCommentaireInt())->supprimer_commentaire($username, $chemin, $timestamp, $numero);
			$réponse = $this->réponse_json(code: 204);
		}

		return $réponse;
	}

	/**
	 * @return array<string>
	 */
	public static function get_liens(string $id, int $numéro, Commentaire $commentaire): array
	{
		$urlBase = Contrôleur::$urlBase;

		return [
			"self" => "{$urlBase}/commentaire/{$id}/{$numéro}",
			"auteur" => "{$urlBase}/user/{$commentaire->créateur->username}",
			"tentative" => "{$urlBase}/tentative/{$id}",
		];
	}

	/**
	 * @param array<mixed> $params
	 */
	private function valider_paramètres(array $params)
	{
		return Validator::make(
			$params,
			[
				"message" => "required",
				"créateur" => "string",
				"numéro_ligne" => ["required", "integer"],
			],
			[
				"required" => "Le champ :attribute est obligatoire.",
				"integer" => "Le champ :attribute doit être un entier.",
			],
		);
	}

	private function valider_et_préparer_réponse(
		Commentaire $commentaire,
		string $username,
		string $question_uri,
		int $timestamp,
		int $numéro,
	): JsonResponse {
		Log::debug("CommentaireCtl.valider_et_préparer_réponse. Params : ", [
			$commentaire,
			$username,
			$question_uri,
			$timestamp,
		]);

		$dto = new GénériqueDTO(
			id: "{$username}/{$question_uri}/{$timestamp}/{$numéro}",
			objet: $commentaire,
			liens: CommentaireCtl::get_liens("{$username}/{$question_uri}/{$timestamp}", $numéro, $commentaire),
		);

		$réponse = $this->item($dto, new CommentaireTransformer());

		$réponse = $this->préparer_réponse($réponse);

		Log::debug("CommentaireCtl.valider_et_préparer_réponse. Retour : ", [$réponse]);

		return $réponse;
	}
}
