<?php
/*
   This file is part of Progression.

   Progression is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Progression is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Progression.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace progression\http\contrôleur;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\{Auth, Log};
use progression\domaine\entité\question\{QuestionProg, QuestionSys, QuestionBD, QuestionSeq};
use progression\domaine\entité\user\User;
use progression\domaine\interacteur\{ObtenirQuestionInt, IntéracteurException};
use progression\http\transformer\{QuestionProgTransformer, QuestionSysTransformer, QuestionSeqTransformer};
use progression\http\transformer\dto\{QuestionDTO, QuestionProgDTO, QuestionSeqDTO};
use progression\util\Encodage;

class QuestionCtl extends Contrôleur
{
	public function get(string $question_uri): JsonResponse
	{
		Log::debug("QuestionCtl.get. Params : ", [$question_uri]);

		$réponse = null;
		$question = $this->obtenir_question($question_uri);
		$réponse = $this->valider_et_préparer_réponse($question, $question_uri);

		Log::debug("QuestionCtl.get. Retour : ", [$réponse]);

		return $réponse;
	}

	/**
	 * @return array<string>
	 */
	public static function get_liens(string $question_uri): array
	{
		$urlBase = Contrôleur::$urlBase;

		$user = Auth::user();

		$liens = [
			"self" => "$urlBase/question/$question_uri",
			"résultats" => "$urlBase/question/$question_uri/resultats",
		];

		if ($user) {
			/* @phpstan-ignore-next-line https://git.dti.crosemont.quebec/progression/progression_backend/-/issues/231 */
			$liens["avancement"] = "$urlBase/avancement/{$user->username}/$question_uri";
		}

		return $liens;
	}

	private function obtenir_question($question_uri)
	{
		Log::debug("QuestionCtl.obtenir_question. Params : ", [$question_uri]);

		$chemin = Encodage::base64_decode_url($question_uri);

		$questionInt = new ObtenirQuestionInt();
		$question = $questionInt->get_question(question_id: $chemin, includes: $this->get_includes());

		Log::debug("Question.Ctl.obtenir_question. Retour : ", [$question]);
		return $question;
	}

	private function valider_et_préparer_réponse($question, $uri)
	{
		Log::debug("QuestionCtl.valider_et_préparer_réponse. Params : ", [$question]);

		if ($question === null) {
			$réponse = $this->préparer_réponse(null);
		} elseif ($question instanceof QuestionProg) {
			$dto = new QuestionProgDTO(id: $uri, objet: $question, liens: QuestionCtl::get_liens($uri));
			$réponse_array = $this->item($dto, new QuestionProgTransformer());
			$réponse = $this->préparer_réponse($réponse_array);
		} elseif ($question instanceof QuestionSys) {
			$dto = new QuestionDTO(id: $uri, objet: $question, liens: QuestionCtl::get_liens($uri));
			$réponse_array = $this->item($dto, new QuestionSysTransformer());
			$réponse = $this->préparer_réponse($réponse_array);
		} elseif ($question instanceof QuestionSeq) {
			$dto = new QuestionSeqDTO(id: $uri, objet: $question, liens: QuestionCtl::get_liens($uri));
			$réponse_array = $this->item($dto, new QuestionSeqTransformer());
			$réponse = $this->préparer_réponse($réponse_array);
		} elseif ($question instanceof QuestionBD) {
			$réponse = $this->réponse_json(["erreur" => "QuestionBD pas encore implémentée"], 501);
		} else {
			$réponse = $this->réponse_json(["erreur" => "Type de question inconnu."], 400);
		}

		Log::debug("QuestionCtl.valider_et_préparer_réponse. Retour : ", [$réponse]);
		return $réponse;
	}
}
