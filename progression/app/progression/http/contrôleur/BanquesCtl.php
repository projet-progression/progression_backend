<?php
/*
  This file is part of Progression.

  Progression is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Progression is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Progression.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace progression\http\contrôleur;

use Illuminate\Http\{JsonResponse, Request};
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Validator as ValidatorImpl;

use progression\domaine\entité\Banque;
use progression\domaine\interacteur\{ObtenirBanqueInt, ObtenirBanquesInt, CréerBanqueInt};
use progression\http\transformer\BanqueTransformer;
use progression\http\transformer\dto\BanqueDTO;
use progression\util\Encodage;

class BanquesCtl extends Contrôleur
{
	/**
	 * @param string $username
	 */
	public function get(string $username): JsonResponse
	{
		Log::debug("BanquesCtl.get. Params : ", [$username]);

		$réponse = null;
		$banques = $this->obtenir_banques($username);
		$réponse = $this->valider_et_préparer_réponse($banques, $username);

		Log::debug("BanquesCtl.get. Retour : ", [$réponse]);
		return $réponse;
	}

	/**
	 * @return array<int, Banque>
	 */
	private function obtenir_banques(string $username): array
	{
		Log::debug("BanquesCtl.obtenir_banques. Params : ", [$username]);

		$banquesInt = new ObtenirBanquesInt();

		$banques = $banquesInt->get_banques($username, $this->get_includes());

		Log::debug("BanquesCtl.obtenir_banques. Retour : ", [$banques]);
		return $banques;
	}

	/**
	 * @param array<int, Banque> $banques
	 */
	private function valider_et_préparer_réponse(array $banques, string $username): JsonResponse
	{
		Log::debug("BanquesCtl.valider_et_préparer_réponse. Params : ", [$banques, $username]);

		$dtos = [];
		foreach ($banques as $url => $banque) {
			$id = Encodage::base64_encode_url($url);
			$dtos[] = new BanqueDTO(
				id: "{$username}/{$id}",
				objet: $banque,
				liens: BanqueCtl::get_liens($username, $id),
			);
		}

		$réponse = $this->préparer_réponse($this->collection($dtos, new BanqueTransformer()));

		Log::debug("BanquesCtl.valider_et_préparer_réponse. Retour : ", [$réponse]);
		return $réponse;
	}
}
