<?php
/*
   This file is part of Progression.

   Progression is free software: you can redistribute it and/or modify
.   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Progression is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Progression.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace progression\http\contrôleur;

use Illuminate\Http\{Request, JsonResponse};
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Gate;

class ContrôleurFrontal extends Contrôleur
{
	public function put_avancement(Request $request, string $username, string $question_uri): JsonResponse
	{
		$avancement = self::décoder_objet($request);

		if (isset($request["data"]["id"]) && $request["data"]["id"] != "${username}/${question_uri}") {
			return $this->préparer_réponse(["erreur" => ["id" => ["Le champ id ne correspond pas à l'URI."]]], 409);
		}

		/**
		 * @var AvancementCtl|null $contrôleur;
		 */
		$contrôleur = $this->get_contrôleur($request);

		if (!$contrôleur) {
			return $this->préparer_réponse(
				["erreur" => ["type" => ["Le champ type est manquant ou ne correspond pas à un type valide"]]],
				409,
			);
		}

		return $contrôleur->post($username, $question_uri, $avancement);
	}

	public function post_avancement(Request $request, string $username): JsonResponse
	{
		$avancement = self::décoder_objet($request);

		if (!isset($request["data"]["id"])) {
			return $this->préparer_réponse(["erreur" => ["id" => ["Le champ id est obligatoire."]]], 400);
		}

		if (!preg_match("/${username}\/[a-zA-Z0-9_-]+/", $request["data"]["id"])) {
			return $this->préparer_réponse(
				["erreur" => ["id" => ["Le champ id doit avoir le format {username}/{question_uri}."]]],
				400,
			);
		}

		$question_uri = explode("/", $request["data"]["id"])[1];

		/**
		 * @var AvancementCtl|null $contrôleur;
		 */
		$contrôleur = $this->get_contrôleur($request);
		if (!$contrôleur) {
			return $this->préparer_réponse(
				["erreur" => ["type" => ["Le champ type est manquant ou ne correspond pas à un type valide"]]],
				409,
			);
		}

		return $contrôleur->post($username, $question_uri, $avancement);
	}

	public function post_commentaire(
		Request $request,
		string $username,
		string $question_uri,
		int $timestamp,
	): JsonResponse {
		$commentaire = self::décoder_objet($request);

		/**
		 * @var CommentaireCtl|null $contrôleur;
		 */
		$contrôleur = $this->get_contrôleur($request);

		if (!$contrôleur) {
			return $this->préparer_réponse(
				["erreur" => ["type" => ["Le champ type est manquant ou ne correspond pas à un type valide"]]],
				409,
			);
		}

		return $contrôleur->post($username, $question_uri, $timestamp, $commentaire);
	}

	public function post_clé(Request $request, string $username): JsonResponse
	{
		$clé = self::décoder_objet($request);

		/**
		 * @var CléCtl|null $contrôleur;
		 */
		$contrôleur = $this->get_contrôleur($request);

		if (!$contrôleur) {
			return $this->préparer_réponse(
				["erreur" => ["type" => ["Le champ type est manquant ou ne correspond pas à un type valide"]]],
				409,
			);
		}

		return $contrôleur->post($username, $clé);
	}

	public function post_user(Request $request): JsonResponse
	{
		Log::info("{$request->ip()} - Tentative d'inscription : {$request->input("username")}");

		$user = self::décoder_objet($request);

		/**
		 * @var UserCtl|null $contrôleur;
		 */
		$contrôleur = $this->get_contrôleur($request);

		if (!$contrôleur) {
			return $this->préparer_réponse(
				["erreur" => ["type" => ["Le champ type est manquant ou ne correspond pas à un type valide"]]],
				409,
			);
		}

		return $contrôleur->post($user);
	}

	public function put_user(Request $request, string $username): JsonResponse
	{
		$user = self::décoder_objet($request);

		if (isset($request["data"]["id"]) && $request["data"]["id"] != $username) {
			return $this->préparer_réponse(["erreur" => ["id" => ["Le champ id ne correspond pas à l'URI."]]], 409);
		}

		/**
		 * @var UserCtl|null $contrôleur;
		 */
		$contrôleur = $this->get_contrôleur($request);
		if (!$contrôleur) {
			return $this->préparer_réponse(
				["erreur" => ["type" => ["Le champ type est manquant ou ne correspond pas à un type valide"]]],
				409,
			);
		}

		return $contrôleur->put($username, $user);
	}

	public function patch_user(Request $request, string $username): JsonResponse
	{
		$user = self::décoder_objet($request);

		/**
		 * @var UserCtl|null $contrôleur;
		 */
		$contrôleur = $this->get_contrôleur($request);
		if (!$contrôleur) {
			return $this->préparer_réponse(
				["erreur" => ["type" => ["Le champ type est manquant ou ne correspond pas à un type valide"]]],
				409,
			);
		}

		if (
			(isset($request->input("data")["attributes"]["password"]) && !Gate::allows("modifier-mdp", $request)) ||
			(isset($request->input("data")["attributes"]["état"]) &&
				!Gate::allows("utilisateur-actif", $request) &&
				!Gate::allows("activation", $request))
		) {
			return $this->préparer_réponse(["erreur" => "Opération interdite."], 403);
		}

		return $contrôleur->patch($username, $user);
	}

	public function patch_avancement(Request $request, string $username, string $question_uri): JsonResponse
	{
		$avancement = self::décoder_objet($request);

		/**
		 * @var AvancementCtl|null $contrôleur;
		 */
		$contrôleur = $this->get_contrôleur($request);
		if (!$contrôleur) {
			return $this->préparer_réponse(
				["erreur" => ["type" => ["Le champ type est manquant ou ne correspond pas à un type valide"]]],
				409,
			);
		}

		return $contrôleur->patch($username, $question_uri, $avancement);
	}

	public function post_résultat(Request $request, string $uri): JsonResponse
	{
		$résultat = self::décoder_objet($request);

		/**
		 * @var RésultatCtl|null $contrôleur;
		 */
		$contrôleur = $this->get_contrôleur($request);

		if (!$contrôleur) {
			return $this->préparer_réponse(
				["erreur" => ["type" => ["Le champ type est manquant ou ne correspond pas à un type valide"]]],
				409,
			);
		}

		return $contrôleur->post($uri, $résultat);
	}

	public function post_sauvegarde(Request $request, string $username, string $question_uri): JsonResponse
	{
		$sauvegarde = self::décoder_objet($request);

		/**
		 * @var SauvegardeCtl|null $contrôleur;
		 */
		$contrôleur = $this->get_contrôleur($request);

		if (!$contrôleur) {
			return $this->préparer_réponse(
				["erreur" => ["type" => ["Le champ type est manquant ou ne correspond pas à un type valide"]]],
				409,
			);
		}

		return $contrôleur->post($username, $question_uri, $sauvegarde);
	}

	public function post_tentative(Request $request, string $username, string $question_uri): JsonResponse
	{
		$tentative = self::décoder_objet($request);

		/**
		 * @var TentativeCtl|null $contrôleur;
		 */
		$contrôleur = $this->get_contrôleur($request);

		if (!$contrôleur) {
			return $this->préparer_réponse(
				["erreur" => ["type" => ["Le champ type est manquant ou ne correspond pas à un type valide"]]],
				409,
			);
		}

		return $contrôleur->post($username, $question_uri, $tentative);
	}

	public function patch_tentative(
		Request $request,
		string $username,
		string $question_uri,
		int $timestamp,
	): JsonResponse {
		$tentative = self::décoder_objet($request);

		/**
		 * @var TentativeCtl|null $contrôleur;
		 */
		$contrôleur = $this->get_contrôleur($request);
		if (!$contrôleur) {
			return $this->préparer_réponse(
				["erreur" => ["type" => ["Le champ type est manquant ou ne correspond pas à un type valide"]]],
				409,
			);
		}

		return $contrôleur->patch($username, $question_uri, $timestamp, $tentative);
	}

	public function post_token(Request $request, string $username): JsonResponse
	{
		$token = self::décoder_objet($request);

		/**
		 * @var TokenCtl|null $contrôleur;
		 */
		$contrôleur = $this->get_contrôleur($request);

		if (!$contrôleur) {
			return $this->préparer_réponse(
				["erreur" => ["type" => ["Le champ type est manquant ou ne correspond pas à un type valide"]]],
				409,
			);
		}

		//Interdit l'authentification par token pour générer un token identification
		if (
			empty($request->input("data")["attributes"]["ressources"]) &&
			!Gate::allows("utilisateur-auth-par-mdp-ou-clé", $request)
		) {
			return $this->préparer_réponse(["erreur" => "Opération interdite."], 403);
		}

		return $contrôleur->post($username, $token);
	}

	public function post_banque(Request $request, string $username): JsonResponse
	{
		$banque = self::décoder_objet($request);

		if (!isset($request["data"]["id"])) {
			return $this->préparer_réponse(["erreur" => ["id" => ["Le champ id est obligatoire."]]], 400);
		}

		if (!preg_match("/${username}\/[a-zA-Z0-9_-]+/", $request["data"]["id"])) {
			return $this->préparer_réponse(
				["erreur" => ["id" => ["Le champ id doit avoir le format {username}/{url}."]]],
				400,
			);
		}

		$url = explode("/", $request["data"]["id"])[1];

		/**
		 * @var BanqueCtl|null $contrôleur;
		 */
		$contrôleur = $this->get_contrôleur($request);

		if (!$contrôleur) {
			return $this->préparer_réponse(
				["erreur" => ["type" => ["Le champ type est manquant ou ne correspond pas à un type valide"]]],
				409,
			);
		}

		return $contrôleur->put($username, $url, $banque);
	}

	public function put_banque(Request $request, string $username, string $url): JsonResponse
	{
		$banque = self::décoder_objet($request);

		/**
		 * @var BanqueCtl|null $contrôleur;
		 */
		$contrôleur = $this->get_contrôleur($request);

		if (!$contrôleur) {
			return $this->préparer_réponse(
				["erreur" => ["type" => ["Le champ type est manquant ou ne correspond pas à un type valide"]]],
				409,
			);
		}

		return $contrôleur->put($username, $url, $banque);
	}

	/**
	 * @return array<mixed>
	 */
	private function décoder_objet(Request $request): array
	{
		$data = $request["data"] ?? [];
		if (
			in_array($data["type"] ?? "", [
				"avancement",
				"commentaire",
				"cle",
				"user",
				"resultat",
				"sauvegarde",
				"token",
				"tentative",
				"banque",
			])
		) {
			return $data["attributes"] ?? [];
		} else {
			return [];
		}
	}

	private function get_contrôleur(Request $request): Contrôleur|null
	{
		$type = $request->data["type"] ?? "";
		if ($type == "avancement") {
			return new AvancementCtl();
		} elseif ($type == "avancements") {
			return new AvancementsCtl();
		} elseif ($type == "commentaire") {
			return new CommentaireCtl();
		} elseif ($type == "banque") {
			return new BanqueCtl();
		} elseif ($type == "banques") {
			return new BanquesCtl();
		} elseif ($type == "cle") {
			return new CléCtl();
		} elseif ($type == "user") {
			return new UserCtl();
		} elseif ($type == "resultat") {
			return new RésultatCtl();
		} elseif ($type == "sauvegarde") {
			return new SauvegardeCtl();
		} elseif ($type == "token") {
			return new TokenCtl();
		} elseif ($type == "tentative") {
			return new TentativeCtl();
		} elseif ($type == "user") {
			return new UserCtl();
		} else {
			return null;
		}
	}
}
?>
