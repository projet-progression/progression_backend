<?php
/*
   This file is part of Progression.

   Progression is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Progression is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Progression.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace progression\dao\question;

use progression\domaine\entité\question\QuestionProg;
use progression\domaine\entité\{Exécutable, TestProg};
use DomainException;

class DécodeurQuestionProg
{
	/**
	 * @param array<mixed> $infos_question;
	 * @param array<string> $includes;
	 */
	public static function load(array $infos_question, array $includes = []): QuestionProg
	{
		$question = new QuestionProg();
		DécodeurQuestion::load($question, $infos_question);

		$question->exécutables = self::load_exécutables($infos_question);
		$question->tests = self::load_tests($infos_question);

		return $question;
	}

	protected static function load_exécutables($infos_question)
	{
		$exécutables = [];
		foreach ($infos_question["ébauches"] as $lang => $code) {
			$exécutables[$lang] = new Exécutable($code, $lang);
		}
		return $exécutables;
	}

	protected static function load_tests($infos_question)
	{
		$tests = [];
		foreach ($infos_question["tests"] as $test) {
			$tests[] = new TestProg(
				$test["nom"] ?? "",
				$test["sortie"] ?? "",
				$test["entrée"] ?? "",
				$test["params"] ?? "",
				$test["rétroactions"]["positive"] ?? null,
				$test["rétroactions"]["négative"] ?? null,
				$test["rétroactions"]["erreur"] ?? null,
				($test["caché"] ?? false) == "true",
			);
		}

		return $tests;
	}
}
