<?php
namespace progression\dao\question;

use progression\dao\DAOFactory;
use progression\domaine\entité\question\QuestionSeq;
use progression\util\Encodage;
use DomainException;

class DécodeurQuestionSeq
{
	/**
	 * @param array<mixed> $infos_question
	 * @param array<string> $includes
	 */
	public static function load(array $infos_question, array $includes = []): QuestionSeq
	{
		$question = new QuestionSeq();
		DécodeurQuestion::load($question, $infos_question);

		if (in_array("questions", $includes)) {
			$séquence = $infos_question["séquence"];
			foreach ($séquence as $url) {
				$question->questions[$url] = DAOFactory::getInstance()->get_question_dao()->get_question($url);
			}
		}

		return $question;
	}
}

?>
