<?php
/*
  This file is part of Progression.

  Progression is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Progression is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Progression.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace progression\dao\banque;

use progression\dao\{DAOFactory, EntitéDAO, DAOException};
use progression\dao\banque\ChargeurBanqueFichier;
use progression\dao\models\{BanqueMdl, UserMdl};
use progression\dao\chargeur\{ChargeurFactory, ChargeurArchive, ChargeurException};
use progression\domaine\entité\Banque;
use progression\domaine\entité\question\Question;
use progression\domaine\interacteur\IntégritéException;
use progression\util\Encodage;

use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;
use \BadMethodCallException;
use \DomainException;
use \InvalidArgumentException;

class BanqueDAO extends EntitéDAO
{
	public function __construct(DAOFactory $source = null)
	{
		parent::__construct($source);

		ChargeurFactory::get_instance()->set_chargeur_fichier(new ChargeurBanqueFichier());
		ChargeurFactory::get_instance()->set_chargeur_archive(new ChargeurArchive());
	}

	/**
	 * @param array<string> $includes
	 */
	public function get_banque(string $username, string $url, array $includes = []): Banque|null
	{
		try {
			$data = BanqueMdl::select("banque.*")
				->join("user", "banque.user_id", "=", "user.id")
				->where("user.username", $username)
				->where("banque.url", $url)
				->first();
		} catch (QueryException $e) {
			throw new DAOException($e);
		}

		$banque = $this->construire($this->charger_banque($url), $includes);

		if (!empty($data["nom"])) {
			$banque->nom = $data["nom"];
		}

		return $banque;
	}

	/**
	 * @param array<string> $includes
	 * @return array<int|string, Banque>
	 */
	public function get_toutes(string $username, array $includes = []): array
	{
		$banques = [];

		try {
			$items = BanqueMdl::select("banque.*")
				->join("user", "banque.user_id", "=", "user.id")
				->where("user.username", $username)
				->get();
		} catch (QueryException $e) {
			throw new DAOException($e);
		}

		foreach ($items as $data) {
			$url = $data["url"];

			$data_banque = in_array("banques", $includes)
				? $this->charger_banque($url)
				: [
					"nom" => $data["nom"] ?? null,
					"url" => $data["url"] ?? null,
				];

			$data_banque["nom"] = $data["nom"] ?? $data_banque["nom"];

			$banques[$url] = $this->construire($data_banque);
		}

		return $banques;
	}

	/**
	 * @return array<int|string, Banque>
	 */
	public function save(string $username, string $url, Banque $banque): array
	{
		if (empty($url)) {
			throw new InvalidArgumentException("L'url ne peut être vide.");
		}
		try {
			$user = UserMdl::query()->where("username", $username)->first();
		} catch (QueryException $e) {
			throw new DAOException($e);
		}

		if (!$user) {
			throw new IntégritéException("Impossible de sauvegarder la ressource; le parent n'existe pas.");
		}

		$objet = [
			"user_id" => $user->id,
			"nom" => $banque->nom,
		];

		try {
			$data = BanqueMdl::query()->updateOrCreate(["url" => $url], $objet);
		} catch (QueryException $e) {
			throw new DAOException($e);
		}

		$banque = $this->construire(["nom" => $data["nom"]]);
		$banque->nom = $data["nom"] ?? $banque->nom;

		return [$url => $banque];
	}

	/**
	 * @param array<mixed> $data
	 * @param array<string> $includes
	 */
	public static function construire(array $data, array $includes = []): Banque
	{
		$banque = new Banque(nom: $data["nom"] ?? null);

		if (!empty($data["questions"])) {
			foreach (self::extraire_questions($data["questions"], $includes) as $url_question => $question_item) {
				$banque->ajouterQuestion($url_question, $question_item);
			}
		}

		if (!empty($data["banques"])) {
			foreach (self::extraire_banques($data["banques"], $includes) as $url_banque => $banque_item) {
				$banque->ajouterBanque($url_banque, $banque_item);
			}
		}

		return $banque;
	}

	/**
	 * @return array<mixed>
	 */
	public static function charger_banque(string $url): array
	{
		$chargeur = null;
		$scheme = parse_url($url, PHP_URL_SCHEME);
		$path = parse_url($url, PHP_URL_PATH);
		$extension = pathinfo($path ?: "", PATHINFO_EXTENSION);

		if ($scheme == "file") {
			$chargeur = ChargeurFactory::get_instance()->get_chargeur_banque_fichier();
		} elseif ($extension == "git") {
			$chargeur = ChargeurFactory::get_instance()->get_chargeur_banque_git();
		} elseif ($scheme == "https") {
			$chargeur = ChargeurFactory::get_instance()->get_chargeur_banque_http();
		} else {
			throw new ChargeurException("Schéma d'URI invalide");
		}

		return $chargeur->récupérer_fichier($url);
	}

	/**
	 * @param array<array<string>> $questions
	 * @param array<string> $includes
	 * @return array<string, Question>
	 */
	public static function extraire_questions(array $questions, array $includes = []): array
	{
		$items = [];
		foreach ($questions as $contenu) {
			if (in_array("questions", $includes)) {
				if (empty($contenu["url"])) {
					throw new ChargeurException(
						"La question {" . ($contenu["titre"] ?? "<sans titre>") . "} est invalide.",
					);
				}
				$question = DAOFactory::getInstance()->get_question_dao()->get_question($contenu["url"]);
			} else {
				$question = new Question(
					titre: $contenu["titre"] ?? "",
					niveau: $contenu["niveau"] ?? null,
					description: $contenu["description"] ?? null,
					objectif: $contenu["objectif"] ?? null,
				);
			}

			$items[$contenu["url"]] = $question;
		}
		return $items;
	}

	/**
	 * @param array<mixed> $banques
	 * @param array<string> $includes
	 * @return array<int|string, Banque>
	 */
	public static function extraire_banques(array $banques, array $includes = []): array
	{
		$items = [];
		foreach ($banques as $numéro => $contenu) {
			if (array_key_exists("url", $contenu)) {
				$chemin = "";
				$id = $contenu["url"];
			} else {
				$chemin = Encodage::url_encode($contenu["nom"] ?? $numéro);
				$id = $chemin;
			}

			if (in_array("banques", $includes)) {
				$data_banque = array_key_exists("url", $contenu) ? self::charger_banque($contenu["url"]) : $contenu;
			} else {
				$data_banque = array_key_exists("url", $contenu)
					? ["nom" => $contenu["nom"] ?? null, "url" => $contenu["url"] ?? null]
					: $contenu;
			}

			$banque = self::construire($data_banque);
			$banque->nom = $contenu["nom"] ?? $banque->nom;

			$items[$id] = $banque;
		}
		return $items;
	}
}
