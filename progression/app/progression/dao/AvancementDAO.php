<?php
/*
   This file is part of Progression.

   Progression is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Progression is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Progression.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace progression\dao;

use progression\domaine\entité\{Avancement, AvancementSeq};
use progression\domaine\entité\question\Type;
use progression\dao\models\{AvancementMdl, UserMdl};
use progression\dao\tentative\{TentativeProgDAO, TentativeSysDAO};
use progression\domaine\interacteur\IntégritéException;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\Collection;

class AvancementDAO extends EntitéDAO
{
	/**
	 * @return array<Avancement>
	 */
	public function get_tous($username, $includes = []): array
	{
		try {
			return $this->construire(
				AvancementMdl::select("avancement.*")
					->with(in_array("tentatives", $includes) ? ["tentatives_prog", "tentatives_sys"] : [])
					->with(in_array("sauvegardes", $includes) ? ["sauvegardes"] : [])
					->join("user", "avancement.user_id", "=", "user.id")
					->where("user.username", $username)
					->get(),
				$username,
				$includes,
			);
		} catch (QueryException $e) {
			throw new DAOException($e);
		}
	}

	public function get_avancement($username, $question_uri, $includes = []): Avancement|null
	{
		try {
			$data = AvancementMdl::select("avancement.*")
				->with(in_array("tentatives", $includes) ? ["tentatives_prog", "tentatives_sys"] : [])
				->with(in_array("sauvegardes", $includes) ? ["sauvegardes"] : [])
				->join("user", "avancement.user_id", "=", "user.id")
				->where("user.username", $username)
				->where("avancement.question_uri", $question_uri)
				->first();

			return self::premier_élément($this->construire([$data], $username, $includes));
		} catch (QueryException $e) {
			throw new DAOException($e);
		}
	}

	/**
	 * @param array<string> $includes
	 * @return array<Avancement>
	 */
	public function save(
		string $username,
		string $question_uri,
		string $type,
		Avancement $avancement,
		array $includes = [],
	): array {
		try {
			$user = UserMdl::query()->where("username", $username)->first();

			if (!$user) {
				throw new IntégritéException("Impossible de sauvegarder la ressource; le parent n'existe pas.");
			}

			$objet = [
				"question_uri" => $question_uri,
				"état" => $avancement->état,
				"type" => $type,
				"titre" => $avancement->titre,
				"niveau" => $avancement->niveau,
				"date_modification" => $avancement->date_modification,
				"date_reussite" => $avancement->date_réussite,
				"extra" => $avancement->extra,
			];

			$avancement_sauvegardé = $this->construire(
				[AvancementMdl::updateOrCreate(["user_id" => $user["id"], "question_uri" => $question_uri], $objet)],
				$username,
			);

			if ($includes != []) {
				$avancement_sauvegardé = [$question_uri => $this->get_avancement($username, $question_uri, $includes)];
				if ($avancement_sauvegardé[$question_uri] === null) {
					throw new DAOException("Erreur de sauvegarde.");
				}
			}

			return $avancement_sauvegardé;
		} catch (QueryException $e) {
			throw new DAOException($e);
		}
	}

	/**
	 * @param array<AvancementMdl|null> $data
	 * @param array<string> $includes
	 */
	public static function construire(array|Collection $data, string $username, array $includes = [])
	{
		$avancements = [];
		foreach ($data as $item) {
			$tentatives = [];
			$sous_avancements = [];

			if ($item == null) {
				continue;
			}

			if ($includes) {
				if ($item["type"] == Type::PROG->value) {
					$tentatives = in_array("tentatives", $includes)
						? TentativeProgDAO::construire(
							$item["tentatives_prog"],
							self::filtrer_niveaux($includes, "tentatives"),
						)
						: [];
				} elseif ($item["type"] == Type::SYS->value) {
					$tentatives = in_array("tentatives", $includes)
						? TentativeSysDAO::construire(
							$item["tentatives_sys"],
							self::filtrer_niveaux($includes, "tentatives"),
						)
						: [];
				} elseif ($item["type"] == Type::SEQ->value) {
					if (in_array("avancements", $includes)) {
						$question = DAOFactory::getInstance()
							->get_question_dao()
							->get_question($item["question_uri"], ["questions"]);
						foreach ($question->questions as $uri => $question) {
							$sous_avancement = DAOFactory::getInstance()
								->get_avancement_dao()
								->get_avancement($username, $uri);
							$sous_avancements[$uri] =
								$sous_avancement ?? new Avancement(titre: $question->titre, niveau: $question->niveau);
						}
					}
				}
			}

			if ($item["type"] == Type::SEQ->value) {
				$avancement = new AvancementSeq(
					état: $item["état"],
					avancements: $sous_avancements,
					titre: $item["titre"],
					niveau: $item["niveau"],
					extra: $item["extra"],
					date_modification: $item["date_modification"],
					date_réussite: $item["date_reussite"],
				);
			} else {
				$avancement = new Avancement(
					état: $item["état"],
					tentatives: $tentatives,
					titre: $item["titre"],
					niveau: $item["niveau"],
					extra: $item["extra"],
					sauvegardes: in_array("sauvegardes", $includes)
						? SauvegardeDAO::construire(
							$item["sauvegardes"],
							self::filtrer_niveaux($includes, "sauvegardes"),
						)
						: [],
					date_modification: $item["date_modification"],
					date_réussite: $item["date_reussite"],
				);
			}

			$avancements[$item["question_uri"]] = $avancement;
		}
		return $avancements;
	}
}
