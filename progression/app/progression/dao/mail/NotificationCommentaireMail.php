<?php
/*
  This file is part of Progression.

  Progression is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Progression is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Progression.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace progression\dao\mail;

use progression\domaine\entité\user\User;
use progression\domaine\entité\Commentaire;

class NotificationCommentaireMail extends MailableLocalisé
{
	public string $question_uri;
	public int $date_soumission;
	public string $date_commentaire;
	public string $commentaire;
	public string $créateur;
	public string $appurl;

	public function __construct(
		User $destinataire,
		string $question_uri,
		int $date_soumission,
		Commentaire $commentaire,
	) {
		parent::__construct($destinataire);

		$this->question_uri = $question_uri;
		$this->date_soumission = $date_soumission;
		$this->commentaire = $commentaire->message;
		$this->créateur = $commentaire->créateur->username;

		$this->date_commentaire = date(DATE_ISO8601, $commentaire->date);

		$this->appurl = config("mail.redirection");
	}

	public function build(): self
	{
		//@phpstan-ignore-next-line
		return $this->view("emails.{$this->locale}.notification_commentaire")->text(
			"emails.{$this->locale}.notification_commentaire-text",
		);
	}
}
