<?php
/*
	This file is part of Progression.

	Progression is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Progression is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Progression.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace progression\dao\mail;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use progression\domaine\entité\user\User;
use progression\domaine\entité\Commentaire;
use progression\http\contrôleur\GénérateurDeToken;
use Carbon\Carbon;

class MailUser
{
	public string $name;
	public string|null $email;

	public function __construct(User $user)
	{
		$this->name = $user->username;
		$this->email = $user->courriel;
	}
}

class Expéditeur
{
	function envoyer_courriel_de_validation(User $destinataire): void
	{
		$data = [
			"url_user" => config("app.url") . "/user/" . $destinataire->username,
			"user" => [
				"username" => $destinataire->username,
				"courriel" => $destinataire->courriel,
				"rôle" => $destinataire->rôle,
			],
		];
		$ressources = [
			"user" => [
				"url" => "^user/" . $destinataire->username . "$",
				"method" => "^PATCH$",
			],
		];

		$expirationToken = Carbon::now()->addMinutes((int) config("jwt.expiration"))->timestamp;
		$token = GénérateurDeToken::get_instance()->générer_token(
			$destinataire->username,
			$expirationToken,
			$ressources,
			$data,
		);

		try {
			Mail::to(new MailUser($destinataire))->send(
				(new ValidationCourrielMail($destinataire, $token))->subject(
					trans("courriels.validation.sujet", [], $destinataire->getLocale()),
				),
			);
			Log::notice("(" . __CLASS__ . ") Courriel de validation envoyé à {$destinataire->courriel} :");
		} catch (\Swift_TransportException | \Swift_RfcComplianceException $e) {
			throw new EnvoiDeCourrielException($e);
		}
	}

	function envoyer_courriel_de_notification(
		User $destinataire,
		string $url,
		int $date_soumission,
		Commentaire $commentaire,
	): void {
		try {
			Mail::to(new MailUser($destinataire))->send(
				(new NotificationCommentaireMail($destinataire, $url, $date_soumission, $commentaire))->subject(
					trans(
						"courriels.commentaire.sujet",
						["créateur" => $commentaire->créateur->username],
						$destinataire->getLocale(),
					),
				),
			);
			Log::debug("(" . __CLASS__ . ") Courriel de notification envoyé à {$destinataire->courriel} :");
		} catch (\Swift_TransportException | \Swift_RfcComplianceException $e) {
			throw new EnvoiDeCourrielException($e);
		}
	}
}
