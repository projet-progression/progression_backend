<?php
/*
   This file is part of Progression.

   Progression is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Progression is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Progression.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace progression\providers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Validator as ValidatorImpl;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use progression\domaine\interacteur\{
	ObtenirUserInt,
	ObtenirCléInt,
	LoginInt,
	AccèsInterditException,
	ParamètreInvalideException,
};
use progression\domaine\entité\user\{User, État, Rôle};
use progression\domaine\entité\clé\{Clé, Portée};
use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;
use UnexpectedValueException;
use DomainException;
use Carbon\Carbon;

class AuthServiceProvider extends ServiceProvider
{
	public function register() {}

	public function boot()
	{
		Gate::guessPolicyNamesUsing(function ($modelClass) {
			if ($modelClass == "progression\domaine\entité\user\User") {
				return "acces-utilisateur";
			}
		});

		Gate::before(function ($user, $ability) {
			if ($user && $user->rôle == Rôle::ADMIN) {
				return true;
			}
		});

		// À corriger?
		// Cause : "Cannot access offset 'auth' on Illuminate\Contracts\Foundation\Application."
		// auth est définit dans bootstrap/app.php
		// @phpstan-ignore-next-line https://git.dti.crosemont.quebec/progression/progression_backend/-/issues/231
		$this->app["auth"]->viaRequest("api", function (Request $request): User|null|false {
			// Fournit le User passé aux Gates à fins d'authentification et d'autorisation
			// Retourne un User authentifié ou null.
			// Retourne false si aucun indentifiant n'est reçu.
			$creds = $this->extraireCreds($request);

			if (!array_key_exists("identifiant", $creds)) {
				return false;
			}

			$validateur = $this->valider_paramètres($creds);
			if ($validateur->fails()) {
				throw new ParamètreInvalideException($validateur->errors());
			}

			if (array_key_exists("token", $creds)) {
				$contexte = $this->extraireCookie($request, "contexte_token");
				$fingerprint = $contexte ? hash("sha256", $contexte) : null;
				return $this->authentifier_par_token($creds, $fingerprint);
			} else {
				return $this->authentifier_par_mdp($creds);
			}
		});

		Gate::define("acces-ressource", function ($user, $request) {
			$tokenRessource = $this->extraire_token_ressources($request);

			return ($tokenRessource &&
				$this->validerTokenRessource(
					$tokenRessource,
					$request->username,
					$request->path(),
					$request->method(),
				)) ||
				($user && $this->validerAccèsRessourcesUtilisateur($user, $request));
		});

		Gate::define("utilisateur-authentifié", function ($user) {
			return $user !== false;
		});

		Gate::define("utilisateur-actif", function ($user, $request = null) {
			if ($user === false && $request) {
				$obtenirUserInteracteur = new ObtenirUserInt();
				$user = $obtenirUserInteracteur->get_user($request->username);
			}

			return $user && $this->vérifier_état_user_actif($user);
		});

		Gate::define("utilisateur-non-inactif", function ($user, $request = null) {
			if ($user === false && $request) {
				$obtenirUserInteracteur = new ObtenirUserInt();
				$user = $obtenirUserInteracteur->get_user($request->username);
			}

			return $user && $this->vérifier_état_user_non_inactif($user);
		});

		Gate::define("utilisateur-auth-par-mdp", function ($user, $request) {
			return $user && $this->vérifier_authentification_par_mdp($request);
		});

		Gate::define("utilisateur-auth-par-mdp-ou-clé", function ($user, $request) {
			return $user &&
				($this->vérifier_authentification_par_mdp($request) ||
					$this->vérifier_authentification_par_clé($request));
		});

		Gate::define("utilisateur-validé", function ($user) {
			return $user && $user->état != État::EN_ATTENTE_DE_VALIDATION;
		});

		Gate::define("soumettre-tentative", function ($user, $username) {
			return $user && mb_strtolower($user->username) == mb_strtolower($username);
		});

		Gate::define("modifier-rôle-user-admin", function ($user) {
			return $user && $user->rôle == Rôle::ADMIN;
		});

		Gate::define("modifier-état-user-inactif", function ($user) {
			return $user && $user->rôle == Rôle::ADMIN;
		});

		Gate::define("modifier-mdp", function ($user, $request) {
			return $user && $this->vérifier_authentification_par_mdp($request);
		});

		Gate::define("créer-commentaire", function ($user, User $créateur) {
			return $user == $créateur;
		});

		Gate::define("supprimer-commentaire", function ($user, User $créateur) {
			return $user == $créateur;
		});

		Gate::define("valider-le-courriel", function ($user, $rôle_user_cible) {
			return !($rôle_user_cible == Rôle::ADMIN || config("mail.mailer") == "no");
		});

		Gate::define("activation", function ($user, $request = null) {
			if ($user === false && $request) {
				$obtenirUserInteracteur = new ObtenirUserInt();
				$user = $obtenirUserInteracteur->get_user($request->username);
			}

			return $user && $this->vérifier_activation_du_compte($user, $request);
		});
	}

	/**
	 * @param array<string, string|null> $creds
	 */
	private function authentifier_par_mdp(array $creds): User|null
	{
		// Valide l'authentification par mot de passe ou clé
		$identifiant = $creds["identifiant"];
		if (empty($identifiant)) {
			return null;
		}

		$loginInt = new LoginInt();

		if (array_key_exists("key_name", $creds)) {
			$key_name = $creds["key_name"];
			$key_secret = $creds["key_secret"];
			$user = $loginInt->effectuer_login_par_clé($identifiant, $key_name, $key_secret);
		} else {
			$password = $creds["password"];
			$domaine = $creds["domaine"];
			$user = $loginInt->effectuer_login_par_identifiant($identifiant, $password, $domaine);
		}

		return $user;
	}

	/**
	 * @param array<string, string|null> $creds
	 */
	private function authentifier_par_token(array $creds, string|null $fingerprint): User|null
	{
		// Authentification par token
		$identifiant = $creds["identifiant"];
		if (empty($identifiant)) {
			return null;
		}

		$tokenEncodé = $creds["token"];
		if (empty($tokenEncodé)) {
			return null;
		}

		$tokenDécodé = $this->décoderToken($tokenEncodé);
		if (is_array($tokenDécodé) && array_key_exists("ressources", $tokenDécodé)) {
			//Les ressources sont interdites dans un token d'identification
			throw new AccèsInterditException("Token invalide ou expiré");
		}
		if (
			$tokenDécodé &&
			$this->vérifierExpirationToken($tokenDécodé) &&
			(!isset($tokenDécodé["fingerprint"]) || $tokenDécodé["fingerprint"] === $fingerprint)
		) {
			$obtenirUserInteracteur = new ObtenirUserInt();
			return $obtenirUserInteracteur->get_user($identifiant);
		}
		return null;
	}

	/**
	 * @return array<mixed>|null
	 */
	private function décoderToken(string $tokenEncodé): array|null
	{
		try {
			//JWT::decode fournit une stdClass, le moyen le plus simple de transformer en array
			//est de réencoder/décoder en json.
			// @phpstan-ignore-next-line
			return json_decode(json_encode(JWT::decode($tokenEncodé, config("jwt.secret"), ["HS256"])), true);
		} catch (UnexpectedValueException | SignatureInvalidException | DomainException $e) {
			Log::notice("Token invalide ${tokenEncodé}");
			return null;
		}
	}

	/**
	 * Extrait les identifiants de connexion à partir de l'entête Authorization.
	 *
	 * Si des secrets sont fournis, ils sont d'abord récupérés de l'entête et, à défaut, d'un cookie.
	 *
	 * @return array<string, string|null> Un tableau d'identifiants ou [] si aucune entête d'authentification n'existe
	 *
	 * Si Authorization est de type Bearer, retourne : identifiant et token
	 * Si Authorization est de type Basic, retourne : identifiant, password et domaine
	 * Si Authorization est de type Key, retourne : identifiant, key_name et key_secret
	 */
	private function extraireCreds(Request $request): array
	{
		$authorization = $this->obtenir_identification($request->header("authorization"));

		if (empty($authorization)) {
			return [];
		}

		if (stripos($authorization, "bearer") === 0) {
			$creds = $this->décoderCreds_token($authorization);
		} elseif (stripos($authorization, "basic") === 0) {
			$creds = $this->décoderCreds_basic($authorization);
		} elseif (stripos($authorization, "key") === 0) {
			$creds = $this->décoderCreds_key($authorization);
			$creds["key_secret"] = $creds["key_secret"] ?? $this->extraireCookie($request, "authKey_secret");
		} else {
			throw new ParamètreInvalideException("Type d'authentification invalide.");
		}
		return $creds;
	}

	/**
	 * @param array<string>|string|null $authorization
	 */
	private function obtenir_identification(array|string|null $authorization): string|null
	{
		if (is_array($authorization) && count($authorization) > 0) {
			return $authorization[0];
		}
		if (is_string($authorization)) {
			return trim(explode(",", strval($authorization))[0]);
		}
		return null;
	}

	private function extraire_token_ressources(Request $request): string|null
	{
		$tkres = $request->input("tkres");
		if ($tkres) {
			return $tkres;
		}

		$authorization = $request->header("authorization");

		if (is_array($authorization) && count($authorization) > 1) {
			return trim(str_ireplace("bearer", "", $authorization[1]));
		}
		if (is_string($authorization) && strpos($authorization, ",") !== false) {
			$entête = trim(explode(",", strval($authorization))[1]);
			if (stripos("bearer", $entête) == 0) {
				return trim(str_ireplace("bearer", "", $entête));
			}
		} elseif (is_string($authorization)) {
			return trim(str_ireplace("bearer", "", $authorization));
		}
		return null;
	}

	/**
	 * @return array<string, string>
	 */
	private function décoderCreds_token(string $creds_header): array
	{
		$tokenEncodé = trim(str_ireplace("bearer", "", $creds_header));
		$tokenDécodé = $this->décoderToken($tokenEncodé);

		if (isset($tokenDécodé["ressources"])) {
			return [];
		}

		if ($tokenDécodé && $this->vérifierExpirationToken($tokenDécodé)) {
			return [
				"identifiant" => $tokenDécodé["username"],
				"token" => $tokenEncodé,
			];
		} else {
			throw new AccèsInterditException("Token invalide ou expiré");
		}
	}

	/**
	 * @return array<string, string|null>
	 */
	private function décoderCreds_basic(string $creds_header): array
	{
		$creds_encodés = trim(str_ireplace("basic", "", $creds_header));
		$creds_décodés = base64_decode($creds_encodés);

		$creds_array = preg_split("/:/", $creds_décodés);

		return [
			"identifiant" => $creds_array[0] ?? null,
			"password" => $creds_array[1] ?? null,
			"domaine" => $creds_array[2] ?? null,
		];
	}

	/**
	 * @return array<string, string|null>
	 */
	private function décoderCreds_key(string $creds_header): array
	{
		$creds_encodés = trim(str_ireplace("key", "", $creds_header));
		$creds_décodés = base64_decode($creds_encodés);

		$creds_array = [];

		$creds_array = preg_split("/:/", $creds_décodés);
		return [
			"identifiant" => $creds_array[0] ?? null,
			"key_name" => $creds_array[1] ?? null,
			"key_secret" => $creds_array[2] ?? null,
		];
	}

	private function extraireCookie(Request $request, string $nom): string|null
	{
		$cookie = $request->cookie($nom);
		if (is_array($cookie)) {
			return strval($cookie[0]);
		} elseif (is_string($cookie)) {
			return $cookie;
		}
		return null;
	}

	private function validerTokenRessource(string $token, string $username, string $path, string $method): bool
	{
		$tokenRessourceDécodé = $this->décoderToken($token);

		return is_array($tokenRessourceDécodé) &&
			array_key_exists("ressources", $tokenRessourceDécodé) &&
			is_array($tokenRessourceDécodé["ressources"]) &&
			mb_strtolower($username) == mb_strtolower($tokenRessourceDécodé["username"]) &&
			$this->vérifierExpirationToken($tokenRessourceDécodé) &&
			$this->vérifierRessourceAutorisée($tokenRessourceDécodé["ressources"], $path, $method);
	}

	private function validerAccèsRessourcesUtilisateur(User $user, Request $request): bool
	{
		$creds = $this->extraireCreds($request);

		if ($user->état != État::ACTIF) {
			return false;
		}

		// Valide l'accès aux ressources par clé d'authentification
		if (!empty($creds["key_name"])) {
			$clé = (new ObtenirCléInt())->get_clé($creds["identifiant"], $creds["key_name"]);
			return $clé && $this->vérifier_permissions_clé($clé, $user, $request);
		}
		// Valide l'accès aux ressources par token d'authentification
		elseif (!empty($creds["token"])) {
			return $this->vérifier_permissions_token($creds["token"], $user, $request);
		} else {
			// Valide l'accès aux ressources par mot de passe
			return $this->vérifier_permissions_mdp($user, $request);
		}
	}

	/**
	 * @param array<mixed> $token
	 */
	private function vérifierExpirationToken(array $token): bool
	{
		return Carbon::now()->getTimestamp() < $token["expired"] || $token["expired"] === 0;
	}

	/**
	 * @param array<mixed> $ressources
	 */
	private function vérifierRessourceAutorisée(array $ressources, string $path, string $method): bool
	{
		foreach ($ressources as $ressource) {
			if (
				is_array($ressource) &&
				array_key_exists("url", $ressource) &&
				strlen($ressource["url"]) > 0 &&
				array_key_exists("method", $ressource) &&
				strlen($ressource["method"]) > 0 &&
				preg_match("#" . $ressource["url"] . "#", $path) &&
				preg_match("#" . $ressource["method"] . "#i", $method)
			) {
				return true;
			}
		}

		return false;
	}

	private function vérifier_état_user_non_inactif(User $user): bool
	{
		return $user->état !== État::INACTIF;
	}

	private function vérifier_état_user_actif(User $user): bool
	{
		return $user->état === État::ACTIF;
	}

	private function vérifier_permissions_token(string $token, User $user, Request $request): bool
	{
		if (empty($token)) {
			return false;
		}

		$tokenDécodé = $this->décoderToken($token);

		return $tokenDécodé && mb_strtolower($tokenDécodé["username"]) == mb_strtolower($request->username);
	}

	private function vérifier_permissions_clé(Clé $clé, User $user, Request $request): bool
	{
		return $clé->portée == Portée::AUTH &&
			$request->method() == "POST" &&
			$request->path() == "user/{$user->username}/tokens";
	}

	private function vérifier_authentification_par_mdp(Request $request): bool
	{
		$creds = $this->extraireCreds($request);
		return array_key_exists("password", $creds) &&
			!array_key_exists("key_name", $creds) &&
			!array_key_exists("token", $creds);
	}

	private function vérifier_authentification_par_clé(Request $request): bool
	{
		$creds = $this->extraireCreds($request);
		return !array_key_exists("password", $creds) &&
			array_key_exists("key_name", $creds) &&
			!array_key_exists("token", $creds);
	}

	private function vérifier_permissions_mdp(User $user, Request $request): bool
	{
		return mb_strtolower($user->username) == mb_strtolower($request->username);
	}

	private function vérifier_activation_du_compte(User $user, Request $request): bool
	{
		return $user->état == État::EN_ATTENTE_DE_VALIDATION &&
			($request->input("data")["attributes"]["état"] ?? false) == État::ACTIF->value;
	}

	/**
	 * @param array<string, string|null> $creds
	 */
	private function valider_paramètres(array $creds): ValidatorImpl
	{
		$validateur = Validator::make(
			$creds,
			[
				"identifiant" => "required|string|between:2,64",
				"key_secret" => "required_with:key_name",
				"key_name" => "alpha_dash:ascii",
			],
			[
				"required" => "Le champ :attribute est obligatoire.",
				"identifiant.regex" => "L'identifiant doit être un nom d'utilisateur ou un courriel valide.",
				"password.required_without_all" =>
					"Le champ password est obligatoire lorsque key_name ou token ne sont pas présents.",
				"key_secret.required_with" => "Le champ key_secret est obligatoire lorsque key_name est présent.",
				"key_secret.required" => "Le champ key_secret est obligatoire lorsque key_name est présent",
				"key_name.alpha_dash" => "Le champ key_name doit être alphanumérique 'a-Z0-9-_'",
			],
		)
			->sometimes("password", "required_without_all:key_name,token", function ($input) {
				$auth_local = config("authentification.local") !== false;
				$auth_ldap = config("authentification.ldap") === true;

				return $auth_local || $auth_ldap;
			})
			->sometimes("identifiant", "regex:/^\w{2,64}$/u", function ($input) {
				$auth_local = config("authentification.local") !== false;
				$auth_ldap = config("authentification.ldap") === true;

				return isset($input->key_name) || (!$auth_local && !$auth_ldap);
			})
			->sometimes("identifiant", ["regex:/^\w{2,64}$|^[^\s@]+@[^\s@]+\.[a-zA-Z]{2,}$/u"], function ($input) {
				$auth_local = config("authentification.local") !== false;
				$auth_ldap = config("authentification.ldap") === true;

				return !isset($input->key_name) && ($auth_local || $auth_ldap);
			});

		return $validateur;
	}
}
