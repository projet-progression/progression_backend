<?php
/*
	This file is part of Progression.

	Progression is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Progression is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Progression.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace progression\domaine\entité\user;

use InvalidArgumentException;
use progression\domaine\entité\{Avancement, Banque};
use progression\domaine\entité\clé\Clé;

class User
{
	public string $username;
	public string|null $courriel;
	public État $état;
	public Rôle $rôle;
	public $avancements;
	public $clés;
	/**
	 * @var array<Banque> $banques;
	 */
	public array $banques;
	public string $préférences;
	public int $date_inscription;
	public string $nom;
	public string $prénom;
	public string $nom_complet;
	public string $pseudonyme;
	public string $biographie;
	public Occupation $occupation;
	public string $avatar;
	public string $connaissances;

	/**
	 * @param array<Avancement> $avancements
	 * @param array<Clé> $clés
	 * @param array<Banque> $banques
	 */
	public function __construct(
		string $username,
		int $date_inscription,
		string|null $courriel = null,
		État $état = État::INACTIF,
		Rôle $rôle = Rôle::NORMAL,
		array $avancements = [],
		array $clés = [],
		array $banques = [],
		string $préférences = "",
		string $nom = "",
		string $prénom = "",
		string $nom_complet = "",
		string $pseudonyme = null,
		string $biographie = "",
		Occupation $occupation = Occupation::AUTRE,
		string $avatar = "",
		string $connaissances = "",
	) {
		$this->username = $username;
		$this->courriel = $courriel;
		$this->état = $état;
		$this->rôle = $rôle;
		$this->avancements = $avancements;
		$this->clés = $clés;
		$this->banques = $banques;
		$this->préférences = $préférences;
		$this->date_inscription = $date_inscription;
		$this->nom = $nom;
		$this->prénom = $prénom;
		$this->nom_complet = $nom_complet;
		$this->pseudonyme = $pseudonyme ?? $username;
		$this->biographie = $biographie;
		$this->occupation = $occupation;
		$this->avatar = $avatar;
		$this->connaissances = $connaissances;
	}

	public function getLocale(): string
	{
		$préférences = json_decode($this->préférences);
		return $préférences?->locale ?? "fr";
	}
}
