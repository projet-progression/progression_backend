<?php

namespace progression\domaine\entité\user;

enum Occupation: string
{
	case AUTRE = "autre";
	case ÉTUDIANT = "étudiant";
	case ENSEIGNANT = "enseignant";
	case TUTEUR = "tuteur";
}

?>
