<?php
/*
  This file is part of Progression.

  Progression is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Progression is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Progression.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace progression\domaine\entité;

use progression\domaine\entité\question\État;

class AvancementSeq extends Avancement
{
	/** @var array<Avancement> */
	public array $avancements;

	/**
	 * @param array<Avancement> $avancements;
	 */
	public function __construct(
		État $état = État::DEBUT,
		string|null $titre = null,
		string|null $niveau = null,
		string|null $extra = null,
		array $avancements = [],
		int $date_modification = null,
		int $date_réussite = null,
	) {
		parent::__construct(
			état: $état,
			titre: $titre,
			niveau: $niveau,
			extra: $extra,
			date_modification: $date_modification,
			date_réussite: $date_réussite,
		);

		$this->mettre_à_jour_dates_et_état($avancements);
	}

	public function __set(string $property, mixed $value): void
	{
		switch ($property) {
			case "avancements":
				$this->mettre_à_jour_dates_et_état($value);
				break;
		}
	}

	public function ajouter_avancement(string $uri, Avancement $avancement): void
	{
		if ($avancement->date_modification > $this->date_modification) {
			$this->date_modification = $avancement->date_modification;
		}

		if ($avancement->état == État::DEBUT) {
			$this->état = État::DEBUT;
		} elseif ($avancement->état == État::NONREUSSI && $this->état == État::REUSSI) {
			$this->état = État::NONREUSSI;
		} elseif (
			$avancement->état == État::REUSSI &&
			($this->état == État::NONREUSSI || count($this->avancements) == 0)
		) {
			$this->état = État::REUSSI;
			if (!$this->date_réussite || $avancement->date_réussite < $this->date_réussite) {
				$this->date_réussite = $avancement->date_réussite;
			}
		}

		if ($this->état != État::REUSSI) {
			$this->date_réussite = null;
		}

		$this->avancements[$uri] = $avancement;
	}

	/**
	 * @param array<Avancement> $avancements;
	 */
	private function mettre_à_jour_dates_et_état(array $avancements): void
	{
		if (!$avancements) {
			$this->avancements = [];
			return;
		}

		$this->état = État::DEBUT;
		$this->date_modification = null;
		$this->date_réussite = null;
		$this->avancements = [];

		foreach ($avancements as $uri => $avancement) {
			$this->ajouter_avancement($uri, $avancement);
		}
	}
}
