<?php
/*
  This file is part of Progression.

  Progression is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Progression is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Progression.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace progression\domaine\entité;

use progression\domaine\entité\question\État;

class Avancement
{
	public État $état;
	/** @var array<Tentative> */
	public array $tentatives;
	public string|null $titre;
	public string|null $niveau;
	public int|null $date_modification;
	public int|null $date_réussite;
	/** @var array<Sauvegarde> */
	public array $sauvegardes;
	public string|null $extra;

	/**
	 * @param array<Tentative> $tentatives
	 * @param array<Sauvegarde> $sauvegardes
	 **/
	public function __construct(
		État $état = État::DEBUT,
		array $tentatives = [],
		string|null $titre = null,
		string|null $niveau = null,
		array $sauvegardes = [],
		string|null $extra = null,
		int $date_modification = null,
		int $date_réussite = null,
	) {
		$this->état = $état;
		$this->titre = $titre;
		$this->niveau = $niveau;
		$this->sauvegardes = $sauvegardes;
		$this->extra = $extra;
		$this->date_modification = $date_modification;
		$this->date_réussite = $date_réussite;

		$this->mettre_à_jour_dates_et_état($tentatives);
	}

	public function __set(string $property, mixed $value): void
	{
		switch ($property) {
			case "tentatives":
				$this->mettre_à_jour_dates_et_état($value);
				break;
		}
	}

	public function ajouter_tentative($tentative)
	{
		if ($this->état == État::DEBUT) {
			$this->état = État::NONREUSSI;
		}
		if ($tentative->date_soumission > $this->date_modification) {
			$this->date_modification = $tentative->date_soumission;
		}
		if ($tentative->réussi) {
			$this->état = État::REUSSI;
			if (!$this->date_réussite || $tentative->date_soumission < $this->date_réussite) {
				$this->date_réussite = $tentative->date_soumission;
			}
		}
		$this->tentatives[$tentative->date_soumission] = $tentative;
	}

	/**
	 * @param array<Tentative> $tentatives;
	 */
	private function mettre_à_jour_dates_et_état(array $tentatives)
	{
		$this->tentatives = [];

		if (!$tentatives) {
			return;
		}

		$this->état = État::NONREUSSI;
		$this->date_modification = null;
		$this->date_réussite = null;

		foreach ($tentatives as $i => $tentative) {
			$this->ajouter_tentative($tentative);
		}
	}
}
