<?php

namespace progression\domaine\entité\question;

class QuestionSeq extends Question
{
	/** @var array<Question> */
	public array $questions;

	/**
	 * @param string|array<mixed>|null $enonce
	 * @param array<Question> $questions
	 */
	public function __construct(
		string $niveau = null,
		string $titre = null,
		string $objectif = null,
		string|array $enonce = null,
		string $auteur = null,
		string $licence = null,
		string $feedback_pos = null,
		string $feedback_neg = null,
		string $feedback_err = null,
		string $description = null,
		array $questions = [],
	) {
		parent::__construct(
			$niveau,
			$titre,
			$objectif,
			$enonce,
			$auteur,
			$licence,
			$feedback_pos,
			$feedback_neg,
			$feedback_err,
			tests: [],
			description: $description,
		);
		$this->questions = $questions;
	}
}
