<?php
/*
	This file is part of Progression.

	Progression is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Progression is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Progression.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace progression\domaine\entité;

use progression\domaine\entité\question\Question;

use InvalidArgumentException;

class Banque
{
	public string|null $nom;

	/**
	 * @var array<string, Question> $questions
	 */
	public array $questions;
	/**
	 * @var array<int|string, Banque> $banques
	 */
	public array $banques;

	/**
	 * @param array<Question> $questions
	 * @param array<int|string, Banque> $banques
	 */
	public function __construct(string|null $nom = null, array $questions = [], array $banques = [])
	{
		$this->nom = $nom;
		$this->questions = $questions;
		$this->banques = $banques;
	}

	public function ajouterQuestion(string $question_uri, Question $question): void
	{
		$this->questions[$question_uri] = $question;
	}

	public function ajouterBanque(int|string $banque_uri, Banque $banque): void
	{
		$this->banques[$banque_uri] = $banque;
	}
}
