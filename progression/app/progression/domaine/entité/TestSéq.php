<?php

namespace progression\domaine\entité;

class TestSéq extends Test
{
	public string $entrée;

	/** @var string[] */
	public array $params = [];

	/**
	 * @param array<int, string> $params
	 */
	public function __construct(
		string $nom = "",
		string $sortie_attendue = "",
		string $entrée = "",
		array $params = [],
		string $feedback_pos = null,
		string $feedback_neg = null,
		string $feedback_err = null,
		bool $caché = false,
	) {
		parent::__construct(
			nom: $nom,
			sortie_attendue: $sortie_attendue,
			feedback_pos: $feedback_pos,
			feedback_neg: $feedback_neg,
			feedback_err: $feedback_err,
			caché: $caché,
		);
		$this->entrée = $entrée;
		$this->params = $params;
	}
}
