<?php
/*
   This file is part of Progression.

   Progression is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Progression is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Progression.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace progression\domaine\interacteur;

use progression\dao\DAOException;
use progression\dao\chargeur\ChargeurException;
use progression\domaine\entité\Banque;
use progression\domaine\entité\user\User;

use \LengthException;
use \DomainException;

class SauvegarderBanqueInt extends Interacteur
{
	/**
	 * @return array<int|string, Banque>
	 */
	public function sauvegarder(string $username, string $url, Banque $banque): array
	{
		$dao = $this->source_dao->get_banque_dao();

		try {
			return $dao->save($username, $url, $banque);
		} catch (DAOException $e) {
			throw new IntéracteurException($e, 502);
		} catch (LengthException | DomainException | ChargeurException $e) {
			throw new RessourceInvalideException($e);
		}
	}
}
