<?php
/*
  This file is part of Progression.

  Progression is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Progression is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Progression.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace progression\domaine\interacteur;

use progression\domaine\entité\user\{User, État, Rôle, Occupation};
use progression\dao\mail\EnvoiDeCourrielException;
use progression\domaine\interacteur\SauvegarderUtilisateurInt;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Gate;

class ModifierUserInt extends Interacteur
{
	/**
	 * @param array<mixed> $attributs
	 */
	public function modifier_user(User $user, array $attributs): User
	{
		$user_original = clone $user;

		if (array_key_exists("password", $attributs)) {
			$this->modifier_mot_de_passe($user, $attributs["password"]);
		}

		$user = $this->modifier_entité($user, $attributs);
		if ($user != $user_original) {
			$userInt = new SauvegarderUtilisateurInt();
			$user = $userInt->sauvegarder_user($user->username, $user)[$user->username];
		}

		return $user;
	}

	public function modifier_mot_de_passe(User $user, string $password): User
	{
		$dao = $this->source_dao->get_user_dao();
		$dao->set_password($user, $password);

		return $user;
	}

	/**
	 * @param array<mixed> $attributs
	 */
	private function modifier_entité(User $user, array $attributs): User
	{
		if (array_key_exists("état", $attributs)) {
			$user = $this->modifier_état($user, État::from($attributs["état"]));
		}
		if (array_key_exists("rôle", $attributs)) {
			$user = $this->modifier_rôle($user, Rôle::from($attributs["rôle"]));
		}
		if (array_key_exists("courriel", $attributs)) {
			$user = $this->modifier_courriel($user, $attributs["courriel"]);
		}
		if (array_key_exists("préférences", $attributs)) {
			$user->préférences = $attributs["préférences"];
		}
		if (array_key_exists("nom", $attributs)) {
			$user->nom = $attributs["nom"];
		}
		if (array_key_exists("prénom", $attributs)) {
			$user->prénom = $attributs["prénom"];
		}
		if (array_key_exists("nom_complet", $attributs)) {
			$user->nom_complet = $attributs["nom_complet"];
		}
		if (array_key_exists("biographie", $attributs)) {
			$user->biographie = $attributs["biographie"];
		}
		if (array_key_exists("pseudonyme", $attributs)) {
			if ($this->vérifier_unicité_pseudonyme($attributs["pseudonyme"], $user->username)) {
				$user->pseudonyme = $attributs["pseudonyme"];
			} else {
				throw new DuplicatException("Le pseudonyme est déjà utilisé.");
			}
		}
		if (array_key_exists("avatar", $attributs)) {
			$user->avatar = $attributs["avatar"];
		}
		if (array_key_exists("occupation", $attributs)) {
			$user->occupation = Occupation::from($attributs["occupation"]);
		}
		if (array_key_exists("connaissances", $attributs)) {
			$user->connaissances = $attributs["connaissances"];
		}

		return $user;
	}

	private function vérifier_unicité_pseudonyme(string $pseudonyme, string $username): bool
	{
		$username_trouvé = $this->source_dao->get_user_dao()->trouver(username: $pseudonyme);
		if ($username_trouvé && $username_trouvé->username != $username) {
			return false;
		}
		$pseudonyme_trouvé = $this->source_dao->get_user_dao()->trouver(pseudonyme: $pseudonyme);
		if ($pseudonyme_trouvé && $pseudonyme_trouvé->username != $username) {
			return false;
		}

		return true;
	}

	private function modifier_état(User $user, État $état): User
	{
		if (($user->état == État::INACTIF || $état == État::INACTIF) && !Gate::allows("modifier-état-user-inactif")) {
			throw new PermissionException("Transition d'état interdite");
		}
		$user->état = $état;

		return $user;
	}

	private function modifier_rôle(User $user, Rôle $rôle): User
	{
		if ($rôle == Rôle::ADMIN && !Gate::allows("modifier-rôle-user-admin")) {
			throw new PermissionException("Transition de rôle interdite");
		}

		$user->rôle = $rôle;

		return $user;
	}

	private function modifier_courriel(User $user, string $courriel): User
	{
		if ($user->courriel == $courriel) {
			return $user;
		}

		$dao = $this->source_dao->get_user_dao();
		$user_courriel = $dao->trouver(courriel: $courriel);
		if ($user_courriel?->username && $user_courriel->username !== $user->username) {
			throw new DuplicatException("Le courriel est déjà utilisé.");
		}

		$user->courriel = $courriel;
		if (Gate::allows("valider-le-courriel", $user->rôle)) {
			$user->état = État::EN_ATTENTE_DE_VALIDATION;
			$this->envoyer_courriel_de_validation($user);
		}

		return $user;
	}

	private function envoyer_courriel_de_validation(User $user): void
	{
		try {
			$this->source_dao->get_expéditeur()->envoyer_courriel_de_validation($user);
		} catch (EnvoiDeCourrielException $e) {
			Log::notice("Échec de l'envoi du courriel à " . $user->courriel);
			Log::notice($e->getMessage());
			Log::debug($e);
		}
	}
}
