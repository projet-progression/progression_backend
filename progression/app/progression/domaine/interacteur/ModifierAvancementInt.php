<?php
/*
  This file is part of Progression.

  Progression is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Progression is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Progression.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace progression\domaine\interacteur;

use progression\domaine\entité\Avancement;

class ModifierAvancementInt extends Interacteur
{
	/**
	 * @param array<mixed> $attributs
	 * @param array<string> $includes
	 */
	public function modifier_avancement(
		string $username,
		string $question_uri,
		Avancement $avancement,
		array $attributs,
		array $includes = [],
	): Avancement {
		$avancement_original = clone $avancement;

		$avancement = $this->modifier_entité($avancement, $attributs);

		if ($avancement != $avancement_original) {
			$avancementInt = new SauvegarderAvancementInt();
			$avancement = $avancementInt->sauvegarder($username, $question_uri, $avancement, includes: $includes)[
				$question_uri
			];
		}

		return $avancement;
	}

	/**
	 * @param array<mixed> $attributs
	 */
	private function modifier_entité(Avancement $avancement, array $attributs): Avancement
	{
		if (array_key_exists("extra", $attributs)) {
			$avancement->extra = $attributs["extra"];
		}

		return $avancement;
	}
}
?>
