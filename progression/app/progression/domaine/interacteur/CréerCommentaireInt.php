<?php
/*
   This file is part of Progression.

   Progression is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Progression is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Progression.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace progression\domaine\interacteur;

use progression\domaine\entité\Commentaire;
use progression\domaine\entité\user\User;
use progression\dao\mail\EnvoiDeCourrielException;
use progression\dao\DAOException;
use progression\util\Encodage;
use Illuminate\Support\Facades\Log;

class CréerCommentaireInt extends Interacteur
{
	/**
	 * @return non-empty-array<Int,Commentaire>
	 */
	public function créer_commentaire(
		string $username,
		string $question_uri,
		int $date_soumission,
		Commentaire $commentaire,
	): array {
		$commentaire_sauvegardé = (new SauvegarderCommentaireInt())->sauvegarder_commentaire(
			$username,
			$question_uri,
			$date_soumission,
			null,
			$commentaire,
		);

		if ($commentaire->créateur->username != $username) {
			$this->envoyer_courriel_de_notification(
				$username,
				$question_uri,
				$date_soumission,
				$this->premier_élément($commentaire_sauvegardé),
			);
		}

		return $commentaire_sauvegardé;
	}

	private function envoyer_courriel_de_notification(
		string $username,
		string $question_uri,
		int $date_soumission,
		Commentaire $commentaire,
	): void {
		$destinataire = (new ObtenirUserInt())->get_user($username);

		if ($destinataire?->courriel) {
			try {
				$this->source_dao
					->get_expéditeur()
					->envoyer_courriel_de_notification(
						$destinataire,
						Encodage::base64_encode_url($question_uri),
						$date_soumission,
						$commentaire,
					);
			} catch (EnvoiDeCourrielException $e) {
				Log::notice("Échec de l'envoi du courriel à " . $destinataire->courriel);
				Log::notice($e->getMessage());
				Log::debug($e);
			}
		}
	}
}
