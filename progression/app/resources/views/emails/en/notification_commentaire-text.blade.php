Hi {{$destinataire->username}},


On the {{$date_commentaire}}, «{{$créateur}}» wrote this comment for you:

«{{$commentaire}}»


Follow this link to view the comment in Progression: {{ $appurl }}/question?uri={{ $question_uri }}&tentative={{ $date_soumission }}

Have a good Progression!
