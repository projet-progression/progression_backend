<h1>
	Hi {{$destinataire->username}},
</h1>

<p>On the {{$date_commentaire}}</p>
<p>«{{$créateur}}» wrote this comment for you:</p>

<p>
	<pre>{{$commentaire}}</pre>
</p>

<p>
	Click here to view the <a href="{{ $appurl }}/question?uri={{ $question_uri }}&tentative={{ $date_soumission }}">comment in Progression</a>.
</p>

<h3>
	Have a good Progression!
</h3>
