<h1>
	Bonjour {{$destinataire->username}},
</h1>

<p>Le {{$date_commentaire}}</p>
<p>«{{$créateur}}» vous a écrit ce commentaire:</p>

<p>
	<pre>{{$commentaire}}</pre>
</p>

<p>
	Cliquez ici pour voir le <a href="{{ $appurl }}/question?uri={{ $question_uri }}&tentative={{ $date_soumission }}">commentaire dans Progression</a>.
</p>

<h3>
	Bonne Progression!
</h3>
