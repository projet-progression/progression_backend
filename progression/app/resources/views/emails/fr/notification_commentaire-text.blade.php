Bonjour {{$destinataire->username}},


Le {{$date_commentaire}}, «{{$créateur}}» vous a écrit ce commentaire:

«{{$commentaire}}»


Suivez ce lien pour voir le commentaire dans Progression: {{ $appurl }}/question?uri={{ $question_uri }}&tentative={{ $date_soumission }}

Bonne Progression!
