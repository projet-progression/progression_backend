<?php
/*
   This file is part of Progression.

   Progression is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Progression is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Progression.  If not, see <https://www.gnu.org/licenses/>.
 */

use progression\ContrôleurTestCase;
use progression\dao\chargeur\ChargeurFactory;
use progression\domaine\entité\question\QuestionSeq;
use progression\domaine\entité\user\{Rôle, État};
use progression\UserAuthentifiable;

final class QuestionSeqCtlTests extends ContrôleurTestCase
{
	public function setUp(): void
	{
		parent::setUp();
		$this->user = new UserAuthentifiable(
			username: "bob",
			date_inscription: 0,
			rôle: Rôle::NORMAL,
			état: État::ACTIF,
		);

		$mockChargeurFichier = Mockery::mock("progression\\dao\\question\\ChargeurQuestionFichier")->makePartial();
		$mockChargeurFichier
			->shouldReceive("récupérer_fichier")
			->with("file:///tmp/test/questionSeq/info.yml")
			->andReturn([
				"type" => "seq",
				"titre" => "Test Kevin",
				"énoncé" =>
					"Appliquez les commandes nécessaires au changement des permissions pour le fichier bonjour.txt. Le fichier doit être public pour tous.",

				"séquence" => ["file:///tmp/test/questionSeq/info1.yml", "file:///tmp/test/questionSeq/info2.yml"],
			]);
		$mockChargeurFichier
			->shouldReceive("récupérer_fichier")
			->with("file:///tmp/test/questionSeq/info1.yml")
			->andReturn([
				"type" => "prog",
				"titre" => "question 1",
				"ébauches" => [],
				"tests" => [],
			]);
		$mockChargeurFichier
			->shouldReceive("récupérer_fichier")
			->with("file:///tmp/test/questionSeq/info2.yml")
			->andReturn([
				"type" => "prog",
				"titre" => "question 2",
				"ébauches" => [],
				"tests" => [],
			]);

		$mockFactory = Mockery::mock("progression\\dao\\chargeur\\ChargeurFactory");
		$mockFactory->shouldReceive("get_chargeur_question_fichier")->andReturn($mockChargeurFichier);
		ChargeurFactory::set_instance($mockFactory);
	}

	public function test_étant_donnée_une_question_séquence_lorsquon_la_récupère_sans_inclusion_on_obtient_la_question_seule()
	{
		$résultat_obtenu = $this->actingAs($this->user)->call(
			"GET",
			"/question/ZmlsZTovLy90bXAvdGVzdC9xdWVzdGlvblNlcS9pbmZvLnltbA",
		);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/questionCtlTests_question_seq_simple.json",
			$résultat_obtenu->getContent(),
		);
	}

	public function test_étant_donnée_une_question_séquence_lorsquon_la_récupère_en_incluant_les_sous_questions_on_obtient_la_question_et_ses_sous_questions()
	{
		$résultat_obtenu = $this->actingAs($this->user)->call(
			"GET",
			"/question/ZmlsZTovLy90bXAvdGVzdC9xdWVzdGlvblNlcS9pbmZvLnltbA?include=questions",
		);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/questionCtlTests_question_seq_avec_sous_questions.json",
			$résultat_obtenu->getContent(),
		);
	}
}
