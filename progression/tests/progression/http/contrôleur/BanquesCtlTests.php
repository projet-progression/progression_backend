<?php
/*
   This file is part of Progression.

   Progression is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Progression is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Progression.  If not, see <https://www.gnu.org/licenses/>.
 */

use progression\ContrôleurTestCase;
use progression\dao\DAOFactory;
use progression\dao\chargeur\ChargeurException;
use progression\domaine\entité\Banque;
use progression\domaine\entité\question\{Question, QuestionProg, QuestionSys};
use progression\domaine\entité\{Exécutable, TestProg, TestSys};
use progression\domaine\entité\user\{User, Rôle, État};
use progression\UserAuthentifiable;

final class BanquesCtlTests extends ContrôleurTestCase
{
	public $user;

	public function setUp(): void
	{
		parent::setUp();

		$this->user = new UserAuthentifiable(
			username: "bob",
			date_inscription: 0,
			rôle: Rôle::NORMAL,
			état: État::ACTIF,
		);

		// Question
		$questionProg = new QuestionProg(
			titre: "Appeler une fonction paramétrée",
			objectif: "Appel d'une fonction existante recevant un paramètre",
			description: "Ceci est une question prog complète",
			enonce: "La fonction `salutations` affiche une salution autant de fois que la valeur reçue en paramètre. Utilisez-la pour faire afficher «Bonjour le monde!» autant de fois que le nombre reçu en entrée.",
			auteur: "Albert Einstein",
			licence: "poétique",
			niveau: "débutant",
			// Ébauches
			exécutables: [
				"python" => new Exécutable("print(\"Hello world\")", "python"),
				"java" => new Exécutable("System.out.println(\"Hello world\")", "java"),
			],

			// Tests
			tests: [
				new TestProg(
					nom: "2 salutations",
					sortie_attendue: "Bonjour\nBonjour\n",
					entrée: "2",
					params: "param_0 param_1",
					caché: false,
				),
				new TestProg(
					nom: "Aucune salutation",
					sortie_attendue: "Cette sortie devrait être cachée",
					entrée: "Cette entrée être caché",
					params: "Ce param devrait être caché",
					caché: true,
				),
			],
		);

		//QuestionSys avec solution sans pregmatch
		$questionSys = new QuestionSys(
			solution: "laSolution",
			feedback_pos: "Bon travail!",
			feedback_neg: "Encore un effort!",
			titre: "Octroyer toutes le permissions",
			objectif: "Octroiement de toutes les permissions.",
			description: "Ceci est une question système complète",
			enonce: "Il faut que l'étudiant trouve les commandes justes pour octroyer toutes les permissions à l'utilisateur krusty.",
			auteur: "Albert Einstein",
			licence: "poétique",
			niveau: "débutant",
			image: "l'image",
			utilisateur: "utilisateur",
			tests: [
				new TestSys("test 1", "vrai", "[ -z vrai ]", "bob"),
				new TestSys("test 2", "faux", "[ -z faux ]", "roger"),
			],
		);

		$mockQuestionDAO = Mockery::mock("progression\\dao\\question\\QuestionDAO");
		$mockQuestionDAO
			->shouldReceive("get_question")
			->with("https://depot.com/roger/questions_prog/fonctions01/appeler_une_fonction")
			->andReturn($questionProg);
		$mockQuestionDAO
			->shouldReceive("get_question")
			->with("https://depot.com/roger/questions_sys/permissions01/octroyer_toutes_les_permissions2")
			->andReturn($questionSys);
		$mockQuestionDAO
			->shouldReceive("get_question")
			->with("https://depot.com/roger/questions_invalide")
			->andThrow(new ChargeurException("Question invalide."));
		$mockQuestionDAO->shouldReceive("get_question")->with(Mockery::any())->andReturn(null);

		// Banque
		$mockBanqueDAO = Mockery::mock("progression\\dao\\banque\\BanqueDAO");
		$mockBanqueDAO
			->allows("get_toutes")
			->with("bob", [])
			->andReturn([
				"https://exemple.com/banque_1/contenu.yml" => new Banque(
					nom: "Test banque de questions 1 - fichier yaml valide",
				),
				"https://exemple.com/banque_2/contenu.yml" => new Banque(
					nom: "Test banque de questions 2 - fichier yaml valide",
				),
			]);
		$mockBanqueDAO
			->allows("get_toutes")
			->with("bob", ["banques"])
			->andReturn([
				"https://exemple.com/banque_1/contenu.yml" => new Banque(
					nom: "Première banque de test",
					banques: [
						"sous-répertoire 1" => new Banque(
							nom: "Banque Sous-répertoire 1",
							questions: [
								"https://exemple.com/banque_complexe/banque_2/question_3.yml" => new Question(
									titre: "Test banque de questions 3 - fichier yaml valide",
								),
							],
							banques: [
								"sous-sous-répertoire" => new Banque(nom: "sous-sous-répertoire"),
								0 => new Banque(nom: ""),
							],
						),
						"sous-répertoire 2" => new Banque(nom: "Banque Sous-répertoire 2"),
					],
					questions: [
						"https://depot.com/roger/questions_prog/fonctions01/appeler_une_fonction" => new Question(
							titre: "Appeler une fonction paramétrée",
							objectif: "Appel d'une fonction existante recevant un paramètre",
							description: "Ceci est une question prog complète",
							enonce: "La fonction `salutations` affiche une salution autant de fois que la valeur reçue en paramètre. Utilisez-la pour faire afficher «Bonjour le monde!» autant de fois que le nombre reçu en entrée.",
							auteur: "Albert Einstein",
							licence: "poétique",
							niveau: "débutant",
						),

						"https://depot.com/roger/questions_sys/permissions01/octroyer_toutes_les_permissions2" => new Question(
							feedback_pos: "Bon travail!",
							feedback_neg: "Encore un effort!",
							titre: "Octroyer toutes le permissions",
							objectif: "Octroiement de toutes les permissions.",
							description: "Ceci est une question système complète",
							enonce: "Il faut que l'étudiant trouve les commandes justes pour octroyer toutes les permissions à l'utilisateur krusty.",
						),
					],
				),
				"https://exemple.com/banque_2/contenu.yml" => new Banque(
					nom: "Deuxième banque de test",
					questions: [
						"https://depot.com/roger/questions_prog/fonctions01/appeler_une_fonction" => $questionProg,
						"https://depot.com/roger/questions_sys/permissions01/octroyer_toutes_les_permissions2" => $questionSys,
					],
				),
			]);
		$mockBanqueDAO
			->allows("get_toutes")
			->with("bob", ["questions", "banques"])
			->andReturn([
				"https://exemple.com/banque_1/contenu.yml" => new Banque(
					nom: "Première banque de test",
					banques: [
						"sous-répertoire 1" => new Banque(
							nom: "Banque Sous-répertoire 1",
							questions: [
								"https://exemple.com/banque_complexe/banque_2/question_3.yml" => new Question(
									titre: "Test banque de questions 3 - fichier yaml valide",
								),
							],
							banques: [
								"sous-sous-répertoire" => new Banque(nom: "sous-sous-répertoire"),
								0 => new Banque(nom: ""),
							],
						),
						"sous-répertoire 2" => new Banque(nom: "Banque Sous-répertoire 2"),
					],
					questions: [
						"https://depot.com/roger/questions_prog/fonctions01/appeler_une_fonction" => $questionProg,
						"https://depot.com/roger/questions_sys/permissions01/octroyer_toutes_les_permissions2" => $questionSys,
					],
				),
				"https://exemple.com/banque_2/contenu.yml" => new Banque(
					nom: "Deuxième banque de test",
					questions: [
						"https://depot.com/roger/questions_prog/fonctions01/appeler_une_fonction" => $questionProg,
						"https://depot.com/roger/questions_sys/permissions01/octroyer_toutes_les_permissions2" => $questionSys,
					],
				),
			]);
		$mockBanqueDAO
			->allows("get_toutes")
			->with("bob", ["questions"])
			->andReturn([
				"https://exemple.com/banque_1/contenu.yml" => new Banque(
					nom: "Première banque de test",
					banques: [
						"sous-répertoire 1" => new Banque(nom: "Banque Sous-répertoire 1"),
						"sous-répertoire 2" => new Banque(nom: "Banque Sous-répertoire 2"),
					],
					questions: [
						"https://depot.com/roger/questions_prog/fonctions01/appeler_une_fonction" => $questionProg,
						"https://depot.com/roger/questions_sys/permissions01/octroyer_toutes_les_permissions2" => $questionSys,
					],
				),
				"https://exemple.com/banque_2/contenu.yml" => new Banque(
					nom: "Deuxième banque de test",
					questions: [
						"https://depot.com/roger/questions_prog/fonctions01/appeler_une_fonction" => $questionProg,
						"https://depot.com/roger/questions_sys/permissions01/octroyer_toutes_les_permissions2" => $questionSys,
					],
				),
			]);
		$mockBanqueDAO->allows("get_toutes")->with("jdoe", [])->andReturn([]);

		// User
		$mockUserDAO = Mockery::mock("progression\\dao\\UserDAO");
		$mockUserDAO->allows("get_user")->with("bob", [])->andReturn(new User(username: "bob", date_inscription: 0));
		$mockUserDAO->allows("get_user")->with("jdoe", [])->andReturn(new User(username: "jdoe", date_inscription: 0));

		// DAOFactory
		$mockDAOFactory = Mockery::mock("progression\\dao\\DAOFactory");
		$mockDAOFactory->shouldReceive("get_user_dao")->andReturn($mockUserDAO);
		$mockDAOFactory->shouldReceive("get_banque_dao")->andReturn($mockBanqueDAO);
		$mockDAOFactory->shouldReceive("get_question_dao")->andReturn($mockQuestionDAO);

		DAOFactory::setInstance($mockDAOFactory);
	}

	public function test_étant_donné_un_utilisateur_sans_banques_lorsquon_récupère_ses_banques_on_obtient_une_liste_vide()
	{
		$résultat_observé = $this->actingAs($this->user)->json_api("GET", "/user/jdoe/banques");

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonString('{"data":[]}', $résultat_observé->getContent());
	}

	public function test_étant_donné_un_utilisateur_qui_possède_deux_banques_lorsquon_les_récupère_on_obtient_les_deux_banques_avec_noms_seulement()
	{
		$résultat_observé = $this->actingAs($this->user)->json_api("GET", "/user/bob/banques");

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/banques_simples.json",
			$résultat_observé->getContent(),
		);
	}

	public function test_étant_donné_un_utilisateur_qui_possède_deux_banques_lorsquon_les_récupère_en_incluant_les_questions_on_obtient_les_deux_banques_avec_nom_et_infos_de_base_des_questions()
	{
		$résultat_observé = $this->actingAs($this->user)->json_api("GET", "/user/bob/banques?include=questions");

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/banques_avec_questions.json",
			$résultat_observé->getContent(),
		);
	}

	public function test_étant_donné_un_utilisateur_qui_possède_deux_banques_lorsquon_les_récupère_en_incluant_les_sous_banques_on_obtient_les_deux_banques_avec_nom_et_infos_de_base_des_sous_banques()
	{
		$résultat_observé = $this->actingAs($this->user)->json_api("GET", "/user/bob/banques?include=banques");

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/banques_avec_sous_banques.json",
			$résultat_observé->getContent(),
		);
	}

	public function test_étant_donné_un_utilisateur_qui_possède_deux_banques_lorsquon_les_récupère_en_incluant_les_questions_et_sous_banques_on_obtient_les_deux_banques_avec_nom_et_infos_de_base_des_questiosns_et_sous_banques()
	{
		$résultat_observé = $this->actingAs($this->user)->json_api(
			"GET",
			"/user/bob/banques?include=questions,banques",
		);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/banques_avec_questions_et_sous_banques.json",
			$résultat_observé->getContent(),
		);
	}
}
