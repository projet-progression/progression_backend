<?php
/*
   This file is part of Progression.

   Progression is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Progression is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Progression.  If not, see <https://www.gnu.org/licenses/>.
 */

use progression\ContrôleurTestCase;
use progression\dao\DAOFactory;
use progression\dao\chargeur\ChargeurException;
use progression\domaine\entité\Banque;
use progression\domaine\entité\question\{Question, QuestionProg, QuestionSys};
use progression\domaine\entité\{Exécutable, TestProg, TestSys};
use progression\domaine\entité\user\{User, Rôle, État};
use progression\UserAuthentifiable;

final class BanqueCtlTests extends ContrôleurTestCase
{
	public $user;

	public function setUp(): void
	{
		parent::setUp();

		$this->user = new UserAuthentifiable(
			username: "bob",
			date_inscription: 0,
			rôle: Rôle::NORMAL,
			état: État::ACTIF,
		);

		// Question
		$questionProg = new QuestionProg(
			titre: "Appeler une fonction paramétrée",
			objectif: "Appel d'une fonction existante recevant un paramètre",
			description: "Ceci est une question prog complète",
			enonce: "La fonction `salutations` affiche une salution autant de fois que la valeur reçue en paramètre. Utilisez-la pour faire afficher «Bonjour le monde!» autant de fois que le nombre reçu en entrée.",
			auteur: "Albert Einstein",
			licence: "poétique",
			niveau: "débutant",
			// Ébauches
			exécutables: [
				"python" => new Exécutable("print(\"Hello world\")", "python"),
				"java" => new Exécutable("System.out.println(\"Hello world\")", "java"),
			],

			// Tests
			tests: [
				new TestProg(
					nom: "2 salutations",
					sortie_attendue: "Bonjour\nBonjour\n",
					entrée: "2",
					params: "param_0 param_1",
					caché: false,
				),
				new TestProg(
					nom: "Aucune salutation",
					sortie_attendue: "Cette sortie devrait être cachée",
					entrée: "Cette entrée être caché",
					params: "Ce param devrait être caché",
					caché: true,
				),
			],
		);

		//QuestionSys avec solution sans pregmatch
		$questionSys = new QuestionSys(
			solution: "laSolution",
			feedback_pos: "Bon travail!",
			feedback_neg: "Encore un effort!",
			titre: "Octroyer toutes le permissions",
			objectif: "Octroiement de toutes les permissions.",
			description: "Ceci est une question système complète",
			enonce: "Il faut que l'étudiant trouve les commandes justes pour octroyer toutes les permissions à l'utilisateur krusty.",
			auteur: "Albert Einstein",
			licence: "poétique",
			niveau: "débutant",
			image: "l'image",
			utilisateur: "utilisateur",
			tests: [
				new TestSys("test 1", "vrai", "[ -z vrai ]", "bob"),
				new TestSys("test 2", "faux", "[ -z faux ]", "roger"),
			],
		);

		$mockQuestionDAO = Mockery::mock("progression\\dao\\question\\QuestionDAO");
		$mockQuestionDAO
			->shouldReceive("get_question")
			->with("https://depot.com/roger/questions_prog/fonctions01/appeler_une_fonction")
			->andReturn($questionProg);
		$mockQuestionDAO
			->shouldReceive("get_question")
			->with("https://depot.com/roger/questions_sys/permissions01/octroyer_toutes_les_permissions2")
			->andReturn($questionSys);
		$mockQuestionDAO
			->shouldReceive("get_question")
			->with("https://depot.com/roger/questions_invalide")
			->andThrow(new ChargeurException("Question invalide."));
		$mockQuestionDAO->shouldReceive("get_question")->with(Mockery::any())->andReturn(null);

		// Banque simple
		$mockBanqueDAO = Mockery::mock("progression\\dao\\banque\\BanqueDAO");
		$mockBanqueDAO->allows("get_banque")->with("bob", "https://exemple.com/banque_1/contenu.yml", [])->andReturn(
			new Banque(
				nom: "Première banque de test",
				questions: [
					"https://depot.com/roger/questions_prog/fonctions01/appeler_une_fonction" => new Question(
						titre: "Appeler une fonction paramétrée",
						objectif: "Appel d'une fonction existante recevant un paramètre",
						description: "Ceci est une question prog complète",
					),
					"https://depot.com/roger/questions_sys/permissions01/octroyer_toutes_les_permissions2" => new Question(
						titre: "Octroyer toutes le permissions",
						objectif: "Octroiement de toutes les permissions.",
						description: "Ceci est une question système complète",
						niveau: "débutant",
					),
				],
			),
		);
		$mockBanqueDAO
			->allows("get_banque")
			->with("bob", "https://exemple.com/banque_1/contenu.yml", ["questions"])
			->andReturn(
				new Banque(
					nom: "Première banque de test",
					questions: [
						"https://depot.com/roger/questions_prog/fonctions01/appeler_une_fonction" => $questionProg,
						"https://depot.com/roger/questions_sys/permissions01/octroyer_toutes_les_permissions2" => $questionSys,
					],
				),
			);
		// Banque complexe
		$mockBanqueDAO
			->allows("get_banque")
			->with("bob", "https://exemple.com/banque_complexe/contenu.yml", ["questions"])
			->andReturn(
				new Banque(
					nom: "Banque de test",
					banques: [
						"sous-répertoire 1" => new Banque("sous-répertoire 1"),
					],
					questions: [
						"https://depot.com/roger/questions_prog/fonctions01/appeler_une_fonction" => $questionProg,
						"https://depot.com/roger/questions_sys/permissions01/octroyer_toutes_les_permissions2" => $questionSys,
					],
				),
			);
		$mockBanqueDAO
			->allows("get_banque")
			->with("bob", "https://exemple.com/banque_complexe/contenu.yml", ["questions", "banques"])
			->andReturn(
				new Banque(
					nom: "Banque de test",
					banques: [
						"sous-répertoire 1" => new Banque(
							nom: "Banque Sous-répertoire 1",
							questions: [
								"https://exemple.com/banque_complexe/banque_2/question_3.yml" => new Question(
									titre: "Test banque de questions 3 - fichier yaml valide",
								),
							],
							banques: [
								"sous-sous-répertoire" => new Banque(nom: "sous-sous-répertoire"),
								0 => new Banque(nom: ""),
							],
						),
						"sous-répertoire 2" => new Banque(nom: "Banque Sous-répertoire 2"),
					],
					questions: [
						"https://depot.com/roger/questions_prog/fonctions01/appeler_une_fonction" => $questionProg,
						"https://depot.com/roger/questions_sys/permissions01/octroyer_toutes_les_permissions2" => $questionSys,
					],
				),
			);
		$mockBanqueDAO
			->allows("get_banque")
			->with("bob", "https://exemple.com/sous_banque_avec_url.yml", ["banques"])
			->andReturn(
				new Banque(
					nom: "Banque avec sous-banque distante",
					banques: [
						"https://exemple.com/autre_banque.yml" => new Banque(nom: "sous-banque"),
					],
				),
			);

		//Caractères spéciaux
		$mockBanqueDAO->allows("get_banque")->with("bob", "https://exemple.com/test/contenu.yml", [])->andReturn(
			new Banque(
				nom: "Banque de test",
				banques: [
					"test%2Fslash" => new Banque(nom: "test/slash"),
					"test\backslash" => new Banque(nom: "test\backslash"),
					"test#croisillon" => new Banque(nom: "test#croisillon"),
				],
			),
		);
		// Banque inexistante
		$mockBanqueDAO->allows("get_banque")->with("bob", "http://inexistante.com", [])->andReturn(null);
		// Banque inacessible
		$mockBanqueDAO
			->allows("get_banque")
			->with("bob", "https://exemple.com/banque_inaccessible/contenu.yml", [])
			->andThrow(new ChargeurException());
		// Banque avec sous-banque inaccessible
		$mockBanqueDAO
			->allows("get_banque")
			->with("bob", "https://exemple.com/sous_banque_inaccessible/contenu.yml", ["banques"])
			->andThrow(new ChargeurException());
		// Banque avec sous-banque invalide
		$mockBanqueDAO
			->allows("get_banque")
			->with("bob", "https://exemple.com/sous_banque_invalide/contenu.yml", ["banques"])
			->andThrow(new ChargeurException());
		// Banque avec question inaccessible
		$mockBanqueDAO
			->allows("get_banque")
			->with("bob", "https://exemple.com/question_inaccessible/contenu.yml", ["questions"])
			->andThrow(new ChargeurException());
		// Banque avec question invalide
		$mockBanqueDAO
			->allows("get_banque")
			->with("bob", "https://exemple.com/question_invalide/contenu.yml", ["questions"])
			->andThrow(new ChargeurException());

		// User
		$mockUserDAO = Mockery::mock("progression\\dao\\UserDAO");
		$mockUserDAO->allows("get_user")->with("bob", [])->andReturn(new User(username: "bob", date_inscription: 0));

		// DAOFactory
		$mockDAOFactory = Mockery::mock("progression\\dao\\DAOFactory");
		$mockDAOFactory->shouldReceive("get_user_dao")->andReturn($mockUserDAO);
		$mockDAOFactory->shouldReceive("get_banque_dao")->andReturn($mockBanqueDAO);
		$mockDAOFactory->shouldReceive("get_question_dao")->andReturn($mockQuestionDAO);

		DAOFactory::setInstance($mockDAOFactory);
	}

	// GET
	public function test_étant_donné_une_banque_existante_lorsquon_la_récupère_sans_inclusion_on_obtient_la_banque_avec_nom_et_infos_de_base_des_questions_et_sous_banques()
	{
		$résultat_observé = $this->actingAs($this->user)->json_api(
			"GET",
			"/banque/bob/aHR0cHM6Ly9leGVtcGxlLmNvbS9iYW5xdWVfMS9jb250ZW51LnltbA",
		);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/banque_simple.json",
			$résultat_observé->getContent(),
		);
	}

	public function test_étant_donné_une_banque_existante_lorsquon_la_récupère_en_includant_les_questions_on_obtient_la_banque_avec_nom_et_questions_complètes()
	{
		$résultat_observé = $this->actingAs($this->user)->json_api(
			"GET",
			"/banque/bob/aHR0cHM6Ly9leGVtcGxlLmNvbS9iYW5xdWVfMS9jb250ZW51LnltbA?include=questions",
		);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/banque_avec_questions.json",
			$résultat_observé->getContent(),
		);
	}

	public function test_étant_donné_une_banque_complexe_lorsquon_la_récupère_en_incluant_les_questions_on_obtient_la_banque_ses_questions_complètes_et_les_infos_de_base_des_sous_banques()
	{
		$résultat_observé = $this->actingAs($this->user)->json_api(
			"GET",
			"/banque/bob/aHR0cHM6Ly9leGVtcGxlLmNvbS9iYW5xdWVfY29tcGxleGUvY29udGVudS55bWw?include=questions",
		);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/banque_complexe_avec_questions.json",
			$résultat_observé->getContent(),
		);
	}

	public function test_étant_donné_une_banque_complexe_lorsquon_la_récupère_en_incluant_banques_et_les_questions_on_obtient_la_banque_ses_questions_sous_banques()
	{
		$résultat_observé = $this->actingAs($this->user)->json_api(
			"GET",
			"/banque/bob/aHR0cHM6Ly9leGVtcGxlLmNvbS9iYW5xdWVfY29tcGxleGUvY29udGVudS55bWw?include=questions,banques",
		);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/banque_complexe_avec_banques_et_questions.json",
			$résultat_observé->getContent(),
		);
	}

	public function test_étant_donné_une_banque_complexe_lorsquon_la_récupère_en_précisant_une_sous_banque_on_obtient_uniquement_la_sous_banque()
	{
		$résultat_observé = $this->actingAs($this->user)->json_api(
			"GET",
			"/banque/bob/aHR0cHM6Ly9leGVtcGxlLmNvbS9iYW5xdWVfY29tcGxleGUvY29udGVudS55bWwjc291cy1yw6lwZXJ0b2lyZSAx?include=questions,banques",
		);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/banque_complexe_sous-répertoire_1.json",
			$résultat_observé->getContent(),
		);
	}

	public function test_étant_donné_une_banque_complexe_lorsquon_la_récupère_en_précisant_une_sous_banque_avec_un_slash_final_on_obtient_uniquement_la_sous_banque()
	{
		$résultat_observé = $this->actingAs($this->user)->json_api(
			"GET",
			"/banque/bob/aHR0cHM6Ly9leGVtcGxlLmNvbS9iYW5xdWVfY29tcGxleGUvY29udGVudS55bWwjc291cy1yw6lwZXJ0b2lyZSAxLw?include=questions,banques",
		);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/banque_complexe_sous-répertoire_1.json",
			$résultat_observé->getContent(),
		);
	}

	public function test_étant_donné_une_banque_complexe_lorsquon_la_récupère_en_précisant_une_sous_banque_de_deuxième_niveau_on_obtient_uniquement_la_sous_banque()
	{
		$résultat_observé = $this->actingAs($this->user)->json_api(
			"GET",
			"/banque/bob/aHR0cHM6Ly9leGVtcGxlLmNvbS9iYW5xdWVfY29tcGxleGUvY29udGVudS55bWwjc291cy1yw6lwZXJ0b2lyZSAxL3NvdXMtc291cy1yw6lwZXJ0b2lyZQ?include=questions,banques",
		);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/banque_complexe_sous-sous-répertoire.json",
			$résultat_observé->getContent(),
		);
	}

	public function test_étant_donné_une_banque_complexe_lorsquon_la_récupère_en_précisant_une_sous_banque_contenant_un_slash_on_obtient_la_sous_banque()
	{
		$résultat_observé = $this->actingAs($this->user)->json_api(
			"GET",
			"/banque/bob/aHR0cHM6Ly9leGVtcGxlLmNvbS90ZXN0L2NvbnRlbnUueW1sI3Rlc3QlMjUyRnNsYXNo",
		);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/banque_avec_slash.json",
			$résultat_observé->getContent(),
		);
	}

	public function test_étant_donné_une_banque_complexe_lorsquon_la_récupère_en_précisant_une_sous_banque_contenant_un_backslash_on_obtient_la_sous_banque()
	{
		$résultat_observé = $this->actingAs($this->user)->json_api(
			"GET",
			"/banque/bob/aHR0cHM6Ly9leGVtcGxlLmNvbS90ZXN0L2NvbnRlbnUueW1sI3Rlc3QlNUNiYWNrc2xhc2g",
		);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/banque_avec_backslash.json",
			$résultat_observé->getContent(),
		);
	}

	public function test_étant_donné_une_banque_complexe_lorsquon_la_récupère_en_précisant_une_sous_banque_contenant_un_croisillon_on_obtient_la_sous_banque()
	{
		$résultat_observé = $this->actingAs($this->user)->json_api(
			"GET",
			"/banque/bob/aHR0cHM6Ly9leGVtcGxlLmNvbS90ZXN0L2NvbnRlbnUueW1sI3Rlc3QjY3JvaXNpbGxvbg",
		);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/banque_avec_croisillon.json",
			$résultat_observé->getContent(),
		);
	}

	public function test_étant_donné_une_banque_complexe_lorsquon_la_récupère_en_précisant_une_sous_banque_inexistante_on_obtient_une_erreur_404()
	{
		$this->actingAs($this->user)->json_api(
			"GET",
			"/banque/bob/aHR0cHM6Ly9leGVtcGxlLmNvbS9iYW5xdWVfY29tcGxleGUvY29udGVudS55bWwjaW5leGlzdGFudA?include=questions,banques",
		);

		$this->assertResponseStatus(404);
	}

	public function test_étant_donné_une_banque_inexistante_lorsquon_la_récupère_on_obtient_une_erreur_404()
	{
		$this->actingAs($this->user)->json_api("GET", "/banque/bob/aHR0cDovL2luZXhpc3RhbnRlLmNvbQ");

		$this->assertResponseStatus(404);
	}

	public function test_étant_donné_une_sous_banque_vide_lorsquon_la_récupère_on_obtient_la_banque_complète()
	{
		$résultat_observé = $this->actingAs($this->user)->json_api(
			"GET",
			"/banque/bob/aHR0cHM6Ly9leGVtcGxlLmNvbS9iYW5xdWVfMS9jb250ZW51LnltbCM",
		);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/banque_simple.json",
			$résultat_observé->getContent(),
		);
	}

	public function test_étant_donné_une_banque_inaccessible_lorsquon_la_récupère_on_obtient_une_erreur_422()
	{
		$this->actingAs($this->user)->json_api(
			"GET",
			"/banque/bob/aHR0cHM6Ly9leGVtcGxlLmNvbS9iYW5xdWVfaW5hY2Nlc3NpYmxlL2NvbnRlbnUueW1s",
		);

		$this->assertResponseStatus(422);
	}

	public function test_étant_donné_une_banque_complexe_avec_une_sous_banque_invalide_lorsquon_la_récupère_on_obtient_une_erreur_422()
	{
		$this->actingAs($this->user)->json_api(
			"GET",
			"/banque/bob/aHR0cHM6Ly9leGVtcGxlLmNvbS9zb3VzX2JhbnF1ZV9pbnZhbGlkZS9jb250ZW51LnltbA?include=banques",
		);

		$this->assertResponseStatus(422);
	}

	public function test_étant_donné_une_banque_complexe_avec_une_sous_banque_inaccessible_lorsquon_la_récupère_on_obtient_une_erreur_422()
	{
		$this->actingAs($this->user)->json_api(
			"GET",
			"/banque/bob/aHR0cHM6Ly9leGVtcGxlLmNvbS9zb3VzX2JhbnF1ZV9pbmFjY2Vzc2libGUvY29udGVudS55bWw?include=banques",
		);

		$this->assertResponseStatus(422);
	}

	public function test_étant_donné_une_banque_avec_une_question_invalide_lorsquon_la_récupère_on_obtient_une_erreur_422()
	{
		$this->actingAs($this->user)->json_api(
			"GET",
			"/banque/bob/aHR0cHM6Ly9leGVtcGxlLmNvbS9xdWVzdGlvbl9pbnZhbGlkZS9jb250ZW51LnltbA?include=questions",
		);

		$this->assertResponseStatus(422);
	}

	public function test_étant_donné_une_banque_avec_une_quesiton_inaccessible_lorsquon_la_récupère_on_obtient_une_erreur_422()
	{
		$this->actingAs($this->user)->json_api(
			"GET",
			"/banque/bob/aHR0cHM6Ly9leGVtcGxlLmNvbS9xdWVzdGlvbl9pbmFjY2Vzc2libGUvY29udGVudS55bWw?include=questions",
		);

		$this->assertResponseStatus(422);
	}

	// PUT
	public function test_étant_donné_un_utilisateur_lorsquon_lui_créé_une_nouvelle_banque_alors_elle_est_ajoutée_à_ses_banques_et_on_obtient_la_nouvelle_banque()
	{
		DAOFactory::getInstance()
			->get_banque_dao()
			->shouldReceive("save")
			->withArgs(function ($username, $url, Banque $banque) {
				return $username == "bob" &&
					$url == "http://nouvellebanque.com" &&
					$banque->nom == "une nouvelle banque";
			})
			->andReturn([
				"http://nouvellebanque.com" => new Banque(nom: "une nouvelle banque"),
			]);

		$résultat_observé = $this->actingAs($this->user)->json_api(
			"PUT",
			"/banque/bob/aHR0cDovL25vdXZlbGxlYmFucXVlLmNvbQ",
			[
				"data" => [
					"type" => "banque",
					"attributes" => [
						"nom" => "une nouvelle banque",
					],
				],
			],
		);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/banque_ajoutée.json",
			$résultat_observé->getContent(),
		);
	}

	public function test_étant_donné_une_banque_sans_nom_lorsquon_la_crée_on_obtient_la_banque_avec_le_nom_renseigné_dans_la_banque()
	{
		DAOFactory::getInstance()
			->get_banque_dao()
			->shouldReceive("save")
			->withArgs(function ($username, $url, Banque $banque) {
				return $username == "bob" && $url == "http://nouvellebanque.com" && $banque->nom == null;
			})
			->andReturn(["http://nouvellebanque.com" => new Banque(nom: "Ma banque")]);

		$résultat_observé = $this->actingAs($this->user)->json_api(
			"PUT",
			"/banque/bob/aHR0cDovL25vdXZlbGxlYmFucXVlLmNvbQ",
			[
				"data" => [
					"type" => "banque",
					"attributes" => [],
				],
			],
		);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/banque_sans_nom.json",
			$résultat_observé->getContent(),
		);
	}
	public function test_étant_donné_une_banque_inaccessible_lorsquon_la_crée_on_obtient_la_banque_avec_le_nom_donné()
	{
		DAOFactory::getInstance()
			->get_banque_dao()
			->shouldReceive("save")
			->withArgs(function ($username, $url, Banque $banque) {
				return $username == "bob" && $url == "http://inexistante.com" && $banque->nom == "une nouvelle banque";
			})
			->andReturn(["http://inexistante.com" => new Banque(nom: "une nouvelle banque")]);

		$résultat_observé = $this->actingAs($this->user)->json_api(
			"PUT",
			"/banque/bob/aHR0cDovL2luZXhpc3RhbnRlLmNvbQ",
			[
				"data" => [
					"type" => "banque",
					"attributes" => [
						"nom" => "une nouvelle banque",
					],
				],
			],
		);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/banque_inexistante_ajoutée.json",
			$résultat_observé->getContent(),
		);
	}

	public function test_étant_donné_une_banque_sans_url_valide_lorsquon_la_crée_on_obtient_une_erreur_400()
	{
		DAOFactory::getInstance()->get_banque_dao()->shouldNotReceive("save");

		$résultat_observé = $this->actingAs($this->user)->json_api("PUT", "/banque/bob/pas_un_url_encodé", [
			"data" => [
				"type" => "banque",
				"attributes" => [
					"nom" => "une nouvelle banque",
				],
			],
		]);

		$this->assertResponseStatus(400);
	}

	//POST
	public function test_étant_donné_un_utilisateur_lorsquon_lui_ajoute_une_nouvelle_banque_alors_elle_est_ajoutée_à_ses_banques_et_on_obtient_la_nouvelle_banque()
	{
		DAOFactory::getInstance()
			->get_banque_dao()
			->shouldReceive("save")
			->withArgs(function ($username, $url, Banque $banque) {
				return $username == "bob" &&
					$url == "http://nouvellebanque.com" &&
					$banque->nom == "une nouvelle banque";
			})
			->andReturn([
				"http://nouvellebanque.com" => new Banque(nom: "une nouvelle banque"),
			]);

		$résultat_observé = $this->actingAs($this->user)->json_api("POST", "/user/bob/banques", [
			"data" => [
				"type" => "banque",
				"id" => "bob/aHR0cDovL25vdXZlbGxlYmFucXVlLmNvbQ",
				"attributes" => [
					"nom" => "une nouvelle banque",
				],
			],
		]);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/banque_ajoutée.json",
			$résultat_observé->getContent(),
		);
	}

	public function test_étant_donné_une_banque_sans_nom_lorsquon_lajoute_on_obtient_la_banque_avec_un_nom_vide()
	{
		DAOFactory::getInstance()
			->get_banque_dao()
			->shouldReceive("save")
			->withArgs(function ($username, $url, Banque $banque) {
				return $username == "bob" && $url == "http://nouvellebanque.com" && $banque->nom == null;
			})
			->andReturn(["http://nouvellebanque.com" => new Banque()]);

		$résultat_observé = $this->actingAs($this->user)->json_api("POST", "/user/bob/banques", [
			"data" => [
				"type" => "banque",
				"id" => "bob/aHR0cDovL25vdXZlbGxlYmFucXVlLmNvbQ",
				"attributes" => [],
			],
		]);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/banque_ajoutée_sans_nom.json",
			$résultat_observé->getContent(),
		);
	}

	public function test_étant_donné_une_banque_sans_url_valide_encodé_lorsquon_la_créée_on_obtient_une_erreur_400()
	{
		DAOFactory::getInstance()->get_banque_dao()->shouldNotReceive("save");

		$résultat_observé = $this->actingAs($this->user)->json_api("POST", "/user/bob/banques", [
			"data" => [
				"type" => "banque",
				"id" => "bob/https%3A%2F%2Furl%2Fnon%2Fencod%C3%A9",
				"attributes" => [
					"nom" => "une nouvelle banque",
				],
			],
		]);

		$this->assertResponseStatus(400);
	}

	public function test_étant_donné_une_banque_sans_url_valide_lorsquon_la_créée_on_obtient_une_erreur_400()
	{
		DAOFactory::getInstance()->get_banque_dao()->shouldNotReceive("save");

		$résultat_observé = $this->actingAs($this->user)->json_api("POST", "/user/bob/banques", [
			"data" => [
				"type" => "banque",
				"id" => "bob/cGFzX3VuX3VybA",
				"attributes" => [
					"nom" => "une nouvelle banque",
				],
			],
		]);

		$this->assertResponseStatus(400);
	}
}
