<?php
/*
This file is part of Progression.

Progression is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Progression is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Progression.  If not, see <https://www.gnu.org/licenses/>.
*/

use progression\ContrôleurTestCase;

use Illuminate\Support\Facades\{Config, App};
use progression\dao\DAOFactory;
use progression\http\contrôleur\GénérateurAléatoire;
use progression\domaine\entité\user\{User, Rôle, État};
use progression\UserAuthentifiable;

final class TokenCtlTests extends ContrôleurTestCase
{
	public UserAuthentifiable $user;

	public function setUp(): void
	{
		parent::setUp();

		Gate::define("utilisateur-auth-par-mdp-ou-clé", function () {
			return true;
		});

		$this->user = new UserAuthentifiable(
			username: "utilisateur_lambda",
			date_inscription: 0,
			rôle: Rôle::NORMAL,
			état: État::ACTIF,
		);

		$mockUserDAO = Mockery::mock("progression\\dao\\UserDAO");
		$mockUserDAO->shouldReceive("get_user")->with("utilisateur_lambda")->andReturn($this->user);

		$mockDAOFactory = Mockery::mock("progression\\dao\\DAOFactory");
		$mockDAOFactory->shouldReceive("get_user_dao")->andReturn($mockUserDAO);
		DAOFactory::setInstance($mockDAOFactory);

		$générateur = Mockery::mock("progression\\http\\contrôleur\\GénérateurAléatoire");
		$générateur
			->shouldReceive("générer_chaîne_aléatoire")
			->with(64)
			->andReturn("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
		GénérateurAléatoire::set_instance($générateur);
	}

	public function test_étant_donné_un_utilisateur_authentifié_par_mdp_lorsquon_post_un_token_dauthentification_on_obtient_un_token_avec_le_username()
	{
		Config::set("app.version", "1.2.3");
		Config::set("jwt.secret", "secret");

		$résultat_obtenu = $this->actingAs($this->user)->json_api("POST", "/user/utilisateur_lambda/tokens", [
			"data" => [
				"type" => "token",
				"attributes" => [
					"data" => ["données" => "une donnée"],
					"username" => "utilisateur_lambda",
					"expiration" => 0,
				],
			],
		]);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/token_identification.json",
			$résultat_obtenu->getContent(),
		);
	}

	public function test_étant_donné_un_token_qui_donne_accès_à_une_ressource_lorsquon_effectue_un_post_on_obtient_un_token_avec_les_ressources_voulues_sans_expiration()
	{
		Config::set("app.version", "1.2.3");
		Config::set("jwt.secret", "secret");

		$résultat_obtenu = $this->actingAs($this->user)->json_api("POST", "/user/utilisateur_lambda/tokens", [
			"data" => [
				"type" => "token",
				"attributes" => [
					"data" => ["données" => "une donnée"],
					"ressources" => [
						"ressource" => ["url" => "test", "method" => "POST"],
					],
					"expiration" => 0,
				],
			],
		]);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/token_une_ressources.json",
			$résultat_obtenu->getContent(),
		);
	}

	public function test_étant_donné_un_token_qui_donne_accès_à_plus_dune_ressource_lorsquon_effectue_un_post_on_obtient_un_token_avec_les_ressources_voulues_sans_expiration()
	{
		Config::set("app.version", "1.2.3");
		Config::set("jwt.secret", "secret");

		$résultat_obtenu = $this->actingAs($this->user)->json_api("POST", "/user/utilisateur_lambda/tokens", [
			"data" => [
				"type" => "token",
				"attributes" => [
					"data" => ["données" => "une donnée"],
					"ressources" => [
						"ressource" => ["url" => "test", "method" => "POST"],
						"autre_ressource" => ["url" => "autre_test", "method" => "POST"],
					],
					"expiration" => 0,
				],
			],
		]);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/token_deux_ressources.json",
			$résultat_obtenu->getContent(),
		);
	}

	public function test_étant_donné_un_token_qui_donne_accès_à_une_date_dexpiration_spécifique_lorsquon_effectue_un_post_on_obtient_un_token_avec_cette_expiration()
	{
		Config::set("app.version", "1.2.3");
		Config::set("jwt.secret", "secret");

		$résultat_obtenu = $this->actingAs($this->user)->json_api("POST", "/user/utilisateur_lambda/tokens", [
			"data" => [
				"type" => "token",
				"attributes" => [
					"data" => ["données" => "une autre donnée"],
					"ressources" => [
						"ressources_test" => [
							"url" => "test",
							"method" => "POST",
						],
					],
					"expiration" => 1685831340,
				],
			],
		]);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/token_ressources_avec_expiration_spécifique.json",
			$résultat_obtenu->getContent(),
		);
	}

	public function test_étant_donné_un_token_qui_donne_accès_à_un_date_dexpiration_relative_de_300s_lorsquon_effectue_un_post_on_obtient_un_token_avec_expiration_plus_tard_de_300s()
	{
		Config::set("app.version", "1.2.3");
		Config::set("jwt.secret", "secret");

		$résultat_obtenu = $this->actingAs($this->user)->json_api("POST", "/user/utilisateur_lambda/tokens", [
			"data" => [
				"type" => "token",
				"attributes" => [
					"data" => ["données" => "une autre donnée"],
					"ressources" => [
						"ressources_test" => [
							"url" => "test",
							"method" => "POST",
						],
					],
					"expiration" => "+300",
				],
			],
		]);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/token_ressources_avec_expiration_relative.json",
			$résultat_obtenu->getContent(),
		);
	}

	public function test_étant_donné_un_token_qui_donne_accès_à_un_date_dexpiration_invalide_lorsquon_effectue_un_post_on_obtient_une_erreur_400()
	{
		$résultat_obtenu = $this->actingAs($this->user)->json_api("POST", "/user/utilisateur_lambda/tokens", [
			"data" => [
				"type" => "token",
				"attributes" => [
					"data" => ["données" => "une autre donnée"],
					"ressources" => [
						"ressources_test" => [
							"url" => "test",
							"method" => "POST",
						],
					],
					"expiration" => "demain",
				],
			],
		]);

		$this->assertResponseStatus(400);
		$this->assertEquals(
			'{"errors":[{"title":{"expiration":["Le champ expiration doit représenter une date relative ou absolue."]},"status":400}]}',
			$résultat_obtenu->getContent(),
		);
	}

	public function test_étant_donné_une_ressource_sans_url_lorsquon_effectue_un_post_on_obtient_une_erreur_400()
	{
		$résultat_obtenu = $this->actingAs($this->user)->json_api("POST", "/user/utilisateur_lambda/tokens", [
			"data" => [
				"type" => "token",
				"attributes" => [
					"data" => ["données" => "une autre donnée"],
					"ressources" => [
						"ressources_test" => [
							"method" => "POST",
						],
					],
					"expiration" => "0",
				],
			],
		]);

		$this->assertResponseStatus(400);
		$this->assertEquals(
			'{"errors":[{"title":{"ressources.ressources_test.url":["Le champ ressources.ressources_test.url est obligatoire."]},"status":400}]}',
			$résultat_obtenu->getContent(),
		);
	}

	public function test_étant_donné_une_ressource_sans_méthode_lorsquon_effectue_un_post_on_obtient_une_erreur_400()
	{
		$résultat_obtenu = $this->actingAs($this->user)->json_api("POST", "/user/utilisateur_lambda/tokens", [
			"data" => [
				"type" => "token",
				"attributes" => [
					"data" => ["données" => "une autre donnée"],
					"ressources" => [
						"ressources_test" => [
							"url" => "test",
						],
					],
					"expiration" => "0",
				],
			],
		]);

		$this->assertResponseStatus(400);
		$this->assertEquals(
			'{"errors":[{"title":{"ressources.ressources_test.method":["Le champ ressources.ressources_test.method est obligatoire."]},"status":400}]}',
			$résultat_obtenu->getContent(),
		);
	}

	public function test_étant_donné_un_token_sans_attributs_lorsquon_effectue_un_post_on_obtient_un_token_didentification_avec_valeurs_par_défaut()
	{
		Config::set("app.version", "1.2.3");
		$résultat_obtenu = $this->actingAs($this->user)->json_api("POST", "/user/utilisateur_lambda/tokens", [
			"data" => [
				"type" => "token",
				"attributes" => [],
			],
		]);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/token_défaut.json",
			$résultat_obtenu->getContent(),
		);
	}

	public function test_étant_donné_un_token_sans_champ_attributs_lorsquon_effectue_un_post_on_obtient_un_token_didentification_avec_valeurs_par_défaut()
	{
		Config::set("app.version", "1.2.3");
		$résultat_obtenu = $this->actingAs($this->user)->json_api("POST", "/user/utilisateur_lambda/tokens", [
			"data" => [
				"type" => "token",
			],
		]);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/token_défaut.json",
			$résultat_obtenu->getContent(),
		);
	}

	public function test_étant_donné_un_token_avec_ressources_vides_lorsquon_effectue_un_post_on_obtient_un_token_identification_sans_ressources()
	{
		Config::set("app.version", "1.2.3");
		$résultat_obtenu = $this->actingAs($this->user)->json_api("POST", "/user/utilisateur_lambda/tokens", [
			"data" => [
				"type" => "token",
				"attributes" => [
					"ressources" => [],
				],
			],
		]);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/token_défaut.json",
			$résultat_obtenu->getContent(),
		);
	}

	public function test_étant_donné_un_token_sans_fingerprint_lorsquon_effectue_un_post_on_obtient_un_token_sans_fingerprint()
	{
		Config::set("app.version", "1.2.3");
		Config::set("jwt.secret", "secret");

		$résultat_obtenu = $this->actingAs($this->user)->json_api("POST", "/user/utilisateur_lambda/tokens", [
			"data" => [
				"type" => "token",
				"attributes" => [
					"data" => ["données" => "une donnée"],
					"ressources" => [
						"ressources" => ["url" => "test", "method" => "POST"],
					],
					"expiration" => 0,
					"fingerprint" => false,
				],
			],
		]);

		$this->assertResponseStatus(200);
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/token_ressources.json",
			$résultat_obtenu->getContent(),
		);
	}

	public function test_étant_donné_un_contexte_lorsquon_génère_un_token_avec_expiration_spécifique_on_reçoit_le_hash_du_contexte_et_un_cookie_sécure_expirant_en_même_temps()
	{
		App::shouldReceive("environment")
			->with(["prod", "staging"])
			->andReturn(true);
		Config::set("app.version", "1.2.3");
		Config::set("jwt.secret", "secret");
		Config::set("app.mode", "prod");

		$résultat_obtenu = $this->actingAs($this->user)->json_api("POST", "user/utilisateur_lambda/tokens", [
			"data" => [
				"type" => "token",
				"attributes" => [
					"data" => ["données" => "une autre donnée"],
					"ressources" => [
						"ressources_test" => [
							"url" => "test",
							"method" => "POST",
						],
					],
					"fingerprint" => true,
					"expiration" => 1685831340,
				],
			],
		]);

		$this->assertResponseStatus(200);
		$this->assertEquals(
			"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
			$résultat_obtenu->headers->getCookies()[0]->getValue(),
		);
		$this->assertEquals("contexte_token", $résultat_obtenu->headers->getCookies()[0]->getName());
		$this->assertEquals(1685831340, $résultat_obtenu->headers->getCookies()[0]->getExpiresTime());
		$this->assertTrue($résultat_obtenu->headers->getCookies()[0]->isSecure());
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/token_avec_contexte_expiration_spécifique.json",
			$résultat_obtenu->getContent(),
		);
	}

	public function test_étant_donné_un_contexte_lorsquon_génère_un_token_avec_expiration_relatif_on_reçoit_le_hash_du_contexte_et_un_cookie_sécure_expirant_en_même_temps()
	{
		Config::set("app.version", "1.2.3");
		Config::set("jwt.secret", "secret");

		$résultat_obtenu = $this->actingAs($this->user)->json_api("POST", "user/utilisateur_lambda/tokens", [
			"data" => [
				"type" => "token",
				"attributes" => [
					"data" => ["données" => "une autre donnée"],
					"ressources" => [
						"ressources_test" => [
							"url" => "test",
							"method" => "POST",
						],
					],
					"fingerprint" => true,
					"expiration" => "+300",
				],
			],
		]);

		$this->assertResponseStatus(200);

		$this->assertEquals(
			"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
			$résultat_obtenu->headers->getCookies()[0]->getValue(),
		);
		$this->assertEquals(990446700, $résultat_obtenu->headers->getCookies()[0]->getExpiresTime());
		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/token_avec_contexte_expiration_relative.json",
			$résultat_obtenu->getContent(),
		);
	}
}
