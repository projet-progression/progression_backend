<?php

use progression\ContrôleurTestCase;

use Illuminate\Support\Facades\Config;

final class ContrôleurFrontalTests extends ContrôleurTestCase
{
	public function test_étant_donné_un_utilisateur_inexistant_avec_authentification_lorsquon_inscrit_sans_fournir_de_type_on_obtient_une_erreur_409()
	{
		Config::set("authentification.local", false);
		Config::set("authentification.ldap", false);

		$résultat_observé = $this->json_api("PUT", "/user/bob", [
			"data" => [
				"attributes" => [
					"username" => "autre_nom",
				],
			],
		]);

		$this->assertResponseStatus(409);
		$this->assertEquals(
			'{"errors":[{"title":{"type":["Le champ type est manquant ou ne correspond pas à un type valide"]},"status":409}]}',
			$résultat_observé->content(),
		);
	}

	public function test_étant_donné_un_utilisateur_inexistant_avec_authentification_lorsquon_inscrit_sans_fournir_de_champ_attributs_on_obtient_une_erreur_400()
	{
		Config::set("authentification.local", false);
		Config::set("authentification.ldap", false);

		$résultat_observé = $this->json_api("PUT", "/user/bob", [
			"data" => [
				"type" => "user",
			],
		]);

		$this->assertResponseStatus(400);
		$this->assertEquals(
			'{"errors":[{"title":{"username":["Le champ username est obligatoire."]},"status":400}]}',
			$résultat_observé->content(),
		);
	}

	public function test_étant_donné_un_utilisateur_inexistant_avec_authentification_lorsquon_inscrit_sans_fournir_de_données_on_obtient_une_erreur_409()
	{
		Config::set("authentification.local", false);
		Config::set("authentification.ldap", false);

		$résultat_observé = $this->json_api("PUT", "/user/bob", []);

		$this->assertResponseStatus(409);
		$this->assertEquals(
			'{"errors":[{"title":{"type":["Le champ type est manquant ou ne correspond pas à un type valide"]},"status":409}]}',
			$résultat_observé->content(),
		);
	}
}

?>
