<?php
/*
	 This file is part of Progression.

	 Progression is free software: you can redistribute it and/or modify
	 it under the terms of the GNU General Public License as published by
	 the Free Software Foundation, either version 3 of the License, or
	 (at your option) any later version.

	 Progression is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU General Public License for more details.

	 You should have received a copy of the GNU General Public License
	 along with Progression.  If not, see <https://www.gnu.org/licenses/>.
 */

use progression\ContrôleurTestCase;
use progression\UserAuthentifiable;
use progression\dao\DAOFactory;
use progression\dao\mail\NotificationCommentaireMail;
use progression\domaine\entité\Commentaire;
use progression\domaine\entité\user\{User, Rôle, État};

use Illuminate\Support\Facades\Mail;

final class CommentaireCtlTests extends ContrôleurTestCase
{
	public $user;
	public $bob;
	public $admin;

	public $mockCommentaireDAO;
	public $mockUserDAO;

	public function setup(): void
	{
		parent::setUp();

		$this->jdoe = new UserAuthentifiable(
			username: "jdoe",
			courriel: "jdoe@test.com",
			date_inscription: 0,
			rôle: Rôle::NORMAL,
			état: État::ACTIF,
		);

		$this->bob = new UserAuthentifiable(
			username: "bob",
			date_inscription: 0,
			rôle: Rôle::NORMAL,
			état: État::ACTIF,
		);

		$this->oteur = new UserAuthentifiable(
			username: "oteur",
			date_inscription: 0,
			rôle: Rôle::NORMAL,
			état: État::ACTIF,
		);

		$this->bob = new UserAuthentifiable(
			username: "bob",
			date_inscription: 0,
			rôle: Rôle::NORMAL,
			état: État::ACTIF,
		);

		$this->admin = new UserAuthentifiable(
			username: "admin",
			date_inscription: 0,
			rôle: Rôle::ADMIN,
			état: État::ACTIF,
		);

		// Commentaire
		$commentaire_jdoe = new Commentaire("Bon travail", $this->jdoe, 1615696276, 5);
		$commentaire_autre = new Commentaire(
			"Bon travail",
			new User(username: "oteur", date_inscription: 0),
			1615696277,
			5,
		);

		$this->mockCommentaireDAO = Mockery::mock("progression\\dao\\CommentaireDAO");

		$this->mockCommentaireDAO
			->shouldReceive("get_commentaires_par_tentative")
			->with("jdoe", "prog1/les_fonctions_01/appeler_une_fonction_paramétrée", 1614374490)
			->andReturn($commentaire_jdoe);

		$this->mockCommentaireDAO
			->shouldReceive("get_commentaire")
			->with("jdoe", "prog1/les_fonctions_01/appeler_une_fonction_paramétrée", 1614374490, 1, [])
			->andReturn($commentaire_autre);

		$this->mockCommentaireDAO
			->shouldReceive("get_commentaire")
			->with("bob", "prog1/les_fonctions_01/appeler_une_fonction_paramétrée", 1614374490, 1)
			->andReturn($commentaire_jdoe);

		$this->mockCommentaireDAO
			->shouldReceive("get_commentaire")
			->with("bob", "prog1/les_fonctions_01/appeler_une_fonction_paramétrée", 1614374490, 2, [])
			->andReturn($commentaire_autre);

		$this->mockCommentaireDAO->shouldReceive("get_commentaire")->andReturn(null);

		// User
		$this->mockUserDAO = Mockery::mock("progression\\dao\\UserDAO");
		$this->mockUserDAO->shouldReceive("get_user")->with("jdoe", [])->andReturn($this->jdoe);

		$this->mockUserDAO->shouldReceive("get_user")->with("bob", [])->andReturn($this->bob);

		$this->mockUserDAO->shouldReceive("get_user")->with("jojo", [])->andReturn(null);

		// DAOFactory
		$mockDAOFactory = Mockery::mock("progression\\dao\\DAOFactory")->makePartial();
		$mockDAOFactory->shouldReceive("get_commentaire_dao")->andReturn($this->mockCommentaireDAO);
		$mockDAOFactory->shouldReceive("get_user_dao")->andReturn($this->mockUserDAO);

		DAOFactory::setInstance($mockDAOFactory);
	}

	// POST
	public function test_étant_donné_le_username_dun_utilisateur_le_chemin_dune_question_et_le_timestamp_lorsquon_appelle_post_on_obtient_le_commentaire_avec_ses_relations_sous_forme_json()
	{
		$commentaire = new Commentaire("Bon travail", new User(username: "jdoe", date_inscription: 0), 1615696276, 5);

		DAOFactory::getInstance()
			->get_commentaire_dao()
			->shouldReceive("save")
			->andReturn([0 => $commentaire]);

		$résultat_obtenu = $this->actingAs($this->jdoe)->json_api(
			"POST",
			"/tentative/jdoe/cHJvZzEvbGVzX2ZvbmN0aW9uc18wMS9hcHBlbGVyX3VuZV9mb25jdGlvbl9wYXJhbcOpdHLDqWU/1614374490/commentaires",
			[
				"data" => [
					"type" => "commentaire",
					"attributes" => [
						"message" => "Bon travail",
						"numéro_ligne" => 5,
					],
				],
			],
		);
		$this->assertResponseStatus(200);

		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/commentaireCtlTest_1.json",
			$résultat_obtenu->getContent(),
		);
	}

	public function test_étant_donné_le_message_dun_commentaire_non_fourni_dans_la_requete_lorsquon_appelle_post_avec_un_commentaire_on_obtient_une_erreur_400()
	{
		$résultat_obtenu = $this->actingAs($this->jdoe)->json_api(
			"POST",
			"/tentative/jdoe/cHJvZzEvbGVzX2ZvbmN0aW9uc18wMS9hcHBlbGVyX3VuZV9mb25jdGlvbl9wYXJhbcOpdHLDqWU/1614374490/commentaires",
			[
				"data" => [
					"type" => "commentaire",
					"attributes" => ["numéro_ligne" => 5],
				],
			],
		);
		$this->assertResponseStatus(400);
		$this->assertEquals(
			'{"errors":[{"title":{"message":["Le champ message est obligatoire."]},"status":400}]}',
			$résultat_obtenu->getContent(),
		);
	}

	public function test_étant_donné_le_numero_ligne_dun_commentaire_non_entier_dans_la_requete_lorsquon_appelle_post_avec_un_commentaire_on_obtient_une_erreur_400()
	{
		$résultat_obtenu = $this->actingAs($this->jdoe)->json_api(
			"POST",
			"/tentative/jdoe/cHJvZzEvbGVzX2ZvbmN0aW9uc18wMS9hcHBlbGVyX3VuZV9mb25jdGlvbl9wYXJhbcOpdHLDqWU/1614374490/commentaires",
			[
				"data" => [
					"type" => "commentaire",
					"attributes" => [
						"message" => "Bon travail",
						"numéro_ligne" => "numero non entier",
					],
				],
			],
		);
		$this->assertResponseStatus(400);
		$this->assertEquals(
			'{"errors":[{"title":{"numéro_ligne":["Le champ numéro ligne doit être un entier."]},"status":400}]}',
			$résultat_obtenu->getContent(),
		);
	}

	public function test_étant_donné_un_commentaire_incluant_un_créateur_lorsquon_l_ajoute_à_sa_propre_tentative_il_est_sauvegardé_et_on_obtient_le_commentaire()
	{
		$commentaire = new Commentaire("Bon travail", new User(username: "jdoe", date_inscription: 0), 1615696276, 5);

		DAOFactory::getInstance()
			->get_commentaire_dao()
			->shouldReceive("save")
			->andReturn([0 => $commentaire]);
		$résultat_obtenu = $this->actingAs($this->jdoe)->json_api(
			"POST",
			"/tentative/jdoe/cHJvZzEvbGVzX2ZvbmN0aW9uc18wMS9hcHBlbGVyX3VuZV9mb25jdGlvbl9wYXJhbcOpdHLDqWU/1614374490/commentaires",
			[
				"data" => [
					"type" => "commentaire",
					"attributes" => [
						"message" => "Bon travail",
						"créateur" => "jdoe",
						"numéro_ligne" => "3",
					],
				],
			],
		);
		$this->assertResponseStatus(200);

		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/commentaireCtlTest_1.json",
			$résultat_obtenu->getContent(),
		);
	}

	public function test_étant_donné_un_commentaire_d_un_autre_créateur_lorsquon_l_ajoute_à_sa_propre_tentative_on_obtient_une_erreur_403()
	{
		$résultat_obtenu = $this->actingAs($this->jdoe)->json_api(
			"POST",
			"/tentative/jdoe/cHJvZzEvbGVzX2ZvbmN0aW9uc18wMS9hcHBlbGVyX3VuZV9mb25jdGlvbl9wYXJhbcOpdHLDqWU/1614374490/commentaires",
			[
				"data" => [
					"type" => "commentaire",
					"attributes" => [
						"message" => "Bon travail",
						"créateur" => "bob",
						"numéro_ligne" => "3",
					],
				],
			],
		);
		$this->assertResponseStatus(403);
	}

	public function test_étant_donné_un_commentaire_incluant_un_créateur_lorsquon_l_ajoute_la_tentative_dun_autre_utilisateur_il_est_sauvegardé_et_on_obtient_le_commentaire()
	{
		$commentaire = new Commentaire("Bon travail", new User(username: "bob", date_inscription: 0), 1615696276, 5);

		DAOFactory::getInstance()
			->get_commentaire_dao()
			->shouldReceive("save")
			->andReturn([0 => $commentaire]);

		$résultat_obtenu = $this->actingAs($this->bob)->json_api(
			"POST",
			"/tentative/jdoe/cHJvZzEvbGVzX2ZvbmN0aW9uc18wMS9hcHBlbGVyX3VuZV9mb25jdGlvbl9wYXJhbcOpdHLDqWU/1614374490/commentaires",
			[
				"data" => [
					"type" => "commentaire",
					"attributes" => [
						"message" => "Bon travail",
						"créateur" => "bob",
						"numéro_ligne" => "3",
					],
				],
			],
		);
		$this->assertResponseStatus(200);

		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/commentaireCtlTest_commentaire_de_bob.json",
			$résultat_obtenu->getContent(),
		);
	}

	public function test_étant_donné_un_commentaire_d_un_autre_créateur_lorsquon_l_ajoute_à_la_tentative_dun_autre_utilisateur_on_obtient_une_erreur_403()
	{
		$résultat_obtenu = $this->actingAs($this->bob)->json_api(
			"POST",
			"/tentative/jdoe/cHJvZzEvbGVzX2ZvbmN0aW9uc18wMS9hcHBlbGVyX3VuZV9mb25jdGlvbl9wYXJhbcOpdHLDqWU/1614374490/commentaires",
			[
				"data" => [
					"type" => "commentaire",
					"attributes" => [
						"message" => "Bon travail",
						"créateur" => "jdoe",
						"numéro_ligne" => "3",
					],
				],
			],
		);
		$this->assertResponseStatus(403);
	}

	public function test_étant_donné_un_commentaire_sans_créateur_lorsquon_l_ajoute_à_sa_propre_tentative_il_est_sauvegardé_et_on_obtient_le_commentaire()
	{
		$commentaire = new Commentaire("Bon travail", new User(username: "jdoe", date_inscription: 0), 1615696276, 5);

		DAOFactory::getInstance()
			->get_commentaire_dao()
			->shouldReceive("save")
			->andReturn([0 => $commentaire]);

		$résultat_obtenu = $this->actingAs($this->jdoe)->json_api(
			"POST",
			"/tentative/jdoe/cHJvZzEvbGVzX2ZvbmN0aW9uc18wMS9hcHBlbGVyX3VuZV9mb25jdGlvbl9wYXJhbcOpdHLDqWU/1614374490/commentaires",
			[
				"data" => [
					"type" => "commentaire",
					"attributes" => [
						"message" => "Bon travail",
						"numéro_ligne" => "3",
					],
				],
			],
		);
		$this->assertResponseStatus(200);

		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/commentaireCtlTest_1.json",
			$résultat_obtenu->getContent(),
		);
	}

	public function test_étant_donné_un_commentaire_sans_créateur_lorsquon_l_ajoute_la_tentative_dun_autre_utilisateur_il_est_sauvegardé_et_on_obtient_le_commentaire()
	{
		$commentaire = new Commentaire("Bon travail", new User(username: "bob", date_inscription: 0), 1615696276, 5);

		DAOFactory::getInstance()
			->get_commentaire_dao()
			->shouldReceive("save")
			->andReturn([0 => $commentaire]);

		$résultat_obtenu = $this->actingAs($this->bob)->json_api(
			"POST",
			"/tentative/jdoe/cHJvZzEvbGVzX2ZvbmN0aW9uc18wMS9hcHBlbGVyX3VuZV9mb25jdGlvbl9wYXJhbcOpdHLDqWU/1614374490/commentaires",
			[
				"data" => [
					"type" => "commentaire",
					"attributes" => [
						"message" => "Bon travail",
						"numéro_ligne" => "3",
					],
				],
			],
		);
		$this->assertResponseStatus(200);

		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/commentaireCtlTest_commentaire_de_bob.json",
			$résultat_obtenu->getContent(),
		);
	}

	public function test_étant_donné_un_commentaire_d_un_créateur_inexistant_lorsquun_admin_l_ajoute_à_une_tentative_on_obtient_une_erreur_400()
	{
		$résultat_obtenu = $this->actingAs($this->admin)->json_api(
			"POST",
			"/tentative/jdoe/cHJvZzEvbGVzX2ZvbmN0aW9uc18wMS9hcHBlbGVyX3VuZV9mb25jdGlvbl9wYXJhbcOpdHLDqWU/1614374490/commentaires",
			[
				"data" => [
					"type" => "commentaire",
					"attributes" => [
						"message" => "Bon travail",
						"créateur" => "jojo",
						"numéro_ligne" => "3",
					],
				],
			],
		);
		$this->assertEquals(400, $résultat_obtenu->status());
		$this->assertEquals(
			'{"errors":[{"title":"Créateur inexistant.","status":400}]}',
			$résultat_obtenu->getContent(),
		);
	}

	public function test_étant_donné_un_commentaire_d_un_créateur_existant_lorsquon_l_ajoute_à_une_tentative_il_est_sauvegardé_et_on_obtient_le_commentaire_et_un_courriel_de_notification_est_envoyé()
	{
		$commentaire_créé = new Commentaire(
			"Bon travail",
			new User("bob", date_inscription: 0, état: État::ACTIF),
			1653690241,
			3,
		);

		DAOFactory::getInstance()
			->get_commentaire_dao()
			->shouldReceive("save")
			->once()
			->andReturn([0 => $commentaire_créé]);

		Mail::fake();

		$résultat_obtenu = $this->actingAs($this->bob)->json_api(
			"POST",
			"/tentative/jdoe/cHJvZzEvbGVzX2ZvbmN0aW9uc18wMS9hcHBlbGVyX3VuZV9mb25jdGlvbl9wYXJhbcOpdHLDqWU/1614374490/commentaires",
			[
				"data" => [
					"type" => "commentaire",
					"attributes" => [
						"message" => "Bon travail",
						"numéro_ligne" => "3",
					],
				],
			],
		);
		$this->assertResponseStatus(200);

		$this->assertJsonStringEqualsJsonFile(
			__DIR__ . "/résultats_attendus/commentaireCtlTest_bob.json",
			$résultat_obtenu->getContent(),
		);

		Mail::assertSent(NotificationCommentaireMail::class, function (NotificationCommentaireMail $mail) {
			return $mail->hasTo("jdoe@test.com") && $mail->subject == "Vous avez reçu un commentaire de bob";
		});
	}

	// DELETE
	public function test_étant_donné_un_commentaire_inexistant_sur_sa_propre_tentative_lorsquon_le_supprime_on_obtient_un_code_204()
	{
		DAOFactory::getInstance()
			->get_commentaire_dao()
			->shouldReceive("supprimer")
			->withArgs(function ($user, $question_uri, $date_soumission, $numéro) {
				return $numéro == 99;
			});

		$résultat_obtenu = $this->actingAs($this->jdoe)->json_api(
			"DELETE",
			"/commentaire/jdoe/cHJvZzEvbGVzX2ZvbmN0aW9uc18wMS9hcHBlbGVyX3VuZV9mb25jdGlvbl9wYXJhbcOpdHLDqWU/1614374490/99",
			[],
		);

		$this->assertEquals(204, $résultat_obtenu->status());
	}

	public function test_étant_donné_un_commentaire_laissé_sur_la_tentative_dautrui_lorsquon_le_supprime_on_obtient_un_code_204()
	{
		DAOFactory::getInstance()
			->get_commentaire_dao()
			->shouldReceive("supprimer")
			->withArgs(function ($user, $question_uri, $date_soumission, $numéro) {
				return $numéro == 1;
			});

		$résultat_obtenu = $this->actingAs($this->jdoe)->json_api(
			"DELETE",
			"/commentaire/bob/cHJvZzEvbGVzX2ZvbmN0aW9uc18wMS9hcHBlbGVyX3VuZV9mb25jdGlvbl9wYXJhbcOpdHLDqWU/1614374490/1",
			[],
		);

		$this->assertEquals(204, $résultat_obtenu->status());
	}

	public function test_étant_donné_un_commentaire_laissé_par_autrui_sur_sa_propre_tentative_lorsquon_le_supprime_on_obtient_une_erreur_403()
	{
		DAOFactory::getInstance()->get_commentaire_dao()->shouldNotReceive("supprimer");

		$résultat_obtenu = $this->actingAs($this->jdoe)->json_api(
			"DELETE",
			"/commentaire/jdoe/cHJvZzEvbGVzX2ZvbmN0aW9uc18wMS9hcHBlbGVyX3VuZV9mb25jdGlvbl9wYXJhbcOpdHLDqWU/1614374490/1",
			[],
		);

		$this->assertEquals(403, $résultat_obtenu->status());
	}

	public function test_étant_donné_un_commentaire_laissé_par_autrui_sur_la_tentative_dautrui_lorsquon_le_supprime_on_obtient_une_erreur_403()
	{
		DAOFactory::getInstance()->get_commentaire_dao()->shouldNotReceive("supprimer");

		$résultat_obtenu = $this->actingAs($this->jdoe)->json_api(
			"DELETE",
			"/commentaire/bob/cHJvZzEvbGVzX2ZvbmN0aW9uc18wMS9hcHBlbGVyX3VuZV9mb25jdGlvbl9wYXJhbcOpdHLDqWU/1614374490/2",
			[],
		);

		$this->assertEquals(403, $résultat_obtenu->status());
	}
}
