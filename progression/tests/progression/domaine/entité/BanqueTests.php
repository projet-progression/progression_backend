<?php
/*
	This file is part of Progression.

	Progression is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Progression is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Progression.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace progression\domaine\entité;

use progression\TestCase;
use progression\domaine\entité\question\QuestionProg;

final class BanqueTests extends TestCase
{
	public function test_étant_donné_une_Banque_nouvellement_instanciée_lorsquon_récupère_ses_attributs_on_obtient_ses_attributs_sans_questions()
	{
		$nom_attendu = "Test 1";

		$banque = new Banque(nom: "Test 1");

		$this->assertEquals($nom_attendu, $banque->nom);
		$this->assertEquals([], $banque->questions);
	}

	public function test_étant_donné_une_Banque_nouvellement_instanciée_lorsquon_ajoute_une_nouvelle_question_elle_est_ajouté_à_lattribut_questions()
	{
		$banque = new Banque(nom: "Test 1");

		$question = new QuestionProg(titre: "une nouvelle question");
		$banque->ajouterQuestion(
			"https://progression.pages.dti.crosemont.quebec/contenu/prog_1/liste_questions.html",
			$question,
		);

		$this->assertEquals(
			["https://progression.pages.dti.crosemont.quebec/contenu/prog_1/liste_questions.html" => $question],
			$banque->questions,
		);
	}

	public function test_étant_donné_une_Banque_instanciée_avec_une_question_lorsquon_ajoute_une_nouvelle_question_elle_est_ajouté_à_lattribut_questions()
	{
		$première_question = new QuestionProg(titre: "une première question");

		$banque = new Banque(
			nom: "Test 1",
			questions: [
				"https://progression.pages.dti.crosemont.quebec/contenu/prog_1/liste_questions.html" => $première_question,
			],
		);

		$deuxième_question = new QuestionProg(titre: "une nouvelle question");
		$banque->ajouterQuestion(
			"https://progression.pages.dti.crosemont.quebec/contenu/prog_1/liste_questions.html",
			$deuxième_question,
		);

		$this->assertEquals(
			[
				"https://progression.pages.dti.crosemont.quebec/contenu/prog_1/liste_questions.html" => $première_question,
				"https://progression.pages.dti.crosemont.quebec/contenu/prog_1/liste_questions.html" => $deuxième_question,
			],
			$banque->questions,
		);
	}
}
