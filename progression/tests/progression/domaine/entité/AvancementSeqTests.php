<?php
/*
   This file is part of Progression.

   Progression is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Progression is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Progression.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace progression\domaine\entité;

use progression\TestCase;
use progression\domaine\entité\question\État;

final class AvancementSeqTests extends TestCase
{
	public function test_étant_donné_un_avancement_séquence_sans_question_commencée_lorsquon_le_récupère_on_obtient_les_avancements_et_un_état_non_débuté()
	{
		$avancement_1 = new Avancement(titre: "Titre 1", niveau: "niveau 1");
		$avancement_2 = new Avancement(titre: "Titre 2", niveau: "niveau 2");

		$résultat_observé = new AvancementSeq(
			avancements: [
				"1" => $avancement_1,
				"2" => $avancement_2,
			],
		);

		$this->assertEquals(["1" => $avancement_1, "2" => $avancement_2], $résultat_observé->avancements);
		$this->assertEquals(État::DEBUT, $résultat_observé->état);
	}

	public function test_étant_donné_un_avancement_séquence_avec_une_question_non_débutée_lorsquon_le_récupère_on_obtient_les_avancements_et_un_état_non_débuté()
	{
		$avancement_1 = new Avancement(titre: "Titre 1", niveau: "niveau 1");
		$avancement_1->état = État::DEBUT;

		$avancement_2 = new Avancement(titre: "Titre 2", niveau: "niveau 2");
		$avancement_2->état = État::NONREUSSI;

		$résultat_observé = new AvancementSeq(
			avancements: [
				"1" => $avancement_1,
				"2" => $avancement_2,
			],
		);

		$this->assertEquals(["1" => $avancement_1, "2" => $avancement_2], $résultat_observé->avancements);
		$this->assertEquals(État::DEBUT, $résultat_observé->état);
	}

	public function test_étant_donné_un_avancement_séquence_avec_une_question_réussie_et_une_non_réussie_lorsquon_le_récupère_on_obtient_les_avancements_et_un_état_non_réussi()
	{
		$avancement_1 = new Avancement(titre: "Titre 1", niveau: "niveau 1");
		$avancement_1->état = État::REUSSI;

		$avancement_2 = new Avancement(titre: "Titre 2", niveau: "niveau 2");
		$avancement_2->état = État::NONREUSSI;

		$résultat_observé = new AvancementSeq(
			avancements: [
				"1" => $avancement_1,
				"2" => $avancement_2,
			],
		);

		$this->assertEquals(["1" => $avancement_1, "2" => $avancement_2], $résultat_observé->avancements);
		$this->assertEquals(État::NONREUSSI, $résultat_observé->état);
	}

	public function test_étant_donné_un_avancement_séquence_avec_toute_les_questions_réussies_lorsquon_le_récupère_on_obtient_les_avancements_et_un_état_réussi()
	{
		$avancement_1 = new Avancement(titre: "Titre 1", niveau: "niveau 1");
		$avancement_1->état = État::REUSSI;

		$avancement_2 = new Avancement(titre: "Titre 2", niveau: "niveau 2");
		$avancement_2->état = État::REUSSI;

		$résultat_observé = new AvancementSeq(
			avancements: [
				"1" => $avancement_1,
				"2" => $avancement_2,
			],
		);

		$this->assertEquals(["1" => $avancement_1, "2" => $avancement_2], $résultat_observé->avancements);
		$this->assertEquals(État::REUSSI, $résultat_observé->état);
	}

	public function test_étant_donné_un_avancement_séquence_avec_toute_les_questions_réussies_lorsquon_ajoute_un_avancement_non_réussi_on_obtient_les_avancements_et_un_état_nonréussi()
	{
		$avancement_1 = new Avancement(titre: "Titre 1", niveau: "niveau 1");
		$avancement_1->état = État::REUSSI;

		$avancement_2 = new Avancement(titre: "Titre 2", niveau: "niveau 2");
		$avancement_2->état = État::REUSSI;

		$avancement_ajouté = new Avancement(titre: "Titre 3", niveau: "niveau 3");
		$avancement_ajouté->état = État::NONREUSSI;

		$résultat_observé = new AvancementSeq(
			avancements: [
				"1" => $avancement_1,
				"2" => $avancement_2,
			],
		);
		$résultat_observé->ajouter_avancement("ajout", $avancement_ajouté);

		$this->assertEquals(
			["1" => $avancement_1, "2" => $avancement_2, "ajout" => $avancement_ajouté],
			$résultat_observé->avancements,
		);
		$this->assertEquals(État::NONREUSSI, $résultat_observé->état);
	}

	public function test_étant_donné_un_avancement_séquence_avec_toute_les_questions_réussies_lorsquon_ajoute_un_avancement_non_débuté_on_obtient_les_avancements_et_un_état_non_débuté()
	{
		$avancement_1 = new Avancement(titre: "Titre 1", niveau: "niveau 1");
		$avancement_1->état = État::REUSSI;

		$avancement_2 = new Avancement(titre: "Titre 2", niveau: "niveau 2");
		$avancement_2->état = État::REUSSI;

		$avancement_ajouté = new Avancement(titre: "Titre 3", niveau: "niveau 3");
		$avancement_ajouté->état = État::DEBUT;

		$résultat_observé = new AvancementSeq(
			avancements: [
				"1" => $avancement_1,
				"2" => $avancement_2,
			],
		);
		$résultat_observé->ajouter_avancement("ajout", $avancement_ajouté);

		$this->assertEquals(
			["1" => $avancement_1, "2" => $avancement_2, "ajout" => $avancement_ajouté],
			$résultat_observé->avancements,
		);
		$this->assertEquals(État::DEBUT, $résultat_observé->état);
	}

	public function test_étant_donné_un_avancement_séquence_avec_toute_les_questions_réussies_lorsquon_ajoute_un_avancement_réussi_on_obtient_les_avancements_et_un_état_réussi()
	{
		$avancement_1 = new Avancement(titre: "Titre 1", niveau: "niveau 1");
		$avancement_1->état = État::REUSSI;

		$avancement_2 = new Avancement(titre: "Titre 2", niveau: "niveau 2");
		$avancement_2->état = État::REUSSI;

		$avancement_ajouté = new Avancement(titre: "Titre 3", niveau: "niveau 3");
		$avancement_ajouté->état = État::REUSSI;

		$résultat_observé = new AvancementSeq(
			avancements: [
				"1" => $avancement_1,
				"2" => $avancement_2,
			],
		);
		$résultat_observé->ajouter_avancement("ajout", $avancement_ajouté);

		$this->assertEquals(
			["1" => $avancement_1, "2" => $avancement_2, "ajout" => $avancement_ajouté],
			$résultat_observé->avancements,
		);
		$this->assertEquals(État::REUSSI, $résultat_observé->état);
	}
}
