<?php
/*
   This file is part of Progression.

   Progression is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Progression is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Progression.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace progression\domaine\interacteur;

use progression\domaine\entité\Commentaire;
use progression\domaine\entité\user\User;
use progression\dao\DAOFactory;
use progression\TestCase;
use Mockery;

final class SupprimerCommentaireIntTests extends TestCase
{
	public function setUp(): void
	{
		parent::setUp();

		$mockCommentaireDAO = Mockery::mock("progression\\dao\\CommentaireDAO");
		$mockCommentaireDAO
			->shouldReceive("get_commentaire")
			->with("bob", "prog1/les_fonctions_01/appeler_une_fonction_paramétrée", 1615696276, 1)
			->andReturn(
				new Commentaire("le 3er message", new User(username: "oteur", date_inscription: 0), 1615696276, 14),
			);

		$mockDAOFactory = Mockery::mock("progression\\dao\\DAOFactory");
		$mockDAOFactory->allows()->get_commentaire_dao()->andReturn($mockCommentaireDAO);

		DAOFactory::setInstance($mockDAOFactory);
	}

	public function test_étant_donné_un_commentaire_a_supprimer_lorsquon_le_supprime_il_est_supprimé_de_la_BD()
	{
		$intéracteur = new SupprimerCommentaireInt();
		DAOFactory::getInstance()
			->get_commentaire_dao()
			->shouldReceive("supprimer")
			->withArgs(function ($user, $question_uri, $date_soumission, $numéro) {
				return $numéro == 1;
			});

		$this->assertNull(
			$intéracteur->supprimer_commentaire(
				"bob",
				"prog1/les_fonctions_01/appeler_une_fonction_paramétrée",
				1615696276,
				1,
			),
		);
	}
}
