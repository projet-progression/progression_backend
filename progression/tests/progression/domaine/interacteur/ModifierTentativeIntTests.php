<?php
/*
   This file is part of Progression.

   Progression is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Progression is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Progression.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace progression\domaine\interacteur;

use progression\TestCase;
use progression\domaine\entité\TentativeProg;

final class ModifierTentativeIntTests extends TestCase
{
	public TentativeProg $tentative_réussie;
	public TentativeProg $tentative_non_réussie;

	public function setUp(): void
	{
		parent::setUp();

		$this->tentative_réussie = new TentativeProg(
			langage: "réussi",
			code: "codeTest",
			date_soumission: "1614374490",
			réussi: true,
			résultats: [],
			tests_réussis: 1,
			feedback: "feedbackTest",
			temps_exécution: 5,
		);

		$this->tentative_non_réussie = new TentativeProg(
			langage: "non_réussi",
			code: "codeTest",
			date_soumission: "1614374490",
			réussi: false,
			résultats: [],
			tests_réussis: 0,
			feedback: "feedbackTest",
			temps_exécution: 5,
		);
	}

	public function test_étant_donné_une_tentative_prog_réussie_lorsquon_la_rend_publique_on_obtient_une_tentative_publique()
	{
		$this->assertTrue((new ModifierTentativeInt())->modifier_publique($this->tentative_réussie, true)->publique);
	}

	public function test_étant_donné_une_tentative_prog_non_réussie_lorsquon_tente_de_la_modifier_pour_la_rendre_publique_on_obtient_une_PermissionException()
	{
		$this->expectException(PermissionException::class);
		$this->expectExceptionMessage("Transition de visibilité interdite.");

		(new ModifierTentativeInt())->modifier_publique($this->tentative_non_réussie, true);
	}
}
