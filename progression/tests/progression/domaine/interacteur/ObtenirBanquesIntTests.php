<?php
/*
	This file is part of Progression.

	Progression is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Progression is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Progression.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace progression\domaine\interacteur;

use progression\TestCase;
use progression\dao\DAOFactory;
use progression\domaine\entité\Banque;
use progression\domaine\entité\question\{QuestionProg, QuestionSys};
use Mockery;

final class ObtenirBanquesIntTests extends TestCase
{
	public Banque $banque_1_sans_questions;
	public Banque $banque_1_avec_questions;
	public Banque $banque_2_sans_questions;
	public Banque $banque_2_avec_questions;

	public function setUp(): void
	{
		parent::setUp();

		$première_question = new QuestionProg(titre: "une première question");
		$deuxième_question = new QuestionSys(titre: "une deuxième question");
		$troisième_question = new QuestionProg(titre: "une troisième question");
		$quatrième_question = new QuestionSys(titre: "une quatrième question");

		$this->banque_1_sans_questions = new Banque(nom: "Banque 1");
		$this->banque_2_sans_questions = new Banque(nom: "Banque 2");
		$this->banque_1_avec_questions = new Banque(
			nom: "Banque 1",
			questions: [$première_question, $deuxième_question],
		);
		$this->banque_2_avec_questions = new Banque(
			nom: "Banque 1",
			questions: [$troisième_question, $quatrième_question],
		);

		$mockBanqueDAO = Mockery::mock("progression\\dao\\banque\\BanqueDAO");
		$mockBanqueDAO
			->shouldReceive("get_toutes")
			->with("bob", [])
			->andReturn([
				"Banque 1" => $this->banque_1_sans_questions,
				"Banque 2" => $this->banque_2_sans_questions,
			]);
		$mockBanqueDAO
			->shouldReceive("get_toutes")
			->with("bob", ["questions"])
			->andReturn([
				"Banque 1" => $this->banque_1_avec_questions,
				"Banque 2" => $this->banque_2_avec_questions,
			]);

		$mockDAOFactory = Mockery::mock("progression\\dao\\DAOFactory");
		$mockDAOFactory->allows()->get_banque_dao()->andReturn($mockBanqueDAO);
		DAOFactory::setInstance($mockDAOFactory);
	}

	public function test_étant_donnée_un_utilisateur_possédant_deux_banques_lorsquon_les_récupère_on_obtient_les_deux_banques()
	{
		$this->assertEquals(
			["Banque 1" => $this->banque_1_sans_questions, "Banque 2" => $this->banque_2_sans_questions],
			(new ObtenirBanquesInt())->get_banques("bob"),
		);
	}

	public function test_étant_donnée_un_utilisateur_possédant_deux_banques_lorsquon_les_récupère_en_incluant_les_questions_on_obtient_les_deux_banques_et_leurs_questions()
	{
		$this->assertEquals(
			["Banque 1" => $this->banque_1_avec_questions, "Banque 2" => $this->banque_2_avec_questions],
			(new ObtenirBanquesInt())->get_banques("bob", ["questions"]),
		);
	}
}
