<?php
/*
   This file is part of Progression.

   Progression is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Progression is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Progression.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace progression\domaine\interacteur;

use Illuminate\Support\Facades\Config;
use progression\dao\DAOFactory;
use progression\domaine\entité\user\{User, État, Rôle, Occupation};
use progression\TestCase;
use progression\UserAuthentifiable;
use Mockery;

final class ModifierUserIntTests extends TestCase
{
	public $user;

	public function setUp(): void
	{
		parent::setUp();

		$this->user = new UserAuthentifiable(
			username: "jdoe",
			date_inscription: 0,
			rôle: Rôle::NORMAL,
			état: État::ACTIF,
		);

		$this->admin = new UserAuthentifiable(
			username: "admin",
			date_inscription: 0,
			rôle: Rôle::ADMIN,
			état: État::ACTIF,
		);

		$mockDAOFactory = Mockery::mock("progression\\dao\\DAOFactory");
		DAOFactory::setInstance($mockDAOFactory);

		$mockUserDAO = Mockery::mock("progression\\dao\\UserDAO");
		$mockDAOFactory->allows()->get_user_dao()->andReturn($mockUserDAO);

		$mockExpéditeurDao = Mockery::mock("progression\\dao\\mail\\Expéditeur");
		$mockDAOFactory->shouldReceive("get_expéditeur")->andReturn($mockExpéditeurDao);
	}

	#Préférences
	public function test_étant_donné_un_utilisateur_sans_préférences_lorsquon_lui_ajoute_des_préférences_on_obtient_le_même_utilisateur_avec_des_préférences()
	{
		$résultat_attendu = new User(username: "bob", date_inscription: 0, préférences: "mes préférences");
		$mockUserDAO = DAOFactory::getInstance()->get_user_dao();
		$mockUserDAO
			->shouldReceive("save")
			->withArgs(function ($username, $user) use ($résultat_attendu) {
				return $username == "bob" && $user == $résultat_attendu;
			})
			->andReturn(["bob" => $résultat_attendu]);

		$this->actingAs($this->user);

		$user_test = new User(username: "bob", date_inscription: 0);

		$interacteur = new ModifierUserInt();
		$résultat_obtenu = $interacteur->modifier_user($user_test, ["préférences" => "mes préférences"]);

		$this->assertEquals(new User(username: "bob", date_inscription: 0, préférences: "mes préférences"), $user_test);
		$this->assertEquals($résultat_attendu, $résultat_obtenu);
	}

	public function test_étant_donné_un_utilisateur_lorsquon_modifie_tous_ses_attributs_on_obtient_un_utilisateur_dont_seuls_les_attributs_modifiables_ont_été_modifiés()
	{
		$user_test = new User(
			courriel: "bob@testmail.com",
			username: "bob",
			état: État::ACTIF,
			rôle: Rôle::NORMAL,
			date_inscription: 1590828610,
			préférences: "les rouges",
			prénom: "Bob",
			nom: "Test",
			nom_complet: "Bob Test",
			pseudonyme: "Bob le Flambeur",
			biographie: "voici ma vie",
			occupation: Occupation::AUTRE,
			avatar: "https://avatar.com/test.png",
		);

		$résultat_attendu = new User(
			username: "bob",
			courriel: "bob@testmail.com",
			état: État::ACTIF,
			rôle: Rôle::NORMAL,
			date_inscription: 1590828610,
			préférences: "les bleus",
			prénom: "Léo",
			nom: "Lébas",
			nom_complet: "Léo Lébas",
			pseudonyme: "Léobas",
			biographie: "voici ma vraie vie",
			occupation: Occupation::ÉTUDIANT,
			avatar: "https://avatar.com/autre.png",
		);

		$mockUserDAO = DAOFactory::getInstance()->get_user_dao();
		$mockUserDAO
			->shouldReceive("save")
			->withArgs(function ($username, $user) use ($résultat_attendu) {
				return $username == "bob" && $user == $résultat_attendu;
			})
			->andReturn(["bob" => $résultat_attendu]);
		$mockUserDAO->shouldReceive("trouver")->with(null, null, "Léobas")->andReturn(null);
		$mockUserDAO->shouldReceive("trouver")->with("Léobas")->andReturn(null);

		$interacteur = new ModifierUserInt();

		$this->actingAs($this->user);
		$résultat_obtenu = $interacteur->modifier_user($user_test, [
			"username" => "bob",
			"courriel" => "bob@testmail.com",
			"username" => "leo",
			"état" => "actif",
			"rôle" => "normal",
			"date inscription" => 1590828612,
			"préférences" => "les bleus",
			"prénom" => "Léo",
			"nom" => "Lébas",
			"nom_complet" => "Léo Lébas",
			"pseudonyme" => "Léobas",
			"biographie" => "voici ma vraie vie",
			"occupation" => "étudiant",
			"avatar" => "https://avatar.com/autre.png",
			"attribut_inexistant" => "inexistant",
		]);

		$this->assertEquals($résultat_attendu, $résultat_obtenu);
	}

	public function test_étant_donné_un_utilisateur_existant_lorsquon_modifie_son_pseudonyme_pour_un_pseudonyme_dun_autre_utilisateur_existant_on_obtient_une_exception()
	{
		$mockUserDAO = DAOFactory::getInstance()->get_user_dao();
		$mockUserDAO
			->shouldReceive("trouver")
			->with(null, null, "bob01")
			->andReturn(
				new User(username: "Autre", pseudonyme: "bob01", date_inscription: 0, courriel: "autre@test.com"),
			);
		$mockUserDAO->shouldReceive("trouver")->with("bob01")->andReturn(null);
		$mockUserDAO->shouldNotReceive("save");

		DAOFactory::getInstance()->get_expéditeur()->shouldNotReceive("envoyer_courriel_de_validation");

		$this->actingAs($this->user);

		$this->expectException(DuplicatException::class);
		$this->expectExceptionMessage("Le pseudonyme est déjà utilisé.");

		$user_test = new User(username: "bob", date_inscription: 0, courriel: "bob@test.com");

		$interacteur = new ModifierUserInt();
		$interacteur->modifier_user($user_test, ["pseudonyme" => "bob01"]);
	}

	public function test_étant_donné_un_utilisateur_existant_lorsquon_modifie_son_pseudonyme_pour_un_username_dun_autre_utilisateur_existant_on_obtient_une_exception()
	{
		$mockUserDAO = DAOFactory::getInstance()->get_user_dao();
		$mockUserDAO->shouldReceive("trouver")->with(null, null, "bob01")->andReturn(null);
		$mockUserDAO
			->shouldReceive("trouver")
			->with("bob01")
			->andReturn(
				new User(username: "bob01", pseudonyme: "bobby", date_inscription: 0, courriel: "autre@test.com"),
			);
		$mockUserDAO->shouldNotReceive("save");

		DAOFactory::getInstance()->get_expéditeur()->shouldNotReceive("envoyer_courriel_de_validation");

		$this->actingAs($this->user);

		$this->expectException(DuplicatException::class);
		$this->expectExceptionMessage("Le pseudonyme est déjà utilisé.");

		$user_test = new User(username: "bob", date_inscription: 0, courriel: "bob@test.com");

		$interacteur = new ModifierUserInt();
		$interacteur->modifier_user($user_test, ["pseudonyme" => "bob01"]);
	}

	public function test_étant_donné_un_utilisateur_existant_lorsquon_modifie_son_pseudonyme_pour_le_même_pseudonyme_on_obtient_lutilisateur_inchangé()
	{
		$mockUserDAO = DAOFactory::getInstance()->get_user_dao();
		$mockUserDAO
			->shouldReceive("trouver")
			->with(null, null, "bob")
			->andReturn(new User(username: "bob", date_inscription: 0, courriel: "bob@test.com"));
		$mockUserDAO
			->shouldReceive("trouver")
			->with("bob")
			->andReturn(new User(username: "bob", date_inscription: 0, courriel: "bob@test.com"));

		$mockUserDAO->shouldNotReceive("save");

		DAOFactory::getInstance()->get_expéditeur()->shouldNotReceive("envoyer_courriel_de_validation");

		$this->actingAs($this->user);

		$user_test = new User(username: "bob", date_inscription: 0, courriel: "bob@test.com");

		$interacteur = new ModifierUserInt();
		$résultat_obtenu = $interacteur->modifier_user($user_test, ["pseudonyme" => "bob"]);

		$this->assertEquals($user_test, $résultat_obtenu);
	}

	#Courriel
	public function test_étant_donné_un_utilisateur_avec_courriel_lorsquon_modifie_son_courriel_on_obtient_le_même_utilisateur_avec_courriel_modifié_et_état_en_attente()
	{
		$résultat_attendu = new User(
			username: "bob",
			courriel: "autre@test.com",
			date_inscription: 0,
			état: État::EN_ATTENTE_DE_VALIDATION,
		);

		$mockUserDAO = DAOFactory::getInstance()->get_user_dao();
		$mockUserDAO->shouldReceive("trouver")->with(null, "autre@test.com")->andReturn(null);
		$mockUserDAO
			->shouldReceive("save")
			->withArgs(function ($username, $user) use ($résultat_attendu) {
				return $username == "bob" && $user == $résultat_attendu;
			})
			->andReturn(["bob" => $résultat_attendu]);

		$user_test = new User(username: "bob", date_inscription: 0, courriel: "bob@test.com");

		DAOFactory::getInstance()
			->get_expéditeur()
			->shouldReceive("envoyer_courriel_de_validation")
			->with($user_test)
			->once();

		$this->actingAs($this->user);

		$interacteur = new ModifierUserInt();
		$résultat_obtenu = $interacteur->modifier_user($user_test, ["courriel" => "autre@test.com"]);

		$this->assertEquals($résultat_attendu, $résultat_obtenu);
	}

	public function test_étant_donné_un_utilisateur_inactif_sans_validation_de_courriel_lorsquon_modifie_son_courriel_on_obtient_le_même_utilisateur_avec_courriel_modifié_et_état_en_attente()
	{
		$résultat_attendu = new User(
			username: "bob",
			date_inscription: 0,
			courriel: "autre@test.com",
			état: État::INACTIF,
		);

		Config::set("mail.mailer", "no");
		$mockUserDAO = DAOFactory::getInstance()->get_user_dao();
		$mockUserDAO->shouldReceive("trouver")->with(null, "autre@test.com")->andReturn(null);
		$mockUserDAO
			->shouldReceive("save")
			->withArgs(function ($username, $user) use ($résultat_attendu) {
				return $username == "bob" && $user == $résultat_attendu;
			})
			->andReturn(["bob" => $résultat_attendu]);

		$user_test = new User(username: "bob", date_inscription: 0, courriel: "bob@test.com");

		DAOFactory::getInstance()->get_expéditeur()->shouldNotReceive("envoyer_courriel_de_validation");

		$this->actingAs($this->user);

		$interacteur = new ModifierUserInt();
		$résultat_obtenu = $interacteur->modifier_user($user_test, ["courriel" => "autre@test.com"]);

		$this->assertEquals($résultat_attendu, $résultat_obtenu);
	}

	public function test_étant_donné_un_utilisateur_avec_courriel_lorsquon_modifie_son_courriel_avec_un_courriel_existant_on_obtient_une_exception()
	{
		$mockUserDAO = DAOFactory::getInstance()->get_user_dao();
		$mockUserDAO
			->shouldReceive("trouver")
			->with(null, "autre@test.com")
			->andReturn(new User(username: "Autre", date_inscription: 0, courriel: "autre@test.com"));
		$mockUserDAO->shouldNotReceive("save");

		DAOFactory::getInstance()->get_expéditeur()->shouldNotReceive("envoyer_courriel_de_validation");

		$this->actingAs($this->user);

		$this->expectException(DuplicatException::class);
		$this->expectExceptionMessage("Le courriel est déjà utilisé.");

		$user_test = new User(username: "bob", date_inscription: 0, courriel: "bob@test.com");

		$interacteur = new ModifierUserInt();
		$interacteur->modifier_user($user_test, ["courriel" => "autre@test.com"]);
	}

	#État
	public function test_étant_donné_un_utilisateur_inactif_lorsquon_modifie_son_état_on_obtient_une_exception()
	{
		$this->actingAs($this->user);

		$user_test = new User(username: "bob", date_inscription: 0, état: État::INACTIF);

		$interacteur = new ModifierUserInt();

		$this->expectException(PermissionException::class);
		$this->expectExceptionMessage("Transition d'état interdite");

		$interacteur->modifier_user($user_test, ["état" => "actif"]);
	}

	public function test_étant_donné_un_utilisateur_en_attente_lorsquon_modifie_son_état_pour_actif_on_obtient_un_utilisateur_actif()
	{
		$résultat_attendu = new User(username: "bob", date_inscription: 0, état: État::ACTIF);

		$mockUserDAO = DAOFactory::getInstance()->get_user_dao();
		$mockUserDAO
			->shouldReceive("save")
			->withArgs(function ($username, $user) use ($résultat_attendu) {
				return $username == "bob" && $user == $résultat_attendu;
			})
			->andReturn(["bob" => $résultat_attendu]);

		$this->actingAs($this->user);

		$user_test = new User(username: "bob", date_inscription: 0, état: État::EN_ATTENTE_DE_VALIDATION);

		$interacteur = new ModifierUserInt();
		$résultat_obtenu = $interacteur->modifier_user($user_test, ["état" => "actif"]);

		$this->assertEquals($résultat_attendu, $résultat_obtenu);
	}

	public function test_étant_donné_un_utilisateur_en_attente_lorsquon_modifie_son_état_pour_inactif_on_obtient_une_exception()
	{
		$this->actingAs($this->user);

		$user_test = new User(username: "bob", date_inscription: 0, état: État::EN_ATTENTE_DE_VALIDATION);

		$interacteur = new ModifierUserInt();
		$this->expectException(PermissionException::class);
		$this->expectExceptionMessage("Transition d'état interdite");

		$interacteur->modifier_user($user_test, ["état" => "inactif"]);
	}

	public function test_étant_donné_un_utilisateur_inactif_lorsqu_un_admin_modifie_son_état_pour_actif_on_obtient_un_utilisateur_actif()
	{
		$résultat_attendu = new User(username: "bob", date_inscription: 0, état: État::ACTIF);

		$mockUserDAO = DAOFactory::getInstance()->get_user_dao();
		$mockUserDAO
			->shouldReceive("save")
			->withArgs(function ($username, $user) use ($résultat_attendu) {
				return $username == "bob" && $user == $résultat_attendu;
			})
			->andReturn(["bob" => $résultat_attendu]);

		$this->actingAs($this->admin);

		$user_test = new User(username: "bob", date_inscription: 0, état: État::INACTIF);

		$interacteur = new ModifierUserInt();
		$résultat_obtenu = $interacteur->modifier_user($user_test, ["état" => "actif"]);

		$this->assertEquals($résultat_attendu, $résultat_obtenu);
	}

	public function test_étant_donné_un_utilisateur_en_attente_lorsqu_un_admin_modifie_son_état_pour_inactif_on_obtient_un_utilisateur_inactif()
	{
		$résultat_attendu = new User(username: "bob", date_inscription: 0, état: État::INACTIF);

		$mockUserDAO = DAOFactory::getInstance()->get_user_dao();
		$mockUserDAO
			->shouldReceive("save")
			->withArgs(function ($username, $user) use ($résultat_attendu) {
				return $username == "bob" && $user == $résultat_attendu;
			})
			->andReturn(["bob" => $résultat_attendu]);

		$this->actingAs($this->admin);

		$user_test = new User(username: "bob", date_inscription: 0, état: État::EN_ATTENTE_DE_VALIDATION);

		$interacteur = new ModifierUserInt();
		$résultat_obtenu = $interacteur->modifier_user($user_test, ["état" => "inactif"]);

		$this->assertEquals($résultat_attendu, $résultat_obtenu);
	}

	public function test_étant_donné_un_utilisateur_inactif_lorsquon_modifie_son_état_pour_en_attente_on_obtient_un_utilisateur_en_attente()
	{
		$résultat_attendu = new User(username: "bob", date_inscription: 0, état: État::EN_ATTENTE_DE_VALIDATION);

		$mockUserDAO = DAOFactory::getInstance()->get_user_dao();
		$mockUserDAO
			->shouldReceive("save")
			->withArgs(function ($username, $user) use ($résultat_attendu) {
				return $username == "bob" && $user == $résultat_attendu;
			})
			->andReturn(["bob" => $résultat_attendu]);

		$this->actingAs($this->admin);

		$user_test = new User(username: "bob", date_inscription: 0, état: État::INACTIF);

		$interacteur = new ModifierUserInt();

		$résultat_obtenu = $interacteur->modifier_user($user_test, ["état" => "en_attente_de_validation"]);

		$this->assertEquals($résultat_attendu, $résultat_obtenu);
	}

	#Rôle
	public function test_étant_donné_un_utilisateur_normal_lorsquon_modifie_son_rôle_pour_admin_on_obtient_une_erreur_de_permission()
	{
		$this->actingAs($this->user);

		$user_test = new User(username: "bob", date_inscription: 0, état: État::ACTIF);

		$interacteur = new ModifierUserInt();

		$this->expectException(PermissionException::class);
		$this->expectExceptionMessage("Transition de rôle interdite");

		$interacteur->modifier_user($user_test, ["rôle" => "admin"]);
	}

	public function test_étant_donné_un_utilisateur_normal_lorsqu_un_admin_modifie_son_rôle_pour_admin_on_obtient_un_utilisateur_admin()
	{
		$résultat_attendu = new User(username: "bob", date_inscription: 0, état: État::ACTIF, rôle: Rôle::ADMIN);

		$mockUserDAO = DAOFactory::getInstance()->get_user_dao();
		$mockUserDAO
			->shouldReceive("save")
			->withArgs(function ($username, $user) use ($résultat_attendu) {
				return $username == "bob" && $user == $résultat_attendu;
			})
			->andReturn(["bob" => $résultat_attendu]);

		$this->actingAs($this->admin);

		$user_test = new User(username: "bob", date_inscription: 0, état: État::ACTIF);

		$interacteur = new ModifierUserInt();

		$résultat_obtenu = $interacteur->modifier_user($user_test, ["rôle" => "admin"]);

		$this->assertEquals($résultat_attendu, $résultat_obtenu);
	}

	#Mot de passe
	public function test_étant_donné_un_utilisateur_actif_lorsquon_modifie_son_mot_de_passe_on_obtient_un_utilisateur_avec_un_nouveau_mot_de_passe()
	{
		$résultat_attendu = new User(username: "bob", date_inscription: 0, état: État::ACTIF, rôle: Rôle::NORMAL);

		$user_test = new User(username: "bob", date_inscription: 0, état: État::ACTIF);

		$mockUserDAO = DAOFactory::getInstance()->get_user_dao();
		$mockUserDAO->shouldReceive("set_password")->with($user_test, "qwerty123!")->once();

		$this->actingAs($this->user);

		$interacteur = new ModifierUserInt();

		$résultat_obtenu = $interacteur->modifier_user($user_test, ["password" => "qwerty123!"]);

		$this->assertEquals($résultat_attendu, $résultat_obtenu);
	}
}
