<?php
/*
   This file is part of Progression.

   Progression is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Progression is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Progression.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace progression\domaine\interacteur;

use Illuminate\Support\Facades\Mail;
use progression\domaine\entité\Commentaire;
use progression\domaine\entité\user\User;
use progression\dao\DAOFactory;
use progression\dao\mail\NotificationCommentaireMail;
use progression\TestCase;
use Mockery;

final class CréerCommentaireIntTests extends TestCase
{
	public function setUp(): void
	{
		parent::setUp();

		$mockCommentaireDAO = Mockery::mock("progression\\dao\\CommentaireDAO");
		$mockUserDAO = Mockery::mock("progression\\dao\\UserDAO");

		$mockUserDAO
			->shouldReceive("get_user")
			->with("jdoe", [])
			->andReturn(new User(username: "jdoe", date_inscription: 0, courriel: "jdoe@test.com"));

		$mockUserDAO
			->shouldReceive("get_user")
			->with("anglo", [])
			->andReturn(
				new User(
					username: "anglo",
					date_inscription: 0,
					courriel: "anglo@test.com",
					préférences: "{\"locale\": \"en\"}",
				),
			);

		$mockUserDAO
			->shouldReceive("get_user")
			->with("roger", [])
			->andReturn(new User(username: "roger", date_inscription: 0));

		$mockDAOFactory = Mockery::mock("progression\\dao\\DAOFactory")->makePartial();
		$mockDAOFactory->allows()->get_user_dao()->andReturn($mockUserDAO);
		$mockDAOFactory->allows()->get_commentaire_dao()->andReturn($mockCommentaireDAO);

		DAOFactory::setInstance($mockDAOFactory);

		Mail::fake();
	}

	public function test_étant_donné_un_utilisateur_existant_lorsquil_crée_un_commentaire_sur_sa_propre_tentative_il_est_sauvegardé_mais_aucun_courriel_de_notification_nest_envoyé()
	{
		$commentaireAttendu = new Commentaire(
			"le 99iem message",
			new User(username: "jdoe", date_inscription: 0),
			1615696276,
			14,
		);
		DAOFactory::getInstance()
			->get_commentaire_dao()
			->shouldReceive("save")
			->withArgs(function ($user, $uri, $date, $numéro, $commentaire) {
				return $user == "jdoe" &&
					$uri == "prog1/les_fonctions_01/appeler_une_fonction_paramétrée" &&
					$date == 1615696276 &&
					$numéro == null &&
					$commentaire ==
						new Commentaire(
							"le 99iem message",
							new User(username: "jdoe", date_inscription: 0),
							1615696276,
							14,
						);
			})
			->andReturn([0 => $commentaireAttendu]);

		$commentaireInt = new CréerCommentaireInt();
		$this->assertEquals(
			[0 => $commentaireAttendu],
			$commentaireInt->créer_commentaire(
				"jdoe",
				"prog1/les_fonctions_01/appeler_une_fonction_paramétrée",
				1615696276,
				$commentaireAttendu,
			),
		);

		Mail::assertNotSent(NotificationCommentaireMail::class);
	}

	public function test_étant_donné__un_utilisateur_sans_courriel_lorsquon_lui_crée_un_commentaire_on_obtient_le_commentaire_et_le_courriel_de_notification_nest_pas_envoyé()
	{
		$commentaire_créé = new Commentaire("Bon travail", new User("jdoe", date_inscription: 0), 1653690241, 3);

		DAOFactory::getInstance()
			->get_commentaire_dao()
			->shouldReceive("save")
			->once()
			->andReturn([0 => $commentaire_créé]);

		$commentaireInt = new CréerCommentaireInt();
		$résultat_obtenue = $commentaireInt->créer_commentaire(
			"roger",
			"https://progression.pages.dti.crosemont.quebec/contenu/prog_1/2450ecdd-8b5a-40ed-9848-92dc1b4f3627/info.yml",
			1615696276,
			$commentaire_créé,
		);

		Mail::assertNotSent(NotificationCommentaireMail::class);
	}

	public function test_étant_donné_un_utilisateur_avec_un_courriel_lorsquon_lui_crée_un_commentaire_on_obtient_le_commentaire_et_un_courriel_de_notification_est_envoyé()
	{
		$commentaire_créé = new Commentaire("Bon travail", new User("bob", date_inscription: 0), 1653690241, 3);

		DAOFactory::getInstance()
			->get_commentaire_dao()
			->shouldReceive("save")
			->once()
			->andReturn([0 => $commentaire_créé]);

		$commentaireInt = new CréerCommentaireInt();
		$résultat_obtenue = $commentaireInt->créer_commentaire(
			"jdoe",
			"https://progression.pages.dti.crosemont.quebec/contenu/prog_1/2450ecdd-8b5a-40ed-9848-92dc1b4f3627/info.yml",
			1615696276,
			$commentaire_créé,
		);

		Mail::assertSent(NotificationCommentaireMail::class, function (NotificationCommentaireMail $mail) {
			return $mail->hasTo("jdoe@test.com") && $mail->subject == "Vous avez reçu un commentaire de bob";
		});
	}

	public function test_étant_donné_un_utilisateur_anglophone_avec_un_courriel_lorsquon_lui_crée_un_commentaire_on_obtient_le_commentaire_et_un_courriel_de_notification_en_anglais_est_envoyé()
	{
		$commentaire_créé = new Commentaire("Bon travail", new User("bob", date_inscription: 0), 1653690241, 3);

		DAOFactory::getInstance()
			->get_commentaire_dao()
			->shouldReceive("save")
			->once()
			->andReturn([0 => $commentaire_créé]);

		$commentaireInt = new CréerCommentaireInt();
		$résultat_obtenue = $commentaireInt->créer_commentaire(
			"anglo",
			"https://progression.pages.dti.crosemont.quebec/contenu/prog_1/2450ecdd-8b5a-40ed-9848-92dc1b4f3627/info.yml",
			1615696276,
			$commentaire_créé,
		);

		Mail::assertSent(NotificationCommentaireMail::class, function (NotificationCommentaireMail $mail) {
			return $mail->hasTo("anglo@test.com") && $mail->subject == "You have received a comment from bob";
		});
	}
}
