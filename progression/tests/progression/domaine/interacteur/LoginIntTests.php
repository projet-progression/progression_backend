<?php
/*
   This file is part of Progression.

   Progression is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Progression is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Progression.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace progression\domaine\interacteur;

use progression\dao\DAOFactory;
use progression\domaine\entité\clé\{Clé, Portée};
use progression\domaine\entité\user\{État, User};
use progression\facades\LDAP;
use progression\TestCase;
use Illuminate\Support\Facades\Config;
use Mockery;
use Carbon\Carbon;

final class LoginIntTests extends TestCase
{
	private $bob;

	public function setUp(): void
	{
		parent::setUp();

		$this->bob = new User(
			username: "bob",
			courriel: "bob@progressionmail.com",
			date_inscription: Carbon::now()->getTimestamp(),
		);

		$mockUserDAO = Mockery::mock("progression\\dao\\UserDAO");
		$mockUserDAO->allows()->get_user("bob", Mockery::Any())->andReturn($this->bob);
		$mockUserDAO->allows()->trouver(null, "bob@progressionmail.com")->andReturn($this->bob);
		$mockUserDAO->allows()->get_user("bob")->andReturn($this->bob);
		$mockUserDAO->shouldReceive("get_user")->andReturn(null);
		$mockUserDAO->allows()->vérifier_password(Mockery::Any(), "password")->andReturn(true);
		$mockUserDAO->allows()->vérifier_password(Mockery::Any(), Mockery::Any())->andReturn(false);

		$mockCléDAO = Mockery::mock("progression\\dao\\CléDAO");
		$mockCléDAO
			->shouldReceive("get_clé")
			->with("bob", "clé valide")
			->andReturn(new Clé("secret", Carbon::now()->getTimestamp(), 0, Portée::AUTH));
		$mockCléDAO->shouldReceive("vérifier")->with("bob", "clé valide", "secret")->andReturn(true);
		$mockCléDAO
			->shouldReceive("get_clé")
			->with("bob", "cle_expiree")
			->andReturn(
				new Clé("secret", Carbon::now()->getTimestamp() - 2, Carbon::now()->getTimestamp() - 1, Portée::AUTH),
			);
		$mockCléDAO
			->shouldReceive("get_clé")
			->with("bob", "clé révoquée")
			->andReturn(new Clé("secret", Carbon::now()->getTimestamp(), 0, Portée::RÉVOQUÉE));
		$mockCléDAO->shouldReceive("get_clé")->with("bob", "clé inexistante")->andReturn(null);
		$mockCléDAO->shouldReceive("vérifier")->andReturn(false);

		$mockDAOFactory = Mockery::mock("progression\\dao\\DAOFactory");
		$mockDAOFactory->allows()->get_user_dao()->andReturn($mockUserDAO);
		$mockDAOFactory->allows()->get_clé_dao()->andReturn($mockCléDAO);
		DAOFactory::setInstance($mockDAOFactory);
	}

	public function test_étant_donné_lutilisateur_vide_lorsquon_login_obtient_null()
	{
		$interacteur = new LoginInt();
		$résultat_obtenu = $interacteur->effectuer_login_par_identifiant("");

		$this->assertNull($résultat_obtenu);
	}

	public function test_étant_donné_lutilisateur_bob_existant_et_une_authentification_de_type_no_lorsquon_login_sans_mot_de_passe_on_obtient_un_objet_user_nommé_bob()
	{
		Config::set("authentification.local", false);
		Config::set("authentification.ldap", false);

		DAOFactory::getInstance()
			->get_user_dao()
			->shouldReceive("save")
			->withArgs(function ($username, $user) {
				return $username == "bob" && $user == $this->bob;
			})
			->andReturn([
				"bob" => $this->bob,
			]);

		$interacteur = new LoginInt();
		$résultat_obtenu = $interacteur->effectuer_login_par_identifiant("bob");

		$this->assertEquals($this->bob, $résultat_obtenu);
	}

	public function test_étant_donné_lutilisateur_bob_existant_et_une_authentification_de_type_ldap_lorsquon_login_sans_mot_de_passe_on_obtient_null()
	{
		Config::set("authentification.local", false);
		Config::set("authentification.ldap", true);

		$interacteur = new LoginInt();
		$résultat_obtenu = $interacteur->effectuer_login_par_identifiant("bob");

		$this->assertNull($résultat_obtenu);
	}

	public function test_étant_donné_lutilisateur_Banane_inexistant_et_une_authentification_de_type_no_lorsquon_login_sans_mot_de_passe_il_est_créé_et_on_obtient_un_objet_user_actif_nommé_Banane()
	{
		Config::set("authentification.local", false);
		Config::set("authentification.ldap", false);

		$mockUserDAO = DAOFactory::getInstance()->get_user_dao();

		$banane = new User(
			username: "Banane",
			état: État::ACTIF,
			date_inscription: Carbon::now()->getTimestamp(),
			préférences: "",
		);

		$mockUserDAO
			->shouldReceive("save")
			->withArgs(function ($username, $user) use ($banane) {
				return $username == "Banane" && $user == $banane;
			})
			->andReturn([
				"Banane" => $banane,
			]);
		$mockUserDAO->shouldReceive("set_password")->withArgs(function ($user, $password) {
			return $user->username == "Banane" && $password == "password";
		});

		$interacteur = new LoginInt();
		$résultat_obtenu = $interacteur->effectuer_login_par_identifiant("Banane");

		$this->assertEquals($banane, $résultat_obtenu);
	}

	public function test_étant_donné_lutilisateur_existant_bob_et_une_authentification_de_type_local_lorsquon_login_avec_username_et_mdp_correct_on_obtient_un_objet_user_nommé_bob()
	{
		Config::set("authentification.local", true);
		Config::set("authentification.ldap", false);

		$interacteur = new LoginInt();
		$résultat_obtenu = $interacteur->effectuer_login_par_identifiant("bob", "password");

		$this->assertEquals($this->bob, $résultat_obtenu);
	}

	public function test_étant_donné_lutilisateur_existant_bob_et_une_authentification_de_type_local_lorsquon_login_avec_courriel_et_mdp_correct_on_obtient_un_objet_user_nommé_bob()
	{
		Config::set("authentification.local", true);
		Config::set("authentification.ldap", false);

		$interacteur = new LoginInt();
		$résultat_obtenu = $interacteur->effectuer_login_par_identifiant("bob@progressionmail.com", "password");

		$this->assertEquals($this->bob, $résultat_obtenu);
	}

	public function test_étant_donné_lutilisateur_existant_bob_et_une_authentification_de_type_local_lorsquon_login_avec_mdp_incorrect_on_obtient_null()
	{
		Config::set("authentification.local", true);
		Config::set("authentification.ldap", false);

		$interacteur = new LoginInt();
		$résultat_obtenu = $interacteur->effectuer_login_par_identifiant("bob", "pas mon mot de passe");

		$this->assertNull($résultat_obtenu);
	}

	public function test_étant_donné_lutilisateur_Banane_inexistant_et_une_authentification_de_type_local_lorsquon_login_on_obtient_null()
	{
		Config::set("authentification.local", true);
		Config::set("authentification.ldap", false);

		$interacteur = new LoginInt();
		$résultat_obtenu = $interacteur->effectuer_login_par_identifiant("Banane", "password");

		$this->assertNull($résultat_obtenu);
	}

	//LDAP
	public function test_étant_donné_lutilisateur_Banane_inexistant_dans_lannuaire_et_une_authentification_de_type_ldap_lorsquon_login_sans_mot_de_passe_on_obtient_null()
	{
		Config::set("authentification.local", false);
		Config::set("authentification.ldap", true);

		$interacteur = new LoginInt();
		$résultat_obtenu = $interacteur->effectuer_login_par_identifiant("Banane");

		$this->assertNull($résultat_obtenu);
	}

	public function test_étant_donné_lutilisateur_bob_existant_et_deja_inscrit_et_une_authentification_de_type_ldap_lorsquon_login_avec_mot_de_passe_on_obtient_lutilisateur_bob()
	{
		Config::set("authentification.local", false);
		Config::set("authentification.ldap", true);
		Config::set("ldap.domaine", "test");

		LDAP::shouldReceive("get_user")
			->with("bob", "password", "test")
			->andReturn([
				"count" => 1,
				0 => [
					"count" => 2,
					0 => "cn",
					1 => "mail",
					"dn" => "CN=Bob Test,OU=Utilisateurs,DC=test",
					"cn" => [
						"count" => 1,
						0 => "Bob Test",
					],
					"mail" => [
						"count" => 1,
						0 => "bob@progressionmail.com",
					],
				],
			]);

		$interacteur = new LoginInt();
		$résultat_obtenu = $interacteur->effectuer_login_par_identifiant("bob", "password", "test");

		$this->assertEquals($this->bob, $résultat_obtenu);
	}

	public function test_étant_donné_lutilisateur_roger_existant_dans_lannuaire_sans_courriel_mais_non_inscrit_et_une_authentification_de_type_ldap_lorsquon_login_avec_mot_de_passe_on_obtient_lutilisateur_sans_courriel()
	{
		Config::set("authentification.local", false);
		Config::set("authentification.ldap", true);
		Config::set("ldap.domaine", "test");

		$roger = new User(
			username: "roger",
			courriel: null,
			état: État::ACTIF,
			date_inscription: Carbon::now()->getTimestamp(),
		);

		LDAP::shouldReceive("get_user")
			->with("roger", "password", "test")
			->andReturn([
				"count" => 1,
				0 => [
					"count" => 2,
					0 => "cn",
					1 => "mail",
					"dn" => "CN=Roger Test,OU=Utilisateurs,DC=test",
					"cn" => [
						"count" => 1,
						0 => "Roger Test",
					],
					"mail" => [
						"count" => 0,
					],
				],
			]);

		DAOFactory::getInstance()->get_user_dao()->shouldReceive("get_user")->with("roger")->andReturn($roger);
		DAOFactory::getInstance()
			->get_user_dao()
			->shouldReceive("save")
			->withArgs(function ($username, $user) use ($roger) {
				return $username == "roger" && $user == $roger;
			})
			->andReturn([
				"roger" => $roger,
			]);

		$interacteur = new LoginInt();
		$résultat_obtenu = $interacteur->effectuer_login_par_identifiant("roger", "password", "test");

		$this->assertEquals($roger, $résultat_obtenu);
	}

	public function test_étant_donné_lutilisateur_jdoe_existant_dans_lannuaire_avec_courriel_mais_non_inscrit_et_une_authentification_de_type_ldap_lorsquon_login_avec_mot_de_passe_on_obtient_lutilisateur()
	{
		Config::set("authentification.local", false);
		Config::set("authentification.ldap", true);
		Config::set("ldap.domaine", "test");

		LDAP::shouldReceive("get_user")
			->with("jdoe", "password", "test")
			->andReturn([
				"count" => 0,
				0 => [
					"count" => 2,
					0 => "cn",
					1 => "mail",
					"dn" => "CN=Jdoe Test,OU=Utilisateurs,DC=test",
					"cn" => [
						"count" => 1,
						0 => "Jdoe Test",
					],
					"mail" => [
						"count" => 1,
						0 => "jdoe@progressionmail.com",
					],
				],
			]);

		$jdoe = new User(
			username: "jdoe",
			courriel: "jdoe@progressionmail.com",
			état: État::ACTIF,
			date_inscription: Carbon::now()->getTimestamp(),
		);

		DAOFactory::getInstance()->get_user_dao()->shouldReceive("get_user")->with("jdoe")->andReturn(null);
		DAOFactory::getInstance()
			->get_user_dao()
			->shouldReceive("trouver")
			->with(null, "jdoe@progressionmail.com")
			->andReturn(null);

		DAOFactory::getInstance()
			->get_user_dao()
			->shouldReceive("save")
			->withArgs(function ($username, $user) use ($jdoe) {
				return $username == "jdoe" && $user == $jdoe;
			})
			->andReturn([
				"jdoe" => $jdoe,
			]);

		$interacteur = new LoginInt();
		$résultat_obtenu = $interacteur->effectuer_login_par_identifiant("jdoe", "password", "test");

		$this->assertEquals($jdoe, $résultat_obtenu);
	}

	public function test_étant_donné_lutilisateur_jdoe_existant_dans_lannuaire_avec_courriel_déjà_utilisé_mais_non_inscrit_et_une_authentification_de_type_ldap_lorsquon_login_avec_mot_de_passe_on_obtient_une_exception()
	{
		Config::set("authentification.local", false);
		Config::set("authentification.ldap", true);
		Config::set("ldap.domaine", "test");

		LDAP::shouldReceive("get_user")
			->with("jdoe", "password", "test")
			->andReturn([
				"count" => 0,
				0 => [
					"count" => 2,
					0 => "cn",
					1 => "mail",
					"dn" => "CN=Jdoe Test,OU=Utilisateurs,DC=test",
					"cn" => [
						"count" => 1,
						0 => "Jdoe Test",
					],
					"mail" => [
						"count" => 1,
						0 => "jdoe@progressionmail.com",
					],
				],
			]);

		$jdoe = new User(
			username: "jdoe",
			courriel: "jdoe@progressionmail.com",
			état: État::ACTIF,
			date_inscription: Carbon::now()->getTimestamp(),
		);

		DAOFactory::getInstance()->get_user_dao()->shouldReceive("get_user")->with("jdoe")->andReturn(null);
		DAOFactory::getInstance()
			->get_user_dao()
			->shouldReceive("trouver")
			->with(null, "jdoe@progressionmail.com")
			->andReturn(new User(username: "jdoe2", courriel: "jdoe@progressionmail.com", date_inscription: 0));

		DAOFactory::getInstance()->get_user_dao()->shouldNotReceive("save");

		$this->expectException(DuplicatException::class);

		$interacteur = new LoginInt();
		$résultat_obtenu = $interacteur->effectuer_login_par_identifiant("jdoe", "password", "test");
	}

	public function test_étant_donné_lutilisateur_bob_existant_et_deja_inscrit_avec_un_nouveau_courriel_et_une_authentification_de_type_ldap_lorsquon_login_avec_mot_de_passe_on_obtient_lutilisateur_bob_avec_son_nouveau_courriel()
	{
		Config::set("authentification.local", false);
		Config::set("authentification.ldap", true);
		Config::set("ldap.domaine", "test");

		$bob = new User(username: "bob", courriel: "bob@autre_courriel.com", date_inscription: 990446400);

		LDAP::shouldReceive("get_user")
			->with("bob", "password", "test")
			->andReturn([
				"count" => 1,
				0 => [
					"count" => 2,
					0 => "cn",
					1 => "mail",
					"dn" => "CN=Bob Test,OU=Utilisateurs,DC=test",
					"cn" => [
						"count" => 1,
						0 => "Bob Test",
					],
					"mail" => [
						"count" => 1,
						0 => "bob@autre_courriel.com",
					],
				],
			]);

		DAOFactory::getInstance()
			->get_user_dao()
			->shouldReceive("trouver")
			->with(null, "bob@autre_courriel.com")
			->andReturn(null);

		DAOFactory::getInstance()
			->get_user_dao()
			->shouldReceive("save")
			->withArgs(function ($username, $user) use ($bob) {
				return $username == "bob" && $user == $bob;
			})
			->andReturn([
				"bob" => $bob,
			]);

		$interacteur = new LoginInt();

		$résultat_obtenu = $interacteur->effectuer_login_par_identifiant("bob", "password", "test");

		$this->assertEquals($bob, $résultat_obtenu);
	}

	public function test_étant_donné_lutilisateur_bob_existant_et_deja_inscrit_avec_nouveau_courriel_null_et_une_authentification_de_type_ldap_lorsquon_login_avec_mot_de_passe_on_obtient_lutilisateur_bob_avec_courriel_null()
	{
		Config::set("authentification.local", false);
		Config::set("authentification.ldap", true);
		Config::set("ldap.domaine", "test");

		LDAP::shouldReceive("get_user")
			->with("bob", "password", "test")
			->andReturn([
				"count" => 1,
				0 => [
					"count" => 2,
					0 => "cn",
					1 => "mail",
					"dn" => "CN=Bob Test,OU=Utilisateurs,DC=test",
					"cn" => [
						"count" => 1,
						0 => "Bob Test",
					],
					"mail" => [
						"count" => 0,
					],
				],
			]);

		$nouveau_bob = new User(username: "bob", courriel: null, date_inscription: 990446400);
		DAOFactory::getInstance()
			->get_user_dao()
			->shouldReceive("save")
			->withArgs(function ($username, $user) use ($nouveau_bob) {
				return $username == "bob" && $user == $nouveau_bob;
			})
			->andReturn([
				"bob" => $nouveau_bob,
			]);

		$interacteur = new LoginInt();

		$résultat_obtenu = $interacteur->effectuer_login_par_identifiant("bob", "password", "test");

		$this->assertEquals($nouveau_bob, $résultat_obtenu);
	}

	public function test_étant_donné_lutilisateur_bob_existant_et_deja_inscrit_avec_un_nouveau_courriel_déjà_utilisé_et_une_authentification_de_type_ldap_lorsquon_login_avec_mot_de_passe_on_obtient_une_exception()
	{
		Config::set("authentification.local", false);
		Config::set("authentification.ldap", true);
		Config::set("ldap.domaine", "test");

		$bob = new User(username: "bob", courriel: "bob@autre_courriel.com", date_inscription: 990446400);

		LDAP::shouldReceive("get_user")
			->with("bob", "password", "test")
			->andReturn([
				"count" => 1,
				0 => [
					"count" => 2,
					0 => "cn",
					1 => "mail",
					"dn" => "CN=Bob Test,OU=Utilisateurs,DC=test",
					"cn" => [
						"count" => 1,
						0 => "Bob Test",
					],
					"mail" => [
						"count" => 1,
						0 => "bob@autre_courriel.com",
					],
				],
			]);

		DAOFactory::getInstance()
			->get_user_dao()
			->shouldReceive("trouver")
			->with(null, "bob@autre_courriel.com")
			->andReturn(new User(username: "bob2", courriel: "bob@autre_courriel.com", date_inscription: 0));

		DAOFactory::getInstance()->get_user_dao()->shouldNotReceive("save");

		$this->expectException(DuplicatException::class);

		$interacteur = new LoginInt();
		$résultat_obtenu = $interacteur->effectuer_login_par_identifiant("bob", "password", "test");
	}

	// Login par clé
	public function test_étant_donné_lutilisateur_existant_bob_lorsquon_login_avec_une_clé_d_authentification_valide_on_obtient_un_user_bob()
	{
		$interacteur = new LoginInt();
		$résultat_obtenu = $interacteur->effectuer_login_par_clé("bob", "clé valide", "secret");

		$this->assertEquals($this->bob, $résultat_obtenu);
	}

	public function test_étant_donné_lutilisateur_existant_bob_lorsquon_login_avec_une_clé_d_authentification_inexistante_on_obtient_null()
	{
		$interacteur = new LoginInt();
		$résultat_obtenu = $interacteur->effectuer_login_par_clé("bob", "clé inexistante", "secret");

		$this->assertNull($résultat_obtenu);
	}

	public function test_étant_donné_lutilisateur_existant_bob_lorsquon_login_avec_une_clé_d_authentification_et_un_secret_invalide_on_obtient_null()
	{
		$interacteur = new LoginInt();
		$résultat_obtenu = $interacteur->effectuer_login_par_clé("bob", "clé valide", "mauvais secret");

		$this->assertNull($résultat_obtenu);
	}

	public function test_étant_donné_lutilisateur_existant_bob_lorsquon_login_avec_une_clé_d_authentification_expirée_on_obtient_null()
	{
		$interacteur = new LoginInt();
		$résultat_obtenu = $interacteur->effectuer_login_par_clé("bob", "cle_expiree", "secret");

		$this->assertNull($résultat_obtenu);
	}

	public function test_étant_donné_lutilisateur_existant_bob_lorsquon_login_avec_une_clé_d_authentification_révoquée_on_obtient_null()
	{
		$interacteur = new LoginInt();
		$résultat_obtenu = $interacteur->effectuer_login_par_clé("bob", "clé révoquée", "secret");

		$this->assertNull($résultat_obtenu);
	}
}
