<?php
/*
   This file is part of Progression.

   Progression is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Progression is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Progression.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace progression\domaine\interacteur;

use progression\domaine\entité\Avancement;
use progression\domaine\entité\question\QuestionProg;
use progression\dao\DAOFactory;
use progression\TestCase;
use Mockery;

final class ModifierAvancementIntTests extends TestCase
{
	public function setUp(): void
	{
		parent::setUp();

		$mockDAOFactory = Mockery::mock("progression\\dao\\DAOFactory");

		$mockDAOFactory->allows()->get_question_dao()->andReturn(Mockery::mock("progression\\dao\\QuestionDAO"));
		$mockDAOFactory->allows()->get_avancement_dao()->andReturn(Mockery::mock("progression\\dao\\AvancementDAO"));

		DAOFactory::setInstance($mockDAOFactory);
	}

	public function test_étant_donné_un_avancement_lorsquon_modifie_ses_attributs_seuls_ses_attributs_modifiables_le_sont_et_on_reçoit_un_avancement_modifié()
	{
		$résultat_attendu = new Avancement(titre: "Une question", niveau: "Un niveau", extra: "C'est extra!");

		$mockQuestionDAO = DAOFactory::getInstance()->get_question_dao();
		$mockQuestionDAO
			->shouldReceive("get_question")
			->with("uri")
			->andReturn(new QuestionProg(titre: "Une question", niveau: "Un niveau"));

		$mockAvancementDAO = DAOFactory::getInstance()->get_avancement_dao();
		$mockAvancementDAO
			->shouldReceive("save")
			->withArgs(function ($username, $question_uri, $type, $avancement) use ($résultat_attendu) {
				return $username == "bob" &&
					$question_uri == "uri" &&
					$type == "prog" &&
					$avancement == $résultat_attendu;
			})
			->andReturn(["uri" => $résultat_attendu]);

		$avancement_test = new Avancement();

		$résultat_obtenu = (new ModifierAvancementInt())->modifier_avancement("bob", "uri", $avancement_test, [
			"titre" => "Un avancement",
			"niveau" => "débutant",
			"extra" => "C'est extra!",
		]);

		$this->assertEquals($résultat_attendu, $résultat_obtenu);
	}
}
