<?php
/*
   This file is part of Progression.

   Progression is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Progression is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Progression.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace progression\dao;

use progression\domaine\entité\question\{Question, État, QuestionProg, QuestionSeq};
use progression\domaine\entité\{Avancement, AvancementSeq, TentativeProg, Sauvegarde};
use progression\TestCase;
use Mockery;

final class AvancementDAOTests extends TestCase
{
	public function setUp(): void
	{
		parent::setUp();

		app("db")->connection()->beginTransaction();

		$question_1 = new QuestionProg(titre: "Un titre", niveau: "facile");
		$question_2 = new QuestionProg(titre: "Un titre 2", niveau: "facile");
		$question_seq = new QuestionSeq(
			titre: "Une question séquence",
			niveau: "graduel",
			questions: [
				"https://depot.com/roger/questions_prog/fonctions01/appeler_une_autre_fonction" => $question_1,
				"https://depot.com/roger/questions_prog/fonctions01/appeler_une_autre_fonction2" => $question_2,
			],
		);

		$mockQuestionDAO = Mockery::mock("progression\\dao\\question\\QuestionDAO");
		$mockQuestionDAO
			->shouldReceive("get_question")
			->with("https://test.com/question_seq/info.yml", ["questions"])
			->andReturn($question_seq);
		//		$mockQuestionDAO
		//			->shouldReceive("get_question")
		//			->with("https://depot.com/roger/questions_prog/fonctions01/appeler_une_autre_fonction", [])
		//			->andReturn($question_1);
		//		$mockQuestionDAO
		//			->shouldReceive("get_question")
		//			->with("https://depot.com/roger/questions_prog/fonctions01/appeler_une_autre_fonction2", [])
		//			->andReturn($question_2);

		$mockDAOFactory = Mockery::mock("progression\\dao\\DAOFactory")->makePartial();
		$mockDAOFactory->shouldReceive("get_question_dao")->andReturn($mockQuestionDAO);
		DAOFactory::setInstance($mockDAOFactory);
	}

	public function tearDown(): void
	{
		app("db")->connection()->rollBack();

		parent::tearDown();
	}

	public function test_étant_donné_un_avancement_existant_lorsquon_cherche_par_username_et_question_uri_incluant_les_tentatives_on_obtient_un_objet_avancement_correspondant_avec_ses_tentatives()
	{
		$résultat_attendu = new Avancement(
			date_modification: 1615696276,
			tentatives: [
				1615696276 => new TentativeProg("python", 'print("Tourlou le monde!")', 1615696276, false, [], 2, 3456),
			],
			titre: "Un titre",
			niveau: "facile",
		);

		$résultat_observée = (new AvancementDAO())->get_avancement(
			"bob",
			"https://depot.com/roger/questions_prog/fonctions01/appeler_une_fonction",
			includes: ["tentatives"],
		);
		$this->assertEquals($résultat_attendu, $résultat_observée);
	}

	public function test_étant_donné_un_avancement_existant_lorsquon_cherche_par_username_et_question_uri_incluant_les_sauvegardes_on_obtient_un_objet_avancement_correspondant_avec_ses_sauvegardes()
	{
		$résultat_attendu = new Avancement(
			état: État::NONREUSSI,
			titre: "Un titre",
			niveau: "facile",
			date_modification: 1615696276,
			sauvegardes: [
				"python" => new Sauvegarde(1620150294, "print(\"Hello world!\")"),
				"java" => new Sauvegarde(1620150375, "System.out.println(\"Hello world!\");"),
			],
		);

		$résultat_observée = (new AvancementDAO())->get_avancement(
			"bob",
			"https://depot.com/roger/questions_prog/fonctions01/appeler_une_fonction",
			includes: ["sauvegardes"],
		);
		$this->assertEquals($résultat_attendu, $résultat_observée);
	}

	public function test_étant_donné_un_avancement_existant_lorsquon_cherche_par_username_et_question_uri_incluant_les_tentatives_et_sauvegardes_on_obtient_un_objet_avancement_correspondant_avec_ses_tentatives_et_sauvegardes()
	{
		$résultat_attendu = new Avancement(
			tentatives: [
				1615696276 => new TentativeProg("python", 'print("Tourlou le monde!")', 1615696276, false, [], 2, 3456),
			],
			titre: "Un titre",
			niveau: "facile",
			sauvegardes: [
				"python" => new Sauvegarde(1620150294, "print(\"Hello world!\")"),
				"java" => new Sauvegarde(1620150375, "System.out.println(\"Hello world!\");"),
			],
		);

		$résultat_observée = (new AvancementDAO())->get_avancement(
			"bob",
			"https://depot.com/roger/questions_prog/fonctions01/appeler_une_fonction",
			includes: ["tentatives", "sauvegardes"],
		);
		$this->assertEquals($résultat_attendu, $résultat_observée);
	}

	public function étant_donné_un_avancement_séquence_lorsquon_le_récupère_sans_inclusion_on_obtient_lavancement_seul_avec_état_début()
	{
		$résultat_attendu = new AvancementSeq(titre: "Une question séquence", niveau: "graduel", avancements: []);

		$résultat_observée = (new AvancementDAO())->get_avancement("bob", "https://test.com/question_seq/info.yml");

		$this->assertEquals($résultat_attendu, $résultat_observée);
	}

	public function test_étant_donné_un_avancement_séquence_lorsquon_le_récupère_en_incluant_les_sous_avancement_on_obtient_un_tableau_davancements_avec_état_calculé()
	{
		$avancement1 = new Avancement(
			état: État::NONREUSSI,
			titre: "Un titre",
			niveau: "facile",
			date_modification: 1645739981,
			tentatives: [],
		);

		$avancement2 = new Avancement(
			état: État::REUSSI,
			titre: "Un titre 2",
			niveau: "facile",
			date_réussite: 1645739969,
			date_modification: 1645739991,
			tentatives: [],
		);

		$résultat_attendu = new AvancementSeq(
			état: État::NONREUSSI,
			titre: "Une question séquence",
			niveau: "graduel",
			avancements: [
				"https://depot.com/roger/questions_prog/fonctions01/appeler_une_autre_fonction" => $avancement1,
				"https://depot.com/roger/questions_prog/fonctions01/appeler_une_autre_fonction2" => $avancement2,
			],
		);

		$résultat_observée = (new AvancementDAO())->get_avancement("bob", "https://test.com/question_seq/info.yml", [
			"avancements",
		]);

		$this->assertEquals($résultat_attendu, $résultat_observée);
	}

	public function test_étant_donné_un_utilisateur_ayant_des_avancements_lorsquon_cherche_par_username_on_obtient_un_tableau_d_objets_avancement()
	{
		$av0 = new Avancement(
			état: État::NONREUSSI,
			date_modification: 1615696276,
			tentatives: [],
			titre: "Un titre",
			niveau: "facile",
		);

		$av1 = new Avancement(
			état: État::NONREUSSI,
			date_modification: 1645739981,
			tentatives: [],
			titre: "Un titre",
			niveau: "facile",
			sauvegardes: [],
		);

		$av2 = new Avancement(
			état: État::REUSSI,
			date_modification: 1645739991,
			date_réussite: 1645739969,
			tentatives: [],
			titre: "Un titre 2",
			niveau: "facile",
			sauvegardes: [],
		);

		$av3 = new AvancementSeq(
			état: État::NONREUSSI,
			date_modification: 1645739991,
			titre: "Une question séquence",
			niveau: "graduel",
		);

		$résultat_attendu = [
			"https://depot.com/roger/questions_prog/fonctions01/appeler_une_fonction" => $av0,
			"https://depot.com/roger/questions_prog/fonctions01/appeler_une_autre_fonction" => $av1,
			"https://depot.com/roger/questions_prog/fonctions01/appeler_une_autre_fonction2" => $av2,
			"https://test.com/question_seq/info.yml" => $av3,
		];

		$résultat_observée = (new AvancementDAO())->get_tous("bob");
		$this->assertEquals($résultat_attendu, $résultat_observée);
	}

	public function test_étant_donné_un_utilisateur_sans_avancement_lorsquon_cherche_par_username_on_obtient_un_tableau_vide()
	{
		$résultat_attendu = [];

		$résultat_observée = (new AvancementDAO())->get_tous("admin");
		$this->assertEquals($résultat_attendu, $résultat_observée);
	}

	public function test_étant_donné_un_avancement_inexistant_lorsquon_le_cherche_par_username_et_question_uri_on_obtient_null()
	{
		$résultat_observée = (new AvancementDAO())->get_avancement(
			"bob",
			"https://depot.com/roger/questions_prog/fonctions01/appeler_une_fonction_inexistante",
		);
		$this->assertNull($résultat_observée);
	}

	public function test_étant_donné_un_nouvel_avancement_lorsquon_le_sauvegarde_il_est_sauvegardé_dans_la_BD_et_on_obtient_lavancement()
	{
		$nouvel_avancement = new Avancement(
			date_modification: 1645739981,
			date_réussite: 1645739959,
			tentatives: [],
			titre: "Un titre",
			niveau: "facile",
		);

		// L'avancement est retourné
		$résultat_observée = (new AvancementDAO())->save(
			"bob",
			"https://depot.com/roger/une_nouvelle_question",
			"prog",
			$nouvel_avancement,
		);

		$this->assertEquals(
			[
				"https://depot.com/roger/une_nouvelle_question" => $nouvel_avancement,
			],
			$résultat_observée,
		);

		// L'avancement a été sauvegardé
		$résultat_observée = (new AvancementDAO())->get_avancement(
			"bob",
			"https://depot.com/roger/une_nouvelle_question",
		);

		$this->assertEquals($nouvel_avancement, $résultat_observée);
	}

	public function test_étant_donné_un_avancement_existant_lorsquon_le_modifie_il_est_sauvegardé_dans_la_BD_et_on_obtient_lavancement_modifié()
	{
		$résultat_attendu = new Avancement(
			tentatives: [],
			titre: "Nouveau titre",
			niveau: "Nouveau niveau",
			sauvegardes: [],
		);

		// L'avancement est retourné
		$résultat_observée = (new AvancementDAO())->save(
			"bob",
			"https://depot.com/roger/questions_prog/fonctions01/appeler_une_fonction",
			"prog",
			$résultat_attendu,
		);

		$this->assertEquals(
			[
				"https://depot.com/roger/questions_prog/fonctions01/appeler_une_fonction" => $résultat_attendu,
			],
			$résultat_observée,
		);

		// L'avancement a été sauvegardé
		$résultat_observée = (new AvancementDAO())->get_avancement(
			"bob",
			"https://depot.com/roger/questions_prog/fonctions01/appeler_une_fonction",
		);

		$this->assertEquals($résultat_attendu, $résultat_observée);
	}
}
