<?php
/*
  This file is part of Progression.

  Progression is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Progression is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Progression.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace progression\dao\banque;

use progression\TestCase;
use progression\dao\EntitéDAO;
use progression\dao\chargeur\{Chargeur, ChargeurException};
use progression\domaine\entité\question\{Question, QuestionProg, QuestionSys};
use progression\domaine\entité\{Exécutable, TestProg, TestSys};
use progression\domaine\interacteur\IntégritéException;
use progression\domaine\entité\Banque;
use progression\dao\DAOException;

use \InvalidArgumentException;

final class BanqueDAOTests extends TestCase
{
	public static $questions_1;
	public static $questions_2;

	public function setUp(): void
	{
		parent::setUp();
		app("db")->connection()->beginTransaction();

		self::$questions_1 = [
			"file:///var/www/progression/tests/progression/dao/démo/banque_1/question_1.yml" => new QuestionProg(
				titre: "Question 1",
				description: "Ceci est la première question de test",
				objectif: "Faire un test #1",
				exécutables: ["python" => new Exécutable(code: "print(42)", lang: "python")],
				tests: [new TestProg(sortie_attendue: 42)],
			),
			"file:///var/www/progression/tests/progression/dao/démo/banque_1/question_2.yml" => new QuestionProg(
				titre: "Question 2",
				description: "Ceci est la deuxième question de test",
				objectif: "Faire un test #2",
				exécutables: ["python" => new Exécutable(code: "print(42)", lang: "python")],
				tests: [new TestProg(sortie_attendue: 42)],
			),
			"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_3.yml" => new QuestionSys(
				titre: "Question 3",
				description: "Ceci est la troisième question de test",
				objectif: "Faire un test #3",
				image: "ubuntu",
				tests: [new TestSys(validation: "true")],
			),
			"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_4.yml" => new QuestionSys(
				titre: "Question 4",
				description: "Ceci est la quatrième question de test",
				objectif: "Faire un test #4",
				image: "ubuntu",
				tests: [new TestSys(validation: "true")],
			),
		];
		self::$questions_2 = [
			"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_3.yml" => new QuestionSys(
				titre: "Question 3",
				description: "Ceci est la troisième question de test",
				objectif: "Faire un test #3",
				image: "ubuntu",
				tests: [new TestSys(validation: "true")],
			),
			"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_4.yml" => new QuestionSys(
				titre: "Question 4",
				description: "Ceci est la quatrième question de test",
				objectif: "Faire un test #4",
				image: "ubuntu",
				tests: [new TestSys(validation: "true")],
			),
		];
	}

	public function tearDown(): void
	{
		app("db")->connection()->rollBack();
		parent::tearDown();
	}

	# Récupération
	public function test_étant_donné_un_utilisateur_qui_nexiste_pas_lorsquon_récupère_toutes_les_banques_on_obtient_une_DAOException()
	{
		$résultat_observé = (new BanqueDAO())->get_toutes("joe");

		$résultats_attendu = [];

		$this->assertEquals($résultats_attendu, $résultat_observé);
	}

	public function test_étant_donné_un_utilisateur_existant_qui_na_pas_de_banque_lorsquon_récupère_toutes_les_banques_on_obtient_une_liste_vide()
	{
		$résultat_observé = (new BanqueDAO())->get_toutes("jdoe");

		$résultats_attendu = [];

		$this->assertEquals($résultats_attendu, $résultat_observé);
	}

	public function test_étant_donné_un_utilisateur_qui_possède_deux_banques_de_questions_lorsquon_récupère_toutes_ses_banque_sans_inclusion_on_obtient_les_infos_de_base_des_deux_banques()
	{
		$résultats_attendu = [
			"file:///var/www/progression/tests/progression/dao/démo/banque_1/contenu.yml" => new Banque(
				nom: "Test banque de questions 1 - fichier yaml valide",
			),
			"file:///var/www/progression/tests/progression/dao/démo/banque_2/contenu.yml" => new Banque(
				nom: "Test banque de questions 2 - fichier yaml valide",
			),
		];

		$résultat_observé = (new BanqueDAO())->get_toutes("bob");
		$this->assertEquals($résultats_attendu, $résultat_observé);
	}

	public function test_étant_donné_un_utilisateur_qui_possède_deux_banques_de_questions_lorsquon_récupère_toutes_ses_banques_en_incluant_les_sous_banques_on_obtient_les_deux_banques_complètes_et_les_infos_de_base_des_questions()
	{
		$résultats_attendu = [
			"file:///var/www/progression/tests/progression/dao/démo/banque_1/contenu.yml" => new Banque(
				nom: "Test banque de questions 1 - fichier yaml valide",
				questions: [
					"file:///var/www/progression/tests/progression/dao/démo/banque_1/question_1.yml" => new Question(
						titre: "Test banque de questions 1 - fichier yaml valide",
					),
					"file:///var/www/progression/tests/progression/dao/démo/banque_1/question_2.yml" => new Question(
						titre: "Test banque de questions 2 - fichier yaml valide",
					),
					"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_3.yml" => new Question(
						titre: "Test banque de questions 3 - fichier yaml valide",
					),
					"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_4.yml" => new Question(
						titre: "Test banque de questions 4 - fichier yaml valide",
					),
				],
			),
			"file:///var/www/progression/tests/progression/dao/démo/banque_2/contenu.yml" => new Banque(
				nom: "Test banque de questions 2 - fichier yaml valide",
				questions: [
					"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_3.yml" => new Question(
						titre: "Test banque de questions 3 - fichier yaml valide",
					),
					"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_4.yml" => new Question(
						titre: "Test banque de questions 4 - fichier yaml valide",
					),
				],
			),
		];

		$résultat_observé = (new BanqueDAO())->get_toutes("bob", ["banques"]);
		$this->assertEquals($résultats_attendu, $résultat_observé);
	}

	public function test_étant_donné_une_banque_contenant_des_sous_banques_avec_des_caractères_spéciaux_lorsquon_la_récupère_on_obtient_les_noms_de_sous_banques_encodés()
	{
		$résultats_attendu = new Banque(
			nom: "Test banque de questions avec caractères spéciaux",
			banques: [
				"test%2Fslash" => new Banque(nom: "test/slash"),
				"test\backslash" => new Banque(nom: "test\backslash"),
				"test#croisillon" => new Banque(nom: "test#croisillon"),
			],
		);

		$résultat_observé = (new BanqueDAO())->get_banque(
			"jane",
			"file:///var/www/progression/tests/progression/dao/démo/banque_slash.yml",
		);

		$this->assertEquals($résultats_attendu, $résultat_observé);
	}

	public function test_étant_donné_un_utilisateur_qui_possède_une_banque_hiérarchisée_lorsquon_récupère_toutes_ses_banques_en_incluant_les_sous_banques_on_obtient_la_banque_et_les_infos_de_base_des_questions_et_sous_banques()
	{
		$résultats_attendu = [
			"file:///var/www/progression/tests/progression/dao/démo/banque_complexe/contenu.yml" => new Banque(
				nom: "Test banque de questions complexe - fichier yaml valide",
				banques: [
					"sous-répertoire 1" => new Banque(
						nom: "sous-répertoire 1",
						questions: [
							"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_3.yml" => new Question(
								titre: "Test banque de questions 3 - fichier yaml valide",
							),
						],
						banques: [
							"sous-sous-répertoire de sous-répertoire 1" => new Banque(
								nom: "sous-sous-répertoire de sous-répertoire 1",
								questions: [
									"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_4.yml" => new Question(
										titre: "Test banque de questions 4 - fichier yaml valide",
									),
								],
							),
							"1" => new Banque(
								questions: [
									"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_4.yml" => new Question(),
								],
							),
						],
					),
					"file:///var/www/progression/tests/progression/dao/démo/banque_complexe/sous-banque/contenu.yml" => new Banque(
						nom: "sous-répertoire 2",
					),
					"sous-répertoire 3" => new Banque(
						nom: "sous-répertoire 3",
						banques: [
							"Sous-répertoire du sous-répertoire 3" => new Banque(
								nom: "Sous-répertoire du sous-répertoire 3",
								questions: [
									"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_4.yml" => new Question(
										titre: "Test banque de questions 4 - fichier yaml valide",
									),
								],
							),
						],
					),
				],
				questions: [
					"file:///var/www/progression/tests/progression/dao/démo/banque_1/question_1.yml" => new Question(
						titre: "Test banque de questions 1 - fichier yaml valide",
						niveau: "base",
						description: "Une question de test",
						objectif: "Faire un test",
					),
					"file:///var/www/progression/tests/progression/dao/démo/banque_1/question_2.yml" => new Question(
						titre: "Test banque de questions 2 - fichier yaml valide",
					),
				],
			),
		];

		$résultat_observé = (new BanqueDAO())->get_toutes("Stefany", ["banques"]);

		$this->assertEquals($résultats_attendu, $résultat_observé);
	}

	public function test_étant_donné_un_utilisateur_qui_possède_une_banque_de_questions_hiérarchisée_lorsquon_récupère_la_banque_sans_inclusion_on_obtient_la_banque_complète_et_les_infos_de_base_des_sous_banques_et_questions()
	{
		$résultats_attendu = new Banque(
			nom: "Test banque de questions complexe - fichier yaml valide",
			banques: [
				"sous-répertoire 1" => new Banque(
					nom: "sous-répertoire 1",
					questions: [
						"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_3.yml" => new Question(
							titre: "Test banque de questions 3 - fichier yaml valide",
						),
					],
					banques: [
						"sous-sous-répertoire de sous-répertoire 1" => new Banque(
							nom: "sous-sous-répertoire de sous-répertoire 1",
							questions: [
								"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_4.yml" => new Question(
									titre: "Test banque de questions 4 - fichier yaml valide",
								),
							],
						),
						"1" => new Banque(
							questions: [
								"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_4.yml" => new Question(),
							],
						),
					],
				),
				"file:///var/www/progression/tests/progression/dao/démo/banque_complexe/sous-banque/contenu.yml" => new Banque(
					nom: "sous-répertoire 2",
				),
				"sous-répertoire 3" => new Banque(
					nom: "sous-répertoire 3",
					banques: [
						"Sous-répertoire du sous-répertoire 3" => new Banque(
							nom: "Sous-répertoire du sous-répertoire 3",
							questions: [
								"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_4.yml" => new Question(
									titre: "Test banque de questions 4 - fichier yaml valide",
								),
							],
						),
					],
				),
			],
			questions: [
				"file:///var/www/progression/tests/progression/dao/démo/banque_1/question_1.yml" => new Question(
					titre: "Test banque de questions 1 - fichier yaml valide",
					description: "Une question de test",
					niveau: "base",
					objectif: "Faire un test",
				),
				"file:///var/www/progression/tests/progression/dao/démo/banque_1/question_2.yml" => new Question(
					titre: "Test banque de questions 2 - fichier yaml valide",
				),
			],
		);

		$résultat_observé = (new BanqueDAO())->get_banque(
			"jane",
			"file:///var/www/progression/tests/progression/dao/démo/banque_complexe/contenu.yml",
			[],
		);

		$this->assertEquals($résultats_attendu, $résultat_observé);
	}

	public function test_étant_donné_un_utilisateur_qui_possède_une_banque_de_questions_hiérarchisée_lorsquon_récupère_la_banque_en_incluant_les_questions_on_obtient_la_banques_les_questions_complètes_et_les_infos_de_base_des_sous_banques()
	{
		$résultats_attendu = new Banque(
			nom: "Test banque de questions complexe - fichier yaml valide",
			banques: [
				"sous-répertoire 1" => new Banque(
					nom: "sous-répertoire 1",
					questions: [
						"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_3.yml" => new Question(
							titre: "Test banque de questions 3 - fichier yaml valide",
						),
					],
					banques: [
						"sous-sous-répertoire de sous-répertoire 1" => new Banque(
							nom: "sous-sous-répertoire de sous-répertoire 1",
							questions: [
								"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_4.yml" => new Question(
									titre: "Test banque de questions 4 - fichier yaml valide",
								),
							],
						),
						"1" => new Banque(
							questions: [
								"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_4.yml" => new Question(),
							],
						),
					],
				),
				"file:///var/www/progression/tests/progression/dao/démo/banque_complexe/sous-banque/contenu.yml" => new Banque(
					nom: "sous-répertoire 2",
				),
				"sous-répertoire 3" => new Banque(
					nom: "sous-répertoire 3",
					banques: [
						"Sous-répertoire du sous-répertoire 3" => new Banque(
							nom: "Sous-répertoire du sous-répertoire 3",
							questions: [
								"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_4.yml" => new Question(
									titre: "Test banque de questions 4 - fichier yaml valide",
								),
							],
						),
					],
				),
			],
			questions: [
				"file:///var/www/progression/tests/progression/dao/démo/banque_1/question_1.yml" => new QuestionProg(
					titre: "Question 1",
					description: "Ceci est la première question de test",
					objectif: "Faire un test #1",
					exécutables: ["python" => new Exécutable(code: "print(42)", lang: "python")],
					tests: [new TestProg(sortie_attendue: 42)],
				),
				"file:///var/www/progression/tests/progression/dao/démo/banque_1/question_2.yml" => new QuestionProg(
					titre: "Question 2",
					description: "Ceci est la deuxième question de test",
					objectif: "Faire un test #2",
					exécutables: ["python" => new Exécutable(code: "print(42)", lang: "python")],
					tests: [new TestProg(sortie_attendue: 42)],
				),
			],
		);

		$résultat_observé = (new BanqueDAO())->get_banque(
			"jane",
			"file:///var/www/progression/tests/progression/dao/démo/banque_complexe/contenu.yml",
			["questions"],
		);

		$this->assertEquals($résultats_attendu, $résultat_observé);
	}

	public function test_étant_donné_un_utilisateur_qui_possède_une_banque_de_questions_hiérarchisée_lorsquon_récupère_la_banque_en_incluant_les_questions_et_les_banques_on_obtient_la_banques_les_questions_complètes_et_les_sous_banques_complètes()
	{
		$résultats_attendu = new Banque(
			nom: "Test banque de questions complexe - fichier yaml valide",
			banques: [
				"sous-répertoire 1" => new Banque(
					nom: "sous-répertoire 1",
					questions: [
						"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_3.yml" => new Question(
							titre: "Test banque de questions 3 - fichier yaml valide",
						),
					],
					banques: [
						"sous-sous-répertoire de sous-répertoire 1" => new Banque(
							nom: "sous-sous-répertoire de sous-répertoire 1",
							questions: [
								"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_4.yml" => new Question(
									titre: "Test banque de questions 4 - fichier yaml valide",
								),
							],
						),
						"1" => new Banque(
							questions: [
								"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_4.yml" => new Question(),
							],
						),
					],
				),
				"file:///var/www/progression/tests/progression/dao/démo/banque_complexe/sous-banque/contenu.yml" => new Banque(
					nom: "sous-répertoire 2",
					questions: [
						"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_3.yml" => new Question(
							titre: "Test banque de questions 3 - fichier yaml valide",
						),
					],
					banques: [
						"sous-sous-répertoire" => new Banque(
							nom: "sous-sous-répertoire",
							questions: [
								"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_4.yml" => new Question(
									titre: "Test banque de questions 4 - fichier yaml valide",
								),
							],
							banques: [
								"https://loin.com" => new Banque(nom: "sous-sous-sous-répertoire"),
							],
						),
					],
				),
				"sous-répertoire 3" => new Banque(
					nom: "sous-répertoire 3",
					banques: [
						"Sous-répertoire du sous-répertoire 3" => new Banque(
							nom: "Sous-répertoire du sous-répertoire 3",
							questions: [
								"file:///var/www/progression/tests/progression/dao/démo/banque_2/question_4.yml" => new Question(
									titre: "Test banque de questions 4 - fichier yaml valide",
								),
							],
						),
					],
				),
			],
			questions: [
				"file:///var/www/progression/tests/progression/dao/démo/banque_1/question_1.yml" => new QuestionProg(
					titre: "Question 1",
					description: "Ceci est la première question de test",
					objectif: "Faire un test #1",
					exécutables: ["python" => new Exécutable(code: "print(42)", lang: "python")],
					tests: [new TestProg(sortie_attendue: 42)],
				),
				"file:///var/www/progression/tests/progression/dao/démo/banque_1/question_2.yml" => new QuestionProg(
					titre: "Question 2",
					description: "Ceci est la deuxième question de test",
					objectif: "Faire un test #2",
					exécutables: ["python" => new Exécutable(code: "print(42)", lang: "python")],
					tests: [new TestProg(sortie_attendue: 42)],
				),
			],
		);

		$résultat_observé = (new BanqueDAO())->get_banque(
			"jane",
			"file:///var/www/progression/tests/progression/dao/démo/banque_complexe/contenu.yml",
			["questions", "banques"],
		);

		$this->assertEquals($résultats_attendu, $résultat_observé);
	}

	public function test_étant_donné_une_banque_inexistante_lorsquon_la_récupère_on_obtient_une_ChargeurException()
	{
		$this->expectException(ChargeurException::class);

		$résultat_observé = (new BanqueDAO())->get_banque(
			"jane",
			"file:///var/www/progression/tests/progression/dao/démo/banque_inexistante.yml",
			[],
		);
	}

	public function test_étant_donné_une_banque_existante_non_enregistrée_lorsquon_la_récupère_on_obtient_la_banque()
	{
		$résultat_observé = (new BanqueDAO())->get_banque(
			"bob",
			"file:///var/www/progression/tests/progression/dao/démo/banque_complexe/contenu.yml",
			[],
		);

		$this->assertEquals("Banque de test", $résultat_observé->nom);
	}

	public function test_étant_donné_une_banque_aux_questions_inaccessibles_lorsquon_la_récupère_on_obtient_une_ChargeurException()
	{
		$this->expectException(ChargeurException::class);

		$résultat_observé = (new BanqueDAO())->get_banque(
			"jane",
			"file:///var/www/progression/tests/progression/dao/démo/banque_questions_inexistantes.yml",
			["questions"],
		);
	}

	public function test_étant_donné_une_sous_banque_inaccessible_lorsquon_la_récupère_on_obtient_une_ChargeurException()
	{
		$this->expectException(ChargeurException::class);

		$résultat_observé = (new BanqueDAO())->get_banque(
			"jane",
			"file:///var/www/progression/tests/progression/dao/démo/banque_sous_banque_inexistante.yml",
			["banques"],
		);
	}

	public function test_étant_donné_une_sous_banque_avec_un_url_vide_lorsquon_la_récupère_on_obtient_une_ChargeurException()
	{
		$this->expectException(ChargeurException::class);

		$résultat_observé = (new BanqueDAO())->get_banque(
			"jane",
			"file:///var/www/progression/tests/progression/dao/démo/banque_sous_banque_incomplete.yml",
			["banques"],
		);
	}

	public function test_étant_donné_une_banque_contenant_une_question_sans_champ_url_lorsquon_la_récupére_on_obtient_une_ChargeurException()
	{
		$this->expectException(ChargeurException::class);

		$résultat_observé = (new BanqueDAO())->get_banque(
			"jane",
			"file:///var/www/progression/tests/progression/dao/démo/banque_questions_sans_url.yml",
			["questions"],
		);
	}

	public function test_étant_donné_une_banque_contenant_une_question_avec_un_url_vide_lorsquon_la_récupére_on_obtient_une_ChargeurException()
	{
		$this->expectException(ChargeurException::class);

		$résultat_observé = (new BanqueDAO())->get_banque(
			"jane",
			"file:///var/www/progression/tests/progression/dao/démo/banque_questions_avec_url_vide.yml",
			["questions"],
		);
	}

	public function test_étant_donné_une_banque_sans_nom_donné_lorsquon_la_récupére_on_obtient_la_banque_avec_le_nom_contenu_dans_la_banque()
	{
		$résultats_attendu = new Banque(nom: "Banque nommée");

		$résultat_observé = (new BanqueDAO())->get_banque(
			"jane",
			"file:///var/www/progression/tests/progression/dao/démo/banque_anonyme/nommée.yml",
			[],
		);

		$this->assertEquals($résultats_attendu, $résultat_observé);
	}

	public function test_étant_donné_une_banque_sans_nom_donné_ni_contenu_dans_la_banque_lorsquon_la_récupère_on_obtient_une_banque_sans_nom()
	{
		$résultat_observé = (new BanqueDAO())->get_banque(
			"jane",
			"file:///var/www/progression/tests/progression/dao/démo/banque_anonyme/anonyme.yml",
			[],
		);

		$résultat_attendu = new Banque(
			banques: [
				"file:///var/www/progression/tests/progression/dao/démo/banque_anonyme/sous_banque_anonyme.yml" => new Banque(),
			],
		);

		$this->assertEquals($résultat_attendu, $résultat_observé);
	}

	public function test_étant_donné_une_sous_banque_sans_nom_donné_ni_contenu_dans_la_banque_lorsquon_la_récupère_on_obtient_une_sous_banque_sans_nom()
	{
		$résultat_observé = (new BanqueDAO())->get_banque(
			"jane",
			"file:///var/www/progression/tests/progression/dao/démo/banque_anonyme/anonyme.yml",
			["banques"],
		);

		$résultat_attendu = new Banque(
			banques: [
				"file:///var/www/progression/tests/progression/dao/démo/banque_anonyme/sous_banque_anonyme.yml" => new Banque(),
			],
		);

		$this->assertEquals($résultat_attendu, $résultat_observé);
	}

	# Création
	public function test_étant_donné_une_banque_inexistante_lorsquon_la_sauvegarde_avec_un_nom_et_un_URL_alors_la_banque_est_ajoutée_dans_la_BD()
	{
		$dao = new BanqueDAO();
		$banques = $dao->get_toutes("bob");

		$banque_ajoutée = new Banque(nom: "Ma nouvelle banque");

		$résultat_observé = $dao->save(
			"bob",
			"file:///var/www/progression/tests/progression/dao/démo/nouvelle_banque/contenu.yml",
			$banque_ajoutée,
		);

		$résultats_attendu = [
			"file:///var/www/progression/tests/progression/dao/démo/nouvelle_banque/contenu.yml" => new Banque(
				nom: "Ma nouvelle banque",
			),
		];

		//Vérifie la banque retournée par la sauvegarde
		$this->assertEquals($résultats_attendu, $résultat_observé);

		//La banque a bel et bien été modifiée dans la BD
		$this->assertEquals(
			$banque_ajoutée,
			(new BanqueDAO())->get_banque(
				"bob",
				"file:///var/www/progression/tests/progression/dao/démo/nouvelle_banque/contenu.yml",
			),
		);
	}

	public function test_étant_donné_une_banque_inexistante_lorsquon_la_sauvegarde_sans_nom_alors_la_banque_est_ajoutée_dans_la_BD_sans_nom()
	{
		$dao = new BanqueDAO();
		$banques = $dao->get_toutes("bob");

		$banque_ajoutée = new Banque();

		$résultat_observé = $dao->save(
			"bob",
			"file:///var/www/progression/tests/progression/dao/démo/banque_anonyme/anonyme.yml",
			$banque_ajoutée,
		);

		//Vérifie la banque retournée par la sauvegarde
		$this->assertEquals(
			["file:///var/www/progression/tests/progression/dao/démo/banque_anonyme/anonyme.yml" => new Banque()],
			$résultat_observé,
		);

		$résultats_attendu = new Banque(
			nom: null,
			banques: [
				"file:///var/www/progression/tests/progression/dao/démo/banque_anonyme/sous_banque_anonyme.yml" => new Banque(),
			],
		);

		//La banque a bel et bien été modifiée dans la BD
		$this->assertEquals(
			$résultats_attendu,
			(new BanqueDAO())->get_banque(
				"bob",
				"file:///var/www/progression/tests/progression/dao/démo/banque_anonyme/anonyme.yml",
			),
		);
	}

	public function test_étant_donné_une_banque_inexistante_lorsquon_la_sauvegarde_sans_URL_alors_on_obtient_une_InvalidArgumentException()
	{
		$dao = new BanqueDAO();

		$this->expectException(InvalidArgumentException::class);
		$dao->save("test", "", new Banque("test1"));
	}

	# Modifications
	public function test_étant_donné_une_banque_existante_lorsquon_la_sauvegarde_avec_un_nom_différent_alors_la_banque_est_modifiée_dans_la_BD()
	{
		$dao = new BanqueDAO();
		$banque_modifiée = $dao->get_banque(
			"jane",
			"file:///var/www/progression/tests/progression/dao/démo/banque_complexe/contenu.yml",
		);
		$banque_modifiée->nom = "Nom affiché modifié";

		$résultat_observé = $dao->save(
			"jane",
			"file:///var/www/progression/tests/progression/dao/démo/banque_complexe/contenu.yml",
			$banque_modifiée,
		);
		$this->assertEquals(
			"Nom affiché modifié",
			$résultat_observé["file:///var/www/progression/tests/progression/dao/démo/banque_complexe/contenu.yml"]
				->nom,
		);

		//La banque a bel et bien été modifiée dans la BD
		$résultat_observé = (new BanqueDAO())->get_banque(
			"jane",
			"file:///var/www/progression/tests/progression/dao/démo/banque_complexe/contenu.yml",
		);

		$this->assertEquals("Nom affiché modifié", $résultat_observé->nom);
	}

	public function test_étant_donné_un_utilisateur_inexistant_lorsquon_lui_ajoute_une_banque_on_obtient_une_IntégritéException()
	{
		$dao = new BanqueDAO();

		$this->expectException(IntégritéException::class);
		$dao->save("inexistant", "test1", new Banque("test1"));
	}
}
