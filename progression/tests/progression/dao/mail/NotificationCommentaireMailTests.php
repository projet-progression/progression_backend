<?php
/*
  This file is part of Progression.

  Progression is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Progression is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Progression.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace progression\dao\mail;

use progression\domaine\entité\Commentaire;
use progression\domaine\entité\user\User;
use progression\TestCase;
use Illuminate\Support\Facades\{Config, View};

class NotificationCommentaireMailTests extends TestCase
{
	public User $jdoe;
	public User $bob;

	public function setUp(): void
	{
		parent::setUp();

		$this->jdoe = new User(username: "jdoe", date_inscription: 0);
		$this->bob = new User(username: "bob", date_inscription: 0);
	}

	public function test_étant_donné_un_destinataire_existant_francophone_lorsquon_envoie_un_message_le_courriel_est_correctement_localisé_et_envoyé()
	{
		Config::set("mail.redirection", "http://progression.com");

		$mail = new NotificationCommentaireMail(
			$this->jdoe,
			"aHR0cDovL3Rlc3QuY29tL3VuX2V4ZXJjaWNlLmluZm8",
			1735696174,
			new Commentaire(message: "un message", créateur: $this->bob, date: 1615696276, numéro_ligne: 3),
		);

		$mail->locale("fr");

		// HTML
		$this->assertStringEqualsFile(
			__DIR__ . "/résultats_attendus/notification_nouveau_commentaire_fr.html",
			$mail->render(),
		);

		// Text
		$text = View::make($mail->textView, $mail->buildViewData())->render();
		$this->assertStringEqualsFile(__DIR__ . "/résultats_attendus/notification_nouveau_commentaire_fr.txt", $text);
	}

	public function test_étant_donné_un_destinataire_existant_anglophone_lorsquon_envoie_un_message_le_courriel_est_correctement_localisé_et_envoyé()
	{
		Config::set("mail.redirection", "http://progression.com");

		$mail = new NotificationCommentaireMail(
			$this->jdoe,
			"aHR0cDovL3Rlc3QuY29tL3VuX2V4ZXJjaWNlLmluZm8",
			1735696174,
			new Commentaire(message: "un message", créateur: $this->bob, date: 1615696276, numéro_ligne: 3),
		);

		$mail->locale("en");

		// HTML
		$this->assertStringEqualsFile(
			__DIR__ . "/résultats_attendus/notification_nouveau_commentaire_en.html",
			$mail->render(),
		);

		// Text
		$text = View::make($mail->textView, $mail->buildViewData())->render();
		$this->assertStringEqualsFile(__DIR__ . "/résultats_attendus/notification_nouveau_commentaire_en.txt", $text);
	}
}
